package net.sigmainfo.lf.automation.api.function;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.internal.mapping.Jackson2Mapper;
import com.jayway.restassured.mapper.ObjectMapper;
import com.jayway.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.dataset.address;
import net.sigmainfo.lf.automation.common.AbstractTests;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

/**
 * Created by           : Shaishav.s on 20-02-2017.
 * Test class           : ApiFuncUtils.java
 * Description          : Contains reusable methods used while API automation
 * Includes             : 1. Get request implementation
 *                        2. Post request implementation
 *                        3. Response verification methods
 */
@Component
public class ApiFuncUtils extends AbstractTests{

    private Logger logger = LoggerFactory.getLogger(ApiFuncUtils.class);

    /**
     * THIS CLASS DEFINES ALL REUSABLE UTILITIES I.E. REUSABLE FUNCTIONS
     */

    /**
     * BELOW LINE IS VERY MUCH SIMILAR TO FOLLOWING STATEMENT
     * ApiParam apiParam = new ApiParam();
     *
     * =======================================
     *
     * @Autowired
    ApiParam apiParam;
     */
    @Autowired
    ApiParam apiParam;

    @Autowired
    address addressReq;

    /**
     * BELOW METHOD IS USED FOR POSTING A REST REQUEST and IT RETURNS RESPONSE
     * @param conType
     * @param endPoint
     * @param reqType
     * @return
     */

    public Response postRestRequest(String conType, String endPoint, String reqType) {

        String bearerToken = null;
        RequestSpecification requestSpec = createRequestBody(conType,endPoint,reqType,null,null);
        logger.info("=======================================================================================");
        logger.info("Triggering a post request to :" + endPoint );
        logger.info("=======================================================================================");

        Header header = new Header("Authorization","Bearer "+ bearerToken);
        Response response =given()
                .header(header)
                .contentType(conType)
                .spec(requestSpec)
                .log().all()
                .when()
                .post(endPoint);


        logger.info("============== Response Code: "+response.getStatusCode()+"=============================");
        return response;
    }

    /**
     *  CREATES A REQUEST PAYLOAD FOR POST REST REQUEST
     * @param conType
     * @param endPoint
     * @param reqType
     * @param refId
     * @param verificationToken
     * @return
     */
    private RequestSpecification createRequestBody(String conType,String endPoint,String reqType,String refId,String verificationToken) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        ObjectMapper objectMapper = null;

        /**
         * BELOW SNIPPET SERIALIZES THE JSON REQUEST PAYLOAD
         */
        if (conType.equalsIgnoreCase("application/json")) {
            objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

                public com.fasterxml.jackson.databind.ObjectMapper create(
                        Class arg0, String arg1) {
                    com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
                    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                    return objectMapper;
                }

            });

        }
        builder.setContentType(conType);


        if (reqType.equalsIgnoreCase("/address")) {

            logger.info("Constructing request body for checking availability of the user  in " + conType + " format.");

            addressReq.setPort(apiParam.port);

            setBody(builder, objectMapper, addressReq, conType);
        }

        return builder.build();
    }

    /**
     * CONSTRUCTS REQUEST BODY IN JSON OR XML FORMAT
     * @param builder
     * @param objectMapper
     * @param body
     * @param conType
     */

    private static void setBody(RequestSpecBuilder builder,
                                ObjectMapper objectMapper, Object body, String conType) {

        if (conType.contains("json")) {
            builder.setBody(body, objectMapper);
        } else if (conType.contains("xml")) {
            builder.setBody(body);
        }
    }

    /**
     * ASSERTING A RESPONSE PARAMETERS
     * @param getVerifyOtpResponse
     */

    public void verifygetOtpVerification(Response getVerifyOtpResponse) {
        assertEquals(getVerifyOtpResponse.getStatusCode(), 200);
        assertEquals(new JSONObject(getVerifyOtpResponse.prettyPrint()).get("status"),"Success");
        assertEquals(new JSONObject(getVerifyOtpResponse.prettyPrint()).get("success"),false);
        assertEquals(new JSONObject(getVerifyOtpResponse.prettyPrint()).get("message"),"Verification code is incorrect");
    }
}
