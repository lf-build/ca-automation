package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerDashboardPage;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.selenium.webdriven.commands.GetText;

/**
 * Created by shaishav.s on 30-11-2017.
 */
public class BackOfficeNewApplicationPage extends AbstractTests {

	private Logger logger = LoggerFactory.getLogger(BackOfficeNewApplicationPage.class);
	WebDriverWait wait = new WebDriverWait(driver, 60);

	public BackOfficeSearchPage submitApplication(WebDriver driver) throws Exception {
		PortalFuncUtils.waitForPageToLoad(driver);
		try {
			enterBasicDetails(driver);
		} catch (Exception e) {
		}
		try {
			clickBasicSaveAndContinue(driver);
		} catch (Exception e) {
		}
		try {
			enterBusinessDetails(driver);
		} catch (Exception e) {
		}
		try {
			clickBusinessNext(driver);
		} catch (Exception e) {
		}
		try {
			enterOwnerDetails(driver);
		} catch (Exception e) {
		}
		try {
			clickOwnerNext(driver);
		} catch (Exception e) {
		}
		try {
			enterFinishDetails(driver);
		} catch (Exception e) {
		}
		try {
			clickFinishNext(driver);
		} catch (Exception e) {
		}

		return new BackOfficeSearchPage(driver);

	}

	public BackOfficeNewApplicationPage(WebDriver driver) throws Exception {
		this.driver = driver;
		PortalFuncUtils.waitForPageToLoad(driver);
		assertTrue(PortalFuncUtils
				.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_newAppTab)));
		assertTrue(PortalFuncUtils
				.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_newAppTab)));
		assertTrue(
				driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_newAppTab)).isDisplayed());
		PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_newAppTab),
				"New Application Creation Page");

		logger.info("=========== BackOfficeNewApplicationPage is loaded============");
	}

	public BackOfficeNewApplicationPage() {
	}

	public void enterBasicDetails(WebDriver driver) throws Exception {

		// Thread.sleep(1500);
		assertTrue(PortalFuncUtils.waitForElementToBeVisible(
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Basic_howMuchDoYouNeed)));
		assertTrue(PortalFuncUtils.waitForElementToBeClickable(
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Basic_howMuchDoYouNeed)));
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Basic_howMuchDoYouNeed),
				PortalParam.amountNeed);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Basic_howSoonDoYouNeedIt),
				PortalParam.durationNeed);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Basic_purposeOfLoan),
				PortalParam.purposeOfFunds);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Basic_contactFirstName),
				PortalParam.firstName);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Basic_contactLastName),
				PortalParam.lastName);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Basic_phoneNumber),
				PortalParam.primaryPhone);
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Basic_email),
				PortalParam.userName);
			}

	public void clickBasicSaveAndContinue(WebDriver driver) throws Exception {
		try {
			assertTrue(PortalFuncUtils.waitForElementToBeVisible(
					PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Basic_saveAndContinue)));
			assertTrue(PortalFuncUtils.waitForElementToBeClickable(
					PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Basic_saveAndContinue)));
			PortalFuncUtils.clickButton(driver,
					PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Basic_saveAndContinue),
					"Save And Continue");
		} catch (Exception e) {
			throw new Exception(e.getMessage());

		}
	}

	public void enterBusinessDetails(WebDriver driver) throws Exception {
		PortalFuncUtils.waitForPageToLoad(driver);
		assertTrue(PortalFuncUtils.waitForElementToBeVisible(
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_legalBusinessName)));
		assertTrue(PortalFuncUtils.waitForElementToBeClickable(
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_legalBusinessName)));
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_legalBusinessName),
				PortalParam.legalCompanyName);
		
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_businessAddress),
				PortalParam.businessAddress);
		
		WebElement businessCountry = driver
				.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_country));
		businessCountry.sendKeys("USA");
		businessCountry.sendKeys(Keys.ENTER);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_dba), PortalParam.dba);
		
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_city),
				PortalParam.city);
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_state),
				PortalParam.state);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_postalCode),
				PortalParam.postalCode);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_rentorown), PortalParam.ownership);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_industry), PortalParam.industry);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_businessTaxId),
				PortalParam.businessTaxId);
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_sicCode),
				PortalParam.businessTaxId);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_entityType),
				PortalParam.entityType);
		Thread.sleep(1500);
		WebElement element = driver
				.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_entityType));
		element.sendKeys(Keys.TAB);
		WebElement a = driver
				.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_dateEstablished));
		a.sendKeys("03/03/2006");
		a.sendKeys(Keys.ENTER);
		logger.info("Date Selected");
		Thread.sleep(2000);

	}

	public void clickBusinessNext(WebDriver driver) throws Exception {
		PortalFuncUtils.clickButton(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Business_saveAndContinue), "Next");
	}

	public void enterOwnerDetails(WebDriver driver) throws Exception {

		assertTrue(PortalFuncUtils.waitForElementToBeVisible(
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_firstName)));
		Thread.sleep(1500);
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_firstName),
				PortalParam.ownerFirstName);
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_lastName),
				PortalParam.ownerLastName);
		Thread.sleep(2000);
		
		/*assertTrue(PortalFuncUtils.waitForElementToBeVisible(
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_homeAddress)));*/
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_homeAddress),
				PortalParam.homeAddress);
    	WebElement ownerCountry = driver
				.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_country));
		ownerCountry.sendKeys("USA");
		ownerCountry.sendKeys(Keys.ENTER);
		
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_city),
				PortalParam.homeCity);
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_state),
				PortalParam.homeState);
		Thread.sleep(1000);
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_postalCode),
				PortalParam.postalCode);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_mobilePhone),
				PortalParam.mobileNumber);
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_email),
				PortalParam.emailAddress);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_OfOwnership),
				PortalParam.percentOwnership);
		Thread.sleep(2000);
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_ssn),
				PortalParam.ssn);
		Thread.sleep(1000);
		WebElement a = driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_dob));
		a.sendKeys("01/01/2000");
		a.sendKeys(Keys.ENTER);

	}

	public void clickOwnerNext(WebDriver driver) throws Exception {
		PortalFuncUtils.clickButton(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Owner_saveAndContinue), "Save And Continue");
	}

	private void enterFinishDetails(WebDriver driver) throws Exception {

		assertTrue(PortalFuncUtils.waitForElementToBeVisible(
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Finish_annualRevenue)));
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Finish_annualRevenue),
				PortalParam.annualRevenue);
		PortalFuncUtils.insertText(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Finish_averageBankBalances),
				PortalParam.aveBankBalance);
		PortalFuncUtils.selectCheckbox(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Finish_checkBox));

	}

	public void clickFinishNext(WebDriver driver) throws Exception {
		PortalFuncUtils.scrollOnTopOfThePage(driver);
		PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_newAppTab_Finish_submit),
				"Submit");
		Thread.sleep(5000);

	}

	public void searchapp(WebDriver driver) throws Exception {
		Thread.sleep(2000);
		assertTrue(PortalFuncUtils.waitForElementToBeVisible(
				PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_emailTextBox)), "Timed out.");
		assertTrue(
				PortalFuncUtils.waitForElementToBeClickable(
						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_emailTextBox)),
				"Application email textbox not clickable within given time");
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_emailTextBox),
				PortalParam.userName);
		PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_searchButton),
				"Search");
		assertTrue(
				PortalFuncUtils.waitForElementToBeVisible(
						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_viewDetailsButton)),
				"View details button not found");
		assertTrue(
				PortalFuncUtils.waitForElementToBeClickable(
						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_viewDetailsButton)),
				"View details button not clickable");
		assertEquals(
				driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessNameLabel))
						.getText(),
				PortalParam.businessName, "Business name does not match");
		String AppStatus = driver
				.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel))
				.getText();
		if (AppStatus.equalsIgnoreCase("Declined")) {
			assertEquals(driver
					.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel))
					.getText(), "Declined", "Status does not match");
		} else if (AppStatus.equalsIgnoreCase("Verification")) {
			assertEquals(driver
					.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel))
					.getText(), "Verification", "Status does not match");
		} else if (AppStatus.equalsIgnoreCase("Application Submitted")) {
			assertEquals(driver
					.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel))
					.getText(), "Application Submitted", "Status does not match");

		}
		PortalFuncUtils.clickButton(driver,
				PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_viewDetailsButton), "View Details");

	}

}
