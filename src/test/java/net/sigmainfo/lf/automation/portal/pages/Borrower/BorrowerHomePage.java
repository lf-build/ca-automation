package net.sigmainfo.lf.automation.portal.pages.Borrower;

import junit.framework.Assert;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 06-10-2017.
 */
public class BorrowerHomePage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BorrowerHomePage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BorrowerHomePage(WebDriver driver) throws Exception {

        this.driver = driver;
        PortalFuncUtils.waitForPageToLoad(driver);
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_howMuchDoYouNeedDropdown)));
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_newApplicationLabel)));
        assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.homepage_newApplicationLabel)).isDisplayed());
        logger.info("=========== BorrowerHomePage is loaded============");
    }
    public BorrowerHomePage(){}

    public void enterBasicDetails(WebDriver driver) throws Exception {
        //new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_howMuchDoYouNeedDropdown)));
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_howMuchDoYouNeedDropdown)));
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_firstNameTextBox)));
        Thread.sleep(1500);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_howMuchDoYouNeedDropdown), PortalParam.amountNeed);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_howSoonYouNeedDropdown),PortalParam.durationNeed);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_purposeOfFundsDropdown),PortalParam.purposeOfFunds);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_firstNameTextBox),PortalParam.firstName);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_lastNameTextBox),PortalParam.lastName);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_primaryPhoneTextBox),PortalParam.primaryPhone);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_emailAddressTextBox),PortalParam.userName);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_passwordTextBox), StringEncrypter.createNewEncrypter().decrypt(PortalParam.password));
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_confirmPasswordTextBox), StringEncrypter.createNewEncrypter().decrypt(PortalParam.password));
    }
    public void clickBasicNext(WebDriver driver) throws Exception {
        try {
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_nextButton), "Next");
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public void enterBusinessDetails(WebDriver driver) throws Exception {
        //new WebDriverWait(driver,40).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='profile dropdown ']/a/div//strong[contains(text(),'"+portalParam.firstName+"')]")));
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//li[@class='profile dropdown ']/a/div//strong[contains(text(),'"+portalParam.firstName+"')]")),"User is not logged in and his name is not seen on top right corner of the page.");
        PortalFuncUtils.waitForPageToLoad(driver);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_legalCompanyNameTextBox),PortalParam.legalCompanyName);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_businessAddressTextBox),PortalParam.businessAddress);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_dbaTextBox),PortalParam.dba);
        Thread.sleep(2000);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_Country),PortalParam.Country);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_cityTextBox),PortalParam.city);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_stateDropdown),PortalParam.state);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_postalCodeTextBox), PortalParam.postalCode);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_rentOrOwnDropdown), PortalParam.ownership);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_industryDropdown),PortalParam.industry);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_businessTaxIdTextBox), PortalParam.businessTaxId);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_entityTypeDropdown),PortalParam.entityType);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_estMonthDropdown),PortalParam.estMonth);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_estDayDropdown),PortalParam.estDay);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_estYearDropdown),PortalParam.estYear);
        
    }

    public void clickBusinessNext(WebDriver driver) throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_businessTab_nextButton),"Next");
    }

    public BorrowerDashboardPage submitApplication(WebDriver driver) throws Exception {
            PortalFuncUtils.waitForPageToLoad(driver);
            try {
                enterBasicDetails(driver);
            }
            catch (Exception e){}
            try {
                clickBasicNext(driver);
            }catch (Exception e){}
            try {
                enterBusinessDetails(driver);
            }catch (Exception e){}
            try {
                clickBusinessNext(driver);
            }catch (Exception e){}
            try {
                enterOwnerDetails(driver);
            }catch (Exception e){}
            try {
                clickOwnerNext(driver);
            }catch (Exception e){}
            try {
                enterFinishDetails(driver);
            }catch (Exception e){}
            try {
                clickSubmit(driver);
            }catch (Exception e){}
            return new BorrowerDashboardPage(driver);
    }

    public void clickSubmit(WebDriver driver) throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_submitButton),"Submit");
    }

    private void enterFinishDetails(WebDriver driver) throws Exception {
        //new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_annualRevenueTextBox)));
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_annualRevenueTextBox)));
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_annualRevenueTextBox)));
        PortalFuncUtils.scrollUntil(PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab),driver);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_annualRevenueTextBox),PortalParam.annualRevenue);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_avgBankBalanceTextBox),PortalParam.aveBankBalance);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_existingLoanStatusDropdown),PortalParam.existingLoan);
        PortalFuncUtils.selectCheckbox(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_agreementCheckBox));
    }
    private void enterLeadFinishDetails(WebDriver driver) throws Exception {
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_annualRevenueTextBox)));
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_annualRevenueTextBox)));
        PortalFuncUtils.scrollUntil(PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab),driver);
        driver.findElement(PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_annualRevenueTextBox)).sendKeys(Keys.chord(Keys.CONTROL, "a"), PortalParam.annualRevenue);
        driver.findElement(PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_avgBankBalanceTextBox)).sendKeys(Keys.chord(Keys.CONTROL, "a"), PortalParam.aveBankBalance);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_existingLoanStatusDropdown),PortalParam.existingLoan);
        PortalFuncUtils.selectCheckbox(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_agreementCheckBox));
    }


    private void clickOwnerNext(WebDriver driver) throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_nextButton),"Next");
    }

    public void enterOwnerDetails(WebDriver driver) throws Exception {
        //new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_firstNameTextBox)));
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_firstNameTextBox)));
        Thread.sleep(1500);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_firstNameTextBox),PortalParam.ownerFirstName);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_lastNameTextBox),PortalParam.ownerLastName);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_homeAddressTextBox),PortalParam.homeAddress);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_Country),PortalParam.Country);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_homeCityTextBox),PortalParam.homeCity);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_homeStateDropdown),PortalParam.homeState);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_postalCodeTextBox),PortalParam.postalCode);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_mobilePhoneTextBox),PortalParam.mobileNumber);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_ownershipTextBox),PortalParam.percentOwnership);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_ssnTextBox),PortalParam.ssn);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_emailAddressTextBox),PortalParam.emailAddress);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_mobDropdown),PortalParam.dobMonth);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_dobDropdown),PortalParam.dobDay);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_ownerTab_yobDropdown),PortalParam.dobYear);
    }

    public BorrowerHomePage logout(WebDriver driver) throws Exception {
        PortalFuncUtils.clickButton(driver,By.xpath("//li[@class='profile dropdown ']/a/div//strong/strong"),"Profile");
        PortalFuncUtils.clickButton(driver,By.xpath("//a[@id='open_93907719']"),"Log out");
        return new BorrowerHomePage(driver);
    }

    public BorrowerLoginPage clickSignInButton(WebDriver driver) throws Exception {
        PortalFuncUtils.clickButton(driver,By.xpath("//button[@value='Sign In']"),"SignIn");
        return new BorrowerLoginPage(driver);
    }
    public void enterLoginDetails(WebDriver driver) throws Exception {
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loginpage_emailTextBox),PortalParam.userName);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loginpage_passwordTextBox), StringEncrypter.createNewEncrypter().decrypt(PortalParam.password));
        
    }
    public BorrowerLoginPage clickLoginButton(WebDriver driver) throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loginpage_submitButton),"Login button clicked");     
        return new BorrowerLoginPage(driver);
    }
    
    public BorrowerDashboardPage clickReapplyButton(WebDriver driver) throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_todo_reapplyButton),"Reapply button clicked");
        Thread.sleep(3000);
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_todo_reapplyButton_yes),"Reapply Yes button clicked");
    	PortalFuncUtils.waitForPageToLoad(driver);
    	Thread.sleep(8000);
    	PortalFuncUtils.selectCheckbox(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_agreementCheckBox));
    	PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_finishTab_submitButton),"Submit");
    	return new BorrowerDashboardPage(driver);
    }
    
	public BorrowerDashboardPage submitLeadApplication(WebDriver driver) throws Exception {
        PortalFuncUtils.waitForPageToLoad(driver);
        try {
            enterBasicDetails(driver);
        }
        catch (Exception e){}
        try {
            clickBasicNext(driver);
        }catch (Exception e){}
        try {
        	logout(driver);
        }catch (Exception e){}
        try {
        	clickSignInButton(driver);
        }catch(Exception e) {}
        try {
        	enterLoginDetails(driver);
        }catch(Exception e) {}
        try {
        	clickLoginButton(driver);
        }catch(Exception e) {}
        try {
            enterBusinessDetails(driver);
        }catch (Exception e){}
        try {
            clickBusinessNext(driver);
        }catch (Exception e){}
        try {
            enterOwnerDetails(driver);
        }catch (Exception e){}
        try {
            clickOwnerNext(driver);
        }catch (Exception e){}
        try {
        	enterLeadFinishDetails(driver);
        }catch (Exception e){}
        try {
            clickSubmit(driver);
        }catch (Exception e){}
        return new BorrowerDashboardPage(driver);
}
}
