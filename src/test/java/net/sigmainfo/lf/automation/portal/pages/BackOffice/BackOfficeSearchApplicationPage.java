package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 30-11-2017.
 */
public class BackOfficeSearchApplicationPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(BackOfficeSearchApplicationPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public BackOfficeSearchApplicationPage(WebDriver driver) throws Exception {

        this.driver = driver;
        logger.info("=========== BackOfficeSearchApplicationPage is loaded============");
    }
    public BackOfficeSearchApplicationPage(){}
    
    public BackOfficeSearchApplicationPage searchApp(String appId,Boolean isDeclined) throws Exception {

        PortalFuncUtils.waitForPageToLoad(driver);
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown)),"Timed out waiting for profile dropdown.");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_applicationNumberTextBox)),"Timed out waiting for application number textbox.");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_applicationNumberTextBox)),"Application number textbox not clickable within given time");
        //PortalFuncUtils.waitForTextToBePresent(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_productIdTextBox),"mca");
        PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_applicationNumberTextBox), appId);
        Thread.sleep(5000);
        return new BackOfficeSearchApplicationPage(driver);
}
    public void   searchAppByAllFields(WebDriver driver) throws Exception {
      	 Thread.sleep(2000);
      	    assertTrue(PortalFuncUtils.waitForElementToBeVisible(
   				PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_businessPhoneTextBox)), "Timed out.");
   		assertTrue(
   				PortalFuncUtils.waitForElementToBeClickable(
   						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_businessPhoneTextBox)),
   				"Business PhoneNumber is not clickable within given time");
   		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_businessPhoneTextBox),
   				PortalParam.businessPhone);
   		
   		assertTrue(PortalFuncUtils.waitForElementToBeVisible(
   				PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_firstNameTextBox)), "Timed out.");
   		assertTrue(
   				PortalFuncUtils.waitForElementToBeClickable(
   						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_firstNameTextBox)),
   				"First Name is not clickable within given time");
   		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_firstNameTextBox),
   				PortalParam.firstName);
   		assertTrue(PortalFuncUtils.waitForElementToBeVisible(
   				PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_lastNameTextBox)), "Timed out.");
   		assertTrue(
   				PortalFuncUtils.waitForElementToBeClickable(
   						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_lastNameTextBox)),
   				"Last Name is not clickable within given time");
   		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_lastNameTextBox),
   				PortalParam.lastName);
      	 assertTrue(PortalFuncUtils.waitForElementToBeVisible(
      				PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_emailTextBox)), "Timed out.");
      		assertTrue(
      				PortalFuncUtils.waitForElementToBeClickable(
      						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_emailTextBox)),
      				"Application email textbox not clickable within given time");
      		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_emailTextBox),
      				PortalParam.userName);
      		
      		assertTrue(PortalFuncUtils.waitForElementToBeVisible(
      				PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_businessNameTextBox)), "Timed out.");
      		assertTrue(
      				PortalFuncUtils.waitForElementToBeClickable(
      						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_businessNameTextBox)),
      				"Business Name textbox not clickable within given time");
      		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_businessNameTextBox),
      				PortalParam.legalCompanyName);
      		Thread.sleep(1000);
      		WebElement a = driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_statusDropdown));
    		a.sendKeys("Verification");
    		a.sendKeys(Keys.ENTER);
    		Thread.sleep(1000);
    		WebElement appDate = driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_appDateDropdown));
    		appDate.sendKeys("Last 20 Days");
    		appDate.sendKeys(Keys.ENTER);
    		Thread.sleep(1000);
    		WebElement expDate = driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_expDateDropdown));
    		expDate.sendKeys("Next 60 Days");
    		expDate.sendKeys(Keys.ENTER);
      		
      		
        	PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_searchButton),
      				"Search");
      		assertTrue(
      				PortalFuncUtils.waitForElementToBeVisible(
      						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_viewDetailsButton)),
      				"View details button not found");
      		assertTrue(
      				PortalFuncUtils.waitForElementToBeClickable(
      						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_viewDetailsButton)),
      				"View details button not clickable");
      		assertEquals(
      				driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessNameLabel))
      						.getText(),
      				PortalParam.businessName, "Business name does not match");
      		

      		String AppStatus = driver
      				.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel))
      				.getText();
      		if (AppStatus.equalsIgnoreCase("Declined")) {
      			assertEquals(driver
      					.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel))
      					.getText(), "Declined", "Status does not match");
      		} else if (AppStatus.equalsIgnoreCase("Verification")) {
      			assertEquals(driver
      					.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel))
      					.getText(), "Verification", "Status does not match");
      		} else if (AppStatus.equalsIgnoreCase("Application Submitted")) {
      			assertEquals(driver
      					.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel))
      					.getText(), "Application Submitted", "Status does not match");

      		}
      		
      		PortalFuncUtils.clickButton(driver,
      				PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_viewDetailsButton), "View Details");
      	

      }
    
    public BackOfficeLoginPage logout(WebDriver driver) throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown),"Profile");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_logoutLink)),"Logout link not clickable");

        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_header_logoutLink),"Log out");
        return new BackOfficeLoginPage(driver);
    }
}
