package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import junit.framework.Assert;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerHomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 30-11-2017.
 */
public class BackOfficeMyApplicationsPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(BackOfficeMyApplicationsPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);
    public BackOfficeMyApplicationsPage(WebDriver driver) throws Exception {

        this.driver = driver;
        logger.info("=========== BackOfficeMyApplicationsPage is loaded============");
    }
    public BackOfficeMyApplicationsPage(){}
}
