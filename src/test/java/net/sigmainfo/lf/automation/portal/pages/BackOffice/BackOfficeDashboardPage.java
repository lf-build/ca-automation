package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import junit.framework.Assert;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerHomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 30-11-2017.
 */
public class BackOfficeDashboardPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BackOfficeDashboardPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public BackOfficeDashboardPage(WebDriver driver) throws Exception {

        this.driver = driver;
        logger.info("=========== BackOfficeDashboardPage is loaded============");
    }
    public BackOfficeDashboardPage(){}
}
