package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 30-11-2017.
 */
public class BackOfficeAllUsersPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(BackOfficeAllUsersPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public BackOfficeAllUsersPage(WebDriver driver) throws Exception {

        this.driver = driver;
        logger.info("=========== BackOfficeAllUsersPage is loaded============");
    }
    public BackOfficeAllUsersPage(){}
}
