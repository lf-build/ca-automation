package net.sigmainfo.lf.automation.portal.function;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : GlobalVariables.java
 * Description          : Contains all global variables which will be used throughout the automation suite
 */
@Component
public class GlobalVariables {

    public static int Pagetimeout = 90000;
}
