package net.sigmainfo.lf.automation.portal.pages.Borrower;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import javax.swing.*;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 28-11-2017.
 */
public class BorrowerDashboardPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BorrowerDashboardPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BorrowerDashboardPage(WebDriver driver) throws Exception {

        this.driver = driver;
        if(!PortalParam.isDeclined) {
            PortalFuncUtils.waitForPageToLoad(driver);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//div[@id='accordion']//strong[contains(text(),'You are almost complete, please proceed to bank verification')]")),"\"You are almost complete, please proceed to bank verification\" message not seen");
        }
        if(PortalParam.isDeclined)
        {
            PortalFuncUtils.waitForPageToLoad(driver);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//a[contains(.,'Miscellaneous Documents')]")));
        }
        assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_borrowerNameLabel)).getText().contains(portalParam.firstName), "Borrower name does not match");
       // assertTrue(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_phoneNumberLabel)).getText()).contains(PortalParam.primaryPhone), "Phone number does not match");
       // assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_emailIdLabel)).getText().contains(portalParam.userName), "Username does not match");
       assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_purposeOfFundsLabel)).getText().contains(portalParam.purposeOfFunds), "Purpose of funds does not match");
        Assert.assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_loanAmountAppliedLabel)).getText()), PortalFuncUtils.removeSpecialChar(portalParam.amountNeed.split("-")[1]), "Loan amount does not match");
        logger.info("=========== BorrowerDashboardPage is loaded============");
    }
    public BorrowerDashboardPage(){}

    public BorrowerHomePage logout(WebDriver driver) throws Exception {
        try {
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.dashboard_profileDropdown)),"Profile dropdown not visible");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.dashboard_profileDropdown)),"Profile dropdown not clickable");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.dashboard_profileDropdown), "Profile");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.dashboard_logoutLink)),"Logout link not clickable");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.dashboard_logoutLink), "Log out");
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
             return new BorrowerHomePage(driver);
    }

    public void clicklinkBankAccount(WebDriver driver) throws Exception {
        try {
        	Thread.sleep(1000);
            PortalFuncUtils.scrollToElement(driver, PortalFuncUtils.getLocator(UIObjParam.dashboard_linkBankAccountButton));
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.dashboard_linkBankAccountButton)),"Link bank account button not found");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.dashboard_linkBankAccountButton)),"Link bank account button not clickable");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.dashboard_linkBankAccountButton), "Link bank account");
            Thread.sleep(1500);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public void completeToDo() throws Exception {
        PortalFuncUtils.scrollToElement(driver,By.xpath("//button[@value='Complete']"));
        PortalFuncUtils.clickButton(driver,By.xpath("//button[@value='Complete']"),"Complete");
    }

    public void linkBankAccount(WebDriver driver) throws Exception {
        try {
            clicklinkBankAccount(driver);
            connectBank(driver);
        }catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public void connectBank(WebDriver driver) throws Exception {
        try {
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//iframe[contains(@src,'"+PortalParam.iframeSource+"')]")),"Plaid container is not visible");
            driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@src,'"+PortalParam.iframeSource+"')]")));
            driver.findElement(By.xpath("//*[@id=\"plaid-link-container\"]/div/div[1]/div/div/div[2]/div[2]/button")).click();
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_searchBankTextBox), PortalParam.bankToConnect);
            //PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_sunTrustResultantBank), PortalParam.bankToConnect);
            PortalFuncUtils.clickButton(driver,By.xpath("//div[@class='App__content']//li[1]//span[@class='InstitutionSearchResult__text--is-primary' and contains(text(),'"+PortalParam.bankToConnect+"')]"),PortalParam.bankToConnect);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.selectBank_submitPlaidButton)),"Submit button not visible.");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_plaidUsernameTextBox), portalParam.plaid_user);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_plaidPasswordTextBox), portalParam.plaid_password);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_submitPlaidButton), "Submit");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.selectBank_selectYourAccountLabel)),"Select account label not seen");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_plaidSavingAccount), "Plaid Saving");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_continueButton), "Continue");
            driver.switchTo().defaultContent();
            PortalFuncUtils.scrollOnTopOfThePage(driver);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//div[@id='accordion']//strong[contains(text(),'Your application process is now complete')]")),"Success message not displayed after bank linking");
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }

    }
}
