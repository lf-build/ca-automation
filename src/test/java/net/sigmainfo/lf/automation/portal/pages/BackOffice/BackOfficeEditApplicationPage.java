package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.google.common.base.Function;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;

/**
 * Created by shaishav.s on 30-11-2017.
 */
public class BackOfficeEditApplicationPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(BackOfficeEditApplicationPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public BackOfficeEditApplicationPage(WebDriver driver) throws Exception {

        this.driver = driver;
        //wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog)));
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 90);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for BackOfficeAppDetailsPage Load Request to complete.");
        }
        logger.info("=========== BackOfficeAppDetailsPage is loaded============");
    }
    public void BackOfficeEditApplicationPage() throws Exception{
    	  Thread.sleep(8000);
    	  assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_businessName)).getText(), PortalParam.businessName, "Business name does not match");
    	  assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_businessTaxId)).getText(), PortalParam.businessTaxId, "BusinesstaxId does not match");
          assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_phone)).getText()), PortalParam.businessPhone, "Business phone does not match");
    	  assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable.");
          PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
          PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton), "Edit");
          try {
          PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit), "Edit details");
          Thread.sleep(2000);
          if(!driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit_Ownerdetails)).isDisplayed()){
        	  PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton), "close Button");
           	  assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable.");
           	  PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
                 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton), "Edit");
                 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit), "Edit details");
              }else {
           	 }
         }catch (Exception e) {
        	 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton), "close Button");
                 Thread.sleep(10000);
              	  assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable.");
              	  PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton), "Edit");
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit), "Edit details");
		}
          PortalFuncUtils.clearText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit_contactName), "Clearing text on Contact Name");          
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit_contactName), "admin");
          PortalFuncUtils.clearText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit_contactPhone), "Clearing text on Contact phone");          
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit_contactPhone), "9999999999");
          PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab), "Business Tab");
          PortalFuncUtils.clearText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName), "Clearing text on Business Name");          
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName), "SigmaInfo");
          PortalFuncUtils.clearText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId), "Clearing text on Business taxid");          
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId), "000000000");
          PortalFuncUtils.clearText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_SICCode), "Clearing text on SIC code");          
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_SICCode), "0000");
          PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab), "Click on OwnersTab ");
          PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner), "Click on plus ");
          PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName));
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName), "Sam");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName), "Bubsy");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address), "address");
          //PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country), "USA");
          WebElement country = driver
  				.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country));
          country.sendKeys("USA");
          country.sendKeys(Keys.ENTER);
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state), "NY");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city), "Irvine");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode), "12345");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber), "9999999999");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email), "abcde@yopmail.com");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN), "123456777");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership), "10");
          WebElement element= driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB));
          element.sendKeys("03/03/1984");
          element.sendKeys(Keys.ENTER);
          Thread.sleep(2000);
          PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit_saveChanges), "Save changes");
          Thread.sleep(15000);
          assertTrue(PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_phone)));
          Thread.sleep(2000);
          assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_phone)).getText()), "9999999999", "Business phone does not match");
          assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_businessName)).getText(), "SigmaInfo", "updated business name not matched");
          assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_businessTaxId)).getText(), "00-0000000", "updated mobile not matched");
    }
    public void sendpendingemail() throws Exception {
  	  assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable.");
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_sendPendingEmail), "send bank linking");
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_sendPendingEmail_sendPendingEmailButton), "send bank linking button");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Application is sent for Bank Linking')]")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Application is sent for Bank Linking')]")));
        logger.info("Application is sent for Bank Linking successfully");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog_email)),"Activity for email is not visable.");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog_email)).getText(), "Email Sent", "Activity for email is not showing");
        
        
  	
  }

    
    public void addSecondOwner() throws Exception {
    	Thread.sleep(5000);
    	assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton)),"Edit.");
             PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton), "Edit");
          try {
             PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit), "Edit details");
          Thread.sleep(2000);
          if(!driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit_Ownerdetails)).isDisplayed()){
        	  PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton), "close Button");
           	  assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable.");
           	  PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
                 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton), "Edit");
                 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit), "Edit details");
              }else {
           	 }
         }catch (Exception e) {
        	 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton), "close Button");
                 Thread.sleep(10000);
              	  assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable.");
              	  PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton), "Edit");
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit), "Edit details");
		}
          PortalFuncUtils.clearText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit_contactName), "Clearing text on Contact Name");          
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit_contactName), "admin");
          PortalFuncUtils.clearText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit_contactPhone), "Clearing text on Contact phone");          
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit_contactPhone), "9999999999");
          PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab), "Business Tab");
          PortalFuncUtils.clearText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName), "Clearing text on Business Name");          
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName), "SigmaInfo");
          PortalFuncUtils.clearText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId), "Clearing text on Business taxid");          
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId), "993334445");
          PortalFuncUtils.clearText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_SICCode), "Clearing text on SIC code");          
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_SICCode), "0000");
          PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab), "Click on OwnersTab ");
          PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner), "Click on plus ");
          PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName));
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName), "Sam");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName), "Bubsy");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address), "address");
          WebElement country = driver
    				.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country));
            country.sendKeys("USA");
            country.sendKeys(Keys.ENTER);
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state), "NY");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city), "Irvine");
          
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode), "12345");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber), "9999999999");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email), "abcde@yopmail.com");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN), "549483393");
          PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership), "10");
          WebElement element= driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB));
          element.sendKeys("03/03/1984");
          element.sendKeys(Keys.ENTER);
          Thread.sleep(2000);
          PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editButton_edit_saveChanges), "Save changes");
          Thread.sleep(15000);
    	
    }
    
    public void verifySingleOwner() throws Exception {
    	 assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable.");
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
         Thread.sleep(2000);
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_switchOwner), "Switch Owner");
         assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_switchOwner_message)).getText(), "You won't be able to switch owner as this application has only 1 owner", "updated business name not matched");
         Thread.sleep(2000);
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_switchOwner_closeButton), "Close Switch Owner action");
    }
    
    public void verifySwitchToOtherOwner() throws Exception {
    	 assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable.");
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_switchOwner), "Switch Owner");
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_switchOwner_AllOwners), "All Owners");
         Thread.sleep(3000);
         PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_switchOwner_AllOwners), "Sam Bubsy");
         Thread.sleep(2000);
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_switchOwner_newOwner), "new Owner");
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_switchOwner_switchOwnerButton), "Switch Owner button");
         Thread.sleep(10000);
         assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_switchOwner_switchActivity)).getText(), "Switch Owner Completed", "Switch Owner is not done");
    }
    
    public void verifyBankruptcyDecline() throws Exception {
    	assertTrue(PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)));
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
        assertTrue(PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton_bankruptcyButton)));
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton_bankruptcyButton)));
        Thread.sleep(2000);
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton_bankruptcyButton), "Bankruptcy");
        logger.info("Bankruptcy button clicked");
        Thread.sleep(2000);
        assertTrue(PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton_bankruptcySearchButton)));
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton_bankruptcySearchButton)));
        Thread.sleep(2000);
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton_bankruptcySearchButton), "Bankruptcy");
        Thread.sleep(10000);
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_headerStatus)).getText(), "Verification ", "Status does not match");
        logger.info("Application will not decline only fact will fails");
    }
    
    

}
