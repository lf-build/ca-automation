package net.sigmainfo.lf.automation.portal.tests;

import net.sigmainfo.lf.automation.common.CaptureScreenshot;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.*;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerDashboardPage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerHomePage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerLoginPage;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Listeners;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 08-02-2017.
 */
/*@Listeners(CaptureScreenshot.class)*/
public class SanityTests extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(SanityTests.class);
    public static String funcMod="Portal";
    // public WebDriver driver;

    @Autowired
    TestResults testResults;

    @Autowired
    PortalFuncUtils portalFuncUtils;

    public SanityTests() {}

    @AfterClass(alwaysRun=true)
    public void endCasereport() throws IOException, JSONException {

        String funcModule = "Portal";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  "+ org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcModule);
    }

    @Test(description = "", groups = {"PortalTests","Sanity","verifyEndToEndFlow"})
    public void VerifyEndToEndFlow() throws Exception {
        String sTestID = "verifyEndToEndFlow";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            borrowerDashboardPage.linkBankAccount(driver);
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyAppDetails(appId,isManualUpload);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
        	logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    @Test(description = "", groups = {"PortalTests","Sanity","verifyDeclinedAppTest","BusinessScenario"})
    public void verifyDeclinedAppTest () throws Exception {
        String sTestID = "verifyDeclinedAppTest";
        String result = "Failed";
        Boolean isDeclined=true;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyDeclinedAppDetails(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    @Test(description = "", groups = {"PortalTests","Sanity","verifyImproperCreditScoreTest","BusinessScenario"})
    public void verifyImproperCreditScoreTest () throws Exception {
        String sTestID = "verifyImproperCreditScoreTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String status="Verification";
        String pScore="649";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifySubmittedAppDetails(appId,pScore);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    @Test(description = "", groups = {"PortalTests","verifyRestrictedIndustryTest","BusinessScenario"})
    public void verifyRestrictedIndustryTest () throws Exception {
        String sTestID = "verifyRestrictedIndustryTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String status="Verification";
        String pScore="649";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyRestrictedIndustryAppDetails(appId,pScore);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    @Test(description = "", groups = {"PortalTests","Sanity","verifyHappyFlowWithManualUpload","BusinessScenario1"})
    public void verifyHappyFlowWithManualUpload() throws Exception {
        String sTestID = "verifyHappyFlowWithManualUpload";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=true;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyUploadStatement(appId,isManualUpload);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the Eligibility check for Annual revenue is less than $100000. In this test Annual revenue is $99999 so application should move to Manual review and fact will verify manually
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyAnnualRevenueLessTest","BusinessScenario"})
    public void verifyAnnualRevenueLessTest () throws Exception {
        String sTestID = "verifyAnnualRevenueLessTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyAnualRevenueVerification(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }


    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the Eligibility check for Time In Business is less than 6 months. In this test Time in Business is 20 days so application should move to Manual review and fact will verify manually
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyTimeInBusinessLessTest","BusinessScenario"})
    public void verifyTimeInBusinessLessTest() throws Exception {
        String sTestID = "verifyTimeInBusinessLessTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyTimeInBusinessVerification(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the Address verification with 3rd party, no mannual review is required
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyAddressVerificationTest","BusinessScenario"})
    public void verifyAddressVerificationTest() throws Exception {
        String sTestID = "verifyAddressVerificationTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyAddressVerification(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the DataMerch verification for no merchant is available with 3rd party, mannual review is required
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyDataMerchNoMerchantTest","BusinessScenario"})
    public void verifyDataMerchNoMerchantTest() throws Exception {
        String sTestID = "verifyDataMerchNoMerchantTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyDataMerchNM(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the DataMerch verification for Fraud category is available with 3rd party, mannual review is required
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyDataMerchFraudTest","BusinessScenario"})
    public void verifyDataMerchFraudTest() throws Exception {
        String sTestID = "verifyDataMerchFraudTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyDataMerchNM(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the DataMerch verification for Slow pay category is available with 3rd party, mannual review is required
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyDataMerchSlowPayTest","BusinessScenario1"})
    public void verifyDataMerchSlowPayTest() throws Exception {
        String sTestID = "verifyDataMerchSlowPayTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyDataMerchNM(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the DataMerch verification for Default category is available with 3rd party, mannual review is required
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyDataMerchDefaultTest","BusinessScenario1"})
    public void verifyDataMerchDefaultTest() throws Exception {
        String sTestID = "verifyDataMerchDefaultTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyDataMerchNM(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the DataMerch verification for Split category is available with 3rd party, mannual review is required
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyDataMerchSplitTest","BusinessScenario1"})
    public void verifyDataMerchSplitTest() throws Exception {
        String sTestID = "verifyDataMerchSplitTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyDataMerchNM(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the DataMerch verification for Stack category is available with 3rd party, mannual review is required
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyDataMerchStackTest","BusinessScenario1"})
    public void verifyDataMerchStackTest() throws Exception {
        String sTestID = "verifyDataMerchStackTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyDataMerchNM(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the DataMerch verification for Other category is available with 3rd party, mannual review is required
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyDataMerchOtherTest","BusinessScenario1"})
    public void verifyDataMerchOtherTest() throws Exception {
        String sTestID = "verifyDataMerchOtherTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyDataMerchNM(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the DataMerch verification for Criminal category is available with 3rd party, mannual review is required
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyDataMerchCriminalTest","BusinessScenario1"})
    public void verifyDataMerchCriminalTest() throws Exception {
        String sTestID = "verifyDataMerchCriminalTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyDataMerchNM(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the Experian personal report with some tags missed in the response with simulated data
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyPersonalReportNoTagTest","BusinessScenario1"})
    public void verifyPersonalReportNoTagTest() throws Exception {
        String sTestID = "verifyPersonalReportNoTagTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyPersonalReport(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the Experian personal report with Frozen in the response with simulated data
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyPersonalReportFrozenTest","OtherScenario"})
    public void verifyPersonalReportFrozenTest() throws Exception {
        String sTestID = "verifyPersonalReportFrozenTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyPersonalFrozeReport(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the Experian personal report with NoRecord in the response with simulated data
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyPersonalReportNoRecordTest","OtherScenario"})
    public void verifyPersonalReportNoRecordTest() throws Exception {
        String sTestID = "verifyPersonalReportNoRecordTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyPersonalNoRecordReport(appId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    /**
     * @author Shivani
     *
     * TestCase: Verify the borrower business  address verification 
     *
     *
     */

   /* @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyWhitepageAddressVerificationTest","OtherScenario"})
    public void verifyWhitepageAddressVerificationTest() throws Exception {
        String sTestID = "verifyWhitepageAddressVerificationTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(portalFuncUtils.getLocator(uiObjParam.homepage_basicTab_howSoonYouNeedDropdown)));
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);            
            backOfficeAppDetailsPage.verifyBusinessAddress();            
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }*/
    
    
    /**
     * @author Shivani
     *
     * TestCase: Verify the Bankruptcy , SSN: 549483393

     *
     *
     */
    
    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyBankruptcyFCRAandNonFCRATest","BusinessScenario1"})
    public void verifyBankruptcyFCRAandNonFCRATest() throws Exception {
        String sTestID = "verifyBankruptcyFCRAandNonFCRATest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
        	assertTrue(PortalFuncUtils.waitForElementToBeVisible(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
        	assertTrue(PortalFuncUtils.waitForElementToBeClickable(portalFuncUtils.getLocator(uiObjParam.homepage_basicTab_howSoonYouNeedDropdown)));
        	Thread.sleep(5000);
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyBankruptcyFCRAandNonFCRATest();
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
}
    
    /**
     * @author Shivani
     *
     * TestCase: Verify the no hit Bankruptcy verification, SSN: 000000000

     *
     *
     */
    
    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyNoHitBankruptcy","BusinessScenario1"})
    public void verifyNoHitBankruptcy() throws Exception {
        String sTestID = "verifyNoHitBankruptcy";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
        	assertTrue(PortalFuncUtils.waitForElementToBeVisible(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
        	assertTrue(PortalFuncUtils.waitForElementToBeClickable(portalFuncUtils.getLocator(uiObjParam.homepage_basicTab_howSoonYouNeedDropdown)));
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyBankruptcyFCRAandNonFCRATest();
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
}
    
    /**
     * @author Narayanaswamy.k
     *
     * TestCase: Verify edit application from the back office

     *
     *
     */
    
    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyEditApplicationTest","OtherScenario"})
    public void verifyEditApplicationTest() throws Exception {
        String sTestID = "verifyEditApplicationTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
         assertTrue(PortalFuncUtils.waitForElementToBeVisible(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
         assertTrue(PortalFuncUtils.waitForElementToBeClickable(portalFuncUtils.getLocator(uiObjParam.homepage_basicTab_howSoonYouNeedDropdown)));
         BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
         BackOfficeEditApplicationPage backOfficeEditApplicationPage=new BackOfficeEditApplicationPage(driver);
         BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
         appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
         BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
         driver.get(portalParam.backOfficeUrl);
         logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
         BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
         BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
         Thread.sleep(10000);
         backOfficeEditApplicationPage.BackOfficeEditApplicationPage();
         BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
}
    
    /**
     * @author Shivani
     *
     * TestCase: Verify application creation from BO 

     *
     *
     */
      @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyApplicationCreationFromBOTest","OtherScenario"})
    	public void verifyApplicationCreationFromBOTest() throws Exception {
        String sTestID = "verifyApplicationCreationFromBOTest";
        String result = "Failed";
        Boolean isDeclined=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
        	driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeNewApplicationPage backofficenewapplicationpage= new BackOfficeNewApplicationPage(driver);
            backOfficeSearchPage.submitApplication(driver);
            backofficenewapplicationpage.submitApplication(driver);
            backofficenewapplicationpage.searchapp(driver);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(portalFuncUtils.getLocator(uiObjParam.backoffice_homepage_newAppTab)));
        	assertTrue(PortalFuncUtils.waitForElementToBeClickable(portalFuncUtils.getLocator(uiObjParam.backoffice_homepage_newAppTab)));
        	
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    
}
      
      /**
       * @author Narayanaswamy.k
       *
       * TestCase: Verify send email for pending back email from the back office

       *
       *
       */
      
      
      @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifySendEmailTest","BusinessScenario1"})
      public void verifySendEmailTest() throws Exception {
          String sTestID = "verifySendEmailTest";
          String result = "Failed";
          Boolean isDeclined=false;
          String appId;
          WebDriverWait wait = new WebDriverWait(driver,60);
          testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
          logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
          initializeData(funcMod,sTestID);
          try {
           assertTrue(PortalFuncUtils.waitForElementToBeVisible(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
           assertTrue(PortalFuncUtils.waitForElementToBeClickable(portalFuncUtils.getLocator(uiObjParam.homepage_basicTab_howSoonYouNeedDropdown)));
           BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
           BackOfficeEditApplicationPage backOfficeEditApplicationPage=new BackOfficeEditApplicationPage(driver);
           BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
           appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
           BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
           driver.get(portalParam.backOfficeUrl);
           logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
           BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
           BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
           Thread.sleep(10000);
           backOfficeEditApplicationPage.sendpendingemail();
           BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
          } catch (Exception e) {
              logger.info("******************" + sTestID + "  failed. *****\n" + e.getMessage());
          } finally {
              logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
              String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
              testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
              writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
          }
  }
      
      
      /**
       * @author Narayanaswamy.k
       *
       * TestCase: Verify edit deal from the back office

       *
       *
       */
      
      
      @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyEditDealTest","BusinessScenario1"})
      public void verifyEditDealTest() throws Exception {
          String sTestID = "verifyEditDealTest";
          String result = "Failed";
          Boolean isManualUpload=false;
          String appId;
          WebDriverWait wait = new WebDriverWait(driver,60);
          testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
          logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
          initializeData(funcMod,sTestID);
          try {
           assertTrue(PortalFuncUtils.waitForElementToBeVisible(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
           assertTrue(PortalFuncUtils.waitForElementToBeClickable(portalFuncUtils.getLocator(uiObjParam.homepage_basicTab_howSoonYouNeedDropdown)));
           BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
           BackOfficeComputeOfferPage backOfficeComputeOfferPage =new BackOfficeComputeOfferPage(driver);
           BackOfficeEditApplicationPage backOfficeEditApplicationPage=new BackOfficeEditApplicationPage(driver);
           BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
           appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
           BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
           driver.get(portalParam.backOfficeUrl);
           logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
           BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
           BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isManualUpload);
           Thread.sleep(10000);
           backOfficeComputeOfferPage.verifyEditDealDetails(appId,isManualUpload);
           BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
          } catch (Exception e) {
              logger.info("******************" + sTestID + "  failed. *****\n" + e.getMessage());
          } finally {
              logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
              String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
              testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
              writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
          }
  }
      
      /**
       * @author Narayanaswamy.k
       *
       * TestCase: Verify switchOwner from the back office

       *
       *
       */
      
      @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifySwitchOwnerTest","OtherScenario"})
      public void verifySwitchOwnerTest() throws Exception {
          String sTestID = "verifySwitchOwnerTest";
          String result = "Failed";
          Boolean isManualUpload=false;
          String appId;
          WebDriverWait wait = new WebDriverWait(driver,60);
          testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
          logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
          initializeData(funcMod,sTestID);
          try {
           assertTrue(PortalFuncUtils.waitForElementToBeVisible(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
           assertTrue(PortalFuncUtils.waitForElementToBeClickable(portalFuncUtils.getLocator(uiObjParam.homepage_basicTab_howSoonYouNeedDropdown)));
           BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
           BackOfficeComputeOfferPage backOfficeComputeOfferPage =new BackOfficeComputeOfferPage(driver);
           BackOfficeEditApplicationPage backOfficeEditApplicationPage=new BackOfficeEditApplicationPage(driver);
           BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
           appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
           BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
           driver.get(portalParam.backOfficeUrl);
           logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
           BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
           BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isManualUpload);
           Thread.sleep(10000);
           backOfficeEditApplicationPage.verifySingleOwner();
           backOfficeEditApplicationPage.addSecondOwner();
           backOfficeEditApplicationPage.verifySwitchToOtherOwner();
           backOfficeEditApplicationPage.verifyBankruptcyDecline();
           
           BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
          } catch (Exception e) {
              logger.info("******************" + sTestID + "  failed. *****\n" + e.getMessage());
          } finally {  
          logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
          String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
          testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
          writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
          }
  }
      /**
       * @author Shivani
       *
       * TestCase: Verify search application by all fields 

       *
       *
       */
     @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifySearchByAllFieldsTest","OtherScenario"})
      public void verifySearchByAllFieldsTest() throws Exception {
          String sTestID = "verifySearchByAllFieldsTest";
          String result = "Failed";
          Boolean isManualUpload=false;
          String appId;
          WebDriverWait wait = new WebDriverWait(driver,60);
          testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
          logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
          initializeData(funcMod,sTestID);
          try {
              assertTrue(PortalFuncUtils.waitForElementToBeVisible(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
              assertTrue(PortalFuncUtils.waitForElementToBeClickable(portalFuncUtils.getLocator(uiObjParam.homepage_basicTab_howSoonYouNeedDropdown)));
              BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
              BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
              appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
              BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
              driver.get(portalParam.backOfficeUrl);
              logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
              BackOfficeSearchApplicationPage backOfficeSearchApplicationPage = BackOfficeLoginPage.loginToBackOfficeForAllFields(driver);
              backOfficeSearchApplicationPage.searchApp(appId,isManualUpload);
              backOfficeSearchApplicationPage.searchAppByAllFields(driver);
              Thread.sleep(10000);
              BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchApplicationPage.logout(driver);
             } catch (Exception e) {
                 logger.info("******************" + sTestID + "  failed. *****\n" + e.getMessage());
             } finally {  
             logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
             String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
             testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
             writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
             }
          }
     /**
      * @author Shivani
      *
      * TestCase: Verify application creation from status Lead created

      *
      *
      */
    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyLeadcreatedApplication","OtherScenario"})
     public void verifyLeadcreatedApplication() throws Exception {
         String sTestID = "verifyLeadcreatedApplication";
         String result = "Failed";
         Boolean isManualUpload=false;
         String appId;
         WebDriverWait wait = new WebDriverWait(driver,60);
         testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
         logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
         initializeData(funcMod,sTestID);
         try {
             assertTrue(PortalFuncUtils.waitForElementToBeVisible(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
             assertTrue(PortalFuncUtils.waitForElementToBeClickable(portalFuncUtils.getLocator(uiObjParam.homepage_basicTab_howSoonYouNeedDropdown)));
             BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
             BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitLeadApplication(driver);
             appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
             BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
             
            } catch (Exception e) {
                logger.info("******************" + sTestID + "  failed. *****\n" + e.getMessage());
            } finally {  
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
            }
         }
    
    /**
     * @author Shivani
     *
     * TestCase: Verify Reapply application

     *
     *
     */
   @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyReapplyTest","OtherScenario"})
    public void verifyReapplyTest() throws Exception {
        String sTestID = "verifyReapplyTest";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
        	assertTrue(PortalFuncUtils.waitForElementToBeVisible(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(portalFuncUtils.getLocator(uiObjParam.homepage_basicTab_howSoonYouNeedDropdown)));
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeAppDetailsPage.verifyDeclinedAppDetails(appId);
            backOfficeAppDetailsPage.verifyReapplyTest();
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            driver.get(portalParam.borrowerUrl);
            borrowerHomePage.clickSignInButton(driver);
            borrowerHomePage.enterLoginDetails(driver);
            borrowerHomePage.clickLoginButton(driver);
            borrowerHomePage.clickReapplyButton(driver);
            BorrowerHomePage borrowerHomePage01 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage1 = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage1 = backOfficeSearchPage.searchAppUsingEmail(driver);
            BackOfficeLoginPage backOfficeLoginPage1 = backOfficeSearchPage.logout(driver);
            result = "Passed";
           } catch (Exception e) {
               logger.info("******************" + sTestID + "  failed. *****\n" + e.getMessage());
           } finally {  
           logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
           String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
           testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
           writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
           }
        }
}
