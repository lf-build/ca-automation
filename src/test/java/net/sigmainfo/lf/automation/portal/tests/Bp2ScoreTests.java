package net.sigmainfo.lf.automation.portal.tests;

import net.sigmainfo.lf.automation.common.CaptureScreenshot;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.*;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerDashboardPage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerHomePage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerLoginPage;


import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;


import org.testng.annotations.AfterClass;
import org.testng.annotations.Listeners;

import static org.testng.Assert.assertTrue;

/**
 * Created by Narayana on 08-02-2017.
 */
/*@Listeners(CaptureScreenshot.class)*/
public class Bp2ScoreTests extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(Bp2ScoreTests.class);
    public static String funcMod="Portal";
    // public WebDriver driver;

    @Autowired
    TestResults testResults;

    @Autowired
    PortalFuncUtils portalFuncUtils;

    public Bp2ScoreTests() {}

    @AfterClass(alwaysRun=true)
    public void endCasereport() throws IOException, JSONException {

        String funcModule = "Portal";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  "+ org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcModule);
    }

    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the bp2 score is 764 for Tier1 and Grade A1 borrower with simulated data
     *
     *
     */
    
    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyBp2Score764Test","Bp2ScoreTests"})
    public void verifyBp2Score764Test() throws Exception {
        String sTestID = "verifyBp2Score764Test";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BackOfficeComputeOfferPage backOfficeComputeOfferPage =new BackOfficeComputeOfferPage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeComputeOfferPage.verifyOfferDetails(appId,isManualUpload);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
             } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    
    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the bp2 score is 746 for Tier1 and Grade A2 borrower with simulated data
     *
     *
     */
    
    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyBp2Score746Test","Bp2ScoreTests"})
    public void verifyBp2Score746Test() throws Exception {
        String sTestID = "verifyBp2Score746Test";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BackOfficeComputeOfferPage backOfficeComputeOfferPage =new BackOfficeComputeOfferPage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeComputeOfferPage.verifyOfferDetails(appId,isManualUpload);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    
    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the bp2 score is 735 for Tier1 and Grade A3 borrower with simulated data
     *
     *
     */
    
    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyBp2Score735Test","Bp2ScoreTests"})
    public void verifyBp2Score735Test() throws Exception {
        String sTestID = "verifyBp2Score735Test";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BackOfficeComputeOfferPage backOfficeComputeOfferPage =new BackOfficeComputeOfferPage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeComputeOfferPage.verifyOfferDetails(appId,isManualUpload);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    
    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the bp2 score is 731 for Tier1 and Grade A4 borrower with simulated data
     *
     *
     */
    
    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyBp2Score731Test","Bp2ScoreTests"})
    public void verifyBp2Score731Test() throws Exception {
        String sTestID = "verifyBp2Score731Test";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BackOfficeComputeOfferPage backOfficeComputeOfferPage =new BackOfficeComputeOfferPage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeComputeOfferPage.verifyOfferDetails(appId,isManualUpload);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    
    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the bp2 score is 726 for Tier1 and Grade B1 borrower with simulated data
     *
     *
     */
    
    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyBp2Score726Test","Bp2ScoreTests"})
    public void verifyBp2Score726Test() throws Exception {
        String sTestID = "verifyBp2Score726Test";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BackOfficeComputeOfferPage backOfficeComputeOfferPage =new BackOfficeComputeOfferPage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeComputeOfferPage.verifyOfferDetails(appId,isManualUpload);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    
    
    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the bp2 score is 714 for Tier 2 and Grade B4 borrower with simulated data
     *
     *
     */

    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyBp2Score714Test","Bp2ScoreTests"})
    public void verifyBp2Score714Test() throws Exception {
        String sTestID = "verifyBp2Score714Test";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BackOfficeComputeOfferPage backOfficeComputeOfferPage =new BackOfficeComputeOfferPage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeComputeOfferPage.verifyOfferDetails(appId,isManualUpload);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    
    /**
     * @author narayanaswamy.k
     *
     * TestCase: Verify the bp2 score is 709 for Tier2 and Grade C1 borrower with simulated data
     *
     *
     */
    
    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyBp2Score709Test","Bp2ScoreTests"})
    public void verifyBp2Score709Test() throws Exception {
        String sTestID = "verifyBp2Score709Test";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BackOfficeComputeOfferPage backOfficeComputeOfferPage =new BackOfficeComputeOfferPage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeComputeOfferPage.verifyOfferDetails(appId,isManualUpload);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    
    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyBp2Score706Test","Bp2ScoreTests"})
    public void verifyBp2Score706Test() throws Exception {
        String sTestID = "verifyBp2Score706Test";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BackOfficeComputeOfferPage backOfficeComputeOfferPage =new BackOfficeComputeOfferPage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeComputeOfferPage.verifyOfferDetails(appId,isManualUpload);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
   
    @Test(enabled=true,description = "", groups = {"PortalTests","Sanity","verifyBp2Score701Test","Bp2ScoreTests"})
    public void verifyBp2Score701Test() throws Exception {
        String sTestID = "verifyBp2Score701Test";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            BackOfficeComputeOfferPage backOfficeComputeOfferPage =new BackOfficeComputeOfferPage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            driver.get(portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId,isDeclined);
            backOfficeComputeOfferPage.verifyOfferDetails(appId,isManualUpload);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeSearchPage.logout(driver);
            result = "Passed";
        } catch (Exception e) {
            assertTrue(result.equalsIgnoreCase("Passed"),"******************" + sTestID + "  failed. *****");
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
   
    
}
