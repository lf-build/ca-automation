package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 30-11-2017.
 */
public class BackOfficeSearchPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(BackOfficeSearchPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public BackOfficeSearchPage(WebDriver driver) throws Exception {

        this.driver = driver;
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 90);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for BackOfficeAppDetailsPage Load Request to complete.");
        }
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_searchButton)));
        }catch (TimeoutException e)
        {
            Assert.fail("Search button could not be found");
        }
        logger.info("=========== BackOfficeSearchPage is loaded============");
    }
    public BackOfficeSearchPage(){}

    public BackOfficeAppDetailsPage searchApp(String appId,Boolean isDeclined) throws Exception {

            PortalFuncUtils.waitForPageToLoad(driver);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown)),"Timed out waiting for profile dropdown.");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_applicationNumberTextBox)),"Timed out waiting for application number textbox.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_applicationNumberTextBox)),"Application number textbox not clickable within given time");
            //PortalFuncUtils.waitForTextToBePresent(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_productIdTextBox),"mca");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_applicationNumberTextBox), appId);
            Thread.sleep(5000);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_searchButton), "Search");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_viewDetailsButton)),"View details button not found");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_viewDetailsButton)),"View details button not clickable");
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundAppIdLabel)).getText(), appId, "Application Id does not match");
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessNameLabel)).getText(), PortalParam.businessName, "Business name does not match");
            String AppStatus=driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel)).getText();
            if (AppStatus.equalsIgnoreCase("Declined")) {
            	 assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel)).getText(), "Declined", "Status does not match");
            	 } 
            else if(AppStatus.equalsIgnoreCase("Verification")) {
            	 assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel)).getText(), "Verification", "Status does not match");
            	 } 
            else if(AppStatus.equalsIgnoreCase("Application Submitted")){
            	 assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel)).getText(), "Application Submitted", "Status does not match");
            	 
            	 }
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_viewDetailsButton), "View Details");

            return new BackOfficeAppDetailsPage(driver);
    }

    public BackOfficeLoginPage logout(WebDriver driver) throws Exception {
    	assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown)),"Profile button is not clickable");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown),"Profile");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_logoutLink)),"Logout link not clickable");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_header_logoutLink),"Log out");
        return new BackOfficeLoginPage(driver);
    }
 public BackOfficeNewApplicationPage submitApplication(WebDriver driver) throws Exception{
    	
    	assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_newAppTab)));
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_newAppTab)));
        assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_newAppTab)).isDisplayed());
       	return null;
    	
    }
 public BackOfficeAppDetailsPage searchAppUsingEmail(WebDriver driver) throws Exception {
		Thread.sleep(2000);
		assertTrue(PortalFuncUtils.waitForElementToBeVisible(
				PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_emailTextBox)), "Timed out.");
		assertTrue(
				PortalFuncUtils.waitForElementToBeClickable(
						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_emailTextBox)),
				"Application email textbox not clickable within given time");
		PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_emailTextBox),
				PortalParam.userName);
		PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_searchButton),
				"Search");
		assertTrue(
				PortalFuncUtils.waitForElementToBeVisible(
						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_viewDetailsButton)),
				"View details button not found");
		assertTrue(
				PortalFuncUtils.waitForElementToBeClickable(
						PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_viewDetailsButton)),
				"View details button not clickable");
		assertEquals(
				driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessNameLabel))
						.getText(),
				PortalParam.businessName, "Business name does not match");
		String AppStatus = driver
				.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel))
				.getText();
		if (AppStatus.equalsIgnoreCase("Declined")) {
			assertEquals(driver
					.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel))
					.getText(), "Declined", "Status does not match");
		} else if (AppStatus.equalsIgnoreCase("Verification")) {
			assertEquals(driver
					.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel))
					.getText(), "Verification", "Status does not match");
		} else if (AppStatus.equalsIgnoreCase("Application Submitted")) {
			assertEquals(driver
					.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_foundBusinessStatusLabel))
					.getText(), "Application Submitted", "Status does not match");

		}
		
		return new BackOfficeAppDetailsPage(driver);
	}

 
}
