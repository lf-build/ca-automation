package net.sigmainfo.lf.automation.portal.constant;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : PortalParam.java
 * Description          : Contains all placeholders declared within the test package
 * Includes             : 1. Declares the place holders being used
 *                        2. Contains getter and setter methods
 */
@Component
public class PortalParam {


    public static String testid;
    public static String backOfficeUrl;
    public static String borrowerUrl;
    public static String custom_report_location;
    public static String testId;
    public static String businessName;
    public static String firstName;
    public static String lastName;
    public static String userName;
    public static String password;
    public static String primaryPhone;
    public static String purposeOfFunds;
    public static String industry;
    public static String businessAddress;
    public static String city;
    public static String state;
    public static String postalCode;
    public static String businessPhone;
    public static String taxId;
    public static String newTaxId;
    public static String personalName;
    public static String ssn;
    public static String browser;
    public static String encryptedPassword;
    public static String amountNeed;
    public static String durationNeed;
    public static String legalCompanyName;
    public static String ownership;
    public static String businessTaxId;
    public static String entityType;
    public static String estMonth;
    public static String estDay;
    public static String estYear;
    public static String ownerFirstName;
    public static String ownerLastName;
    public static String homeAddress;
    public static String homeCity;
    public static String homeState;
    public static String homePostalCode;
    public static String mobileNumber;
    public static String percentOwnership;
    public static String emailAddress;
    public static String dobDay;
    public static String dobMonth;
    public static String dobYear;
    public static String annualRevenue;
    public static String aveBankBalance;
    public static String existingLoan;
    public static String backofficeUsername;
    public static String backofficePassword;
    public static String gmailId;
    public static String gmailPassword;
    public static String dba;
    public static Boolean isDeclined;
    public static String plaid_user;
    public static String plaid_password;
    public static String upload_file_location;
    public static String bankToConnect;
    public static String iframeSource;
    public static String autobankAccountNumber;
    public static String manualBankAccountNumber;
    public static String Bp2Score;
    public static String Country;

    public static String getCountry() {
		return Country;
	}

	public static void setCountry(String Country) {
		PortalParam.Country = Country;
	}

	public static String getBp2Score() {
    return Bp2Score;
    }

    public static void setBp2Score(String bp2Score) {
    PortalParam.Bp2Score = bp2Score;
    }

    public static String getDba() {
        return dba;
    }

    public static void setDba(String dba) {
        PortalParam.dba = dba;
    }

    public static Boolean getIsDeclined() {
        return isDeclined;
    }

    public static void setIsDeclined(Boolean isDeclined) {
        PortalParam.isDeclined = isDeclined;
    }

    public static String getGmailId() {
        return gmailId;
    }

    public static void setGmailId(String gmailId) {
        PortalParam.gmailId = gmailId;
    }

    public static String getGmailPassword() {
        return gmailPassword;
    }

    public static void setGmailPassword(String gmailPassword) {
        PortalParam.gmailPassword = gmailPassword;
    }

    public static String getBackofficePassword() {
        return backofficePassword;
    }

    public static void setBackofficePassword(String backofficePassword) {
        PortalParam.backofficePassword = backofficePassword;
    }

    public static String getBackofficeUsername() {
        return backofficeUsername;
    }

    public static void setBackofficeUsername(String backofficeUsername) {
        PortalParam.backofficeUsername = backofficeUsername;
    }

    public static String getBackOfficeUrl() {
        return backOfficeUrl;
    }

    public static void setBackOfficeUrl(String backOfficeUrl) {
        PortalParam.backOfficeUrl = backOfficeUrl;
    }

    public static String getExistingLoan() {
        return existingLoan;
    }

    public static String getAnnualRevenue() {
        return annualRevenue;
    }

    public static void setAnnualRevenue(String annualRevenue) {
        PortalParam.annualRevenue = annualRevenue;
    }

    public static String getAveBankBalance() {
        return aveBankBalance;
    }

    public static void setAveBankBalance(String aveBankBalance) {
        PortalParam.aveBankBalance = aveBankBalance;
    }

    public static String isExistingLoan() {
        return existingLoan;
    }

    public static void setExistingLoan(String existingLoan) {
        PortalParam.existingLoan = existingLoan;
    }

    public static String getHomeAddress() {
        return homeAddress;
    }

    public static void setHomeAddress(String homeAddress) {
        PortalParam.homeAddress = homeAddress;
    }

    public static String getHomeCity() {
        return homeCity;
    }

    public static void setHomeCity(String homeCity) {
        PortalParam.homeCity = homeCity;
    }

    public static String getHomeState() {
        return homeState;
    }

    public static void setHomeState(String homeState) {
        PortalParam.homeState = homeState;
    }

    public static String getOwnerLastName() {
        return ownerLastName;
    }

    public static void setOwnerLastName(String ownerLastName) {
        PortalParam.ownerLastName = ownerLastName;
    }

    public static String getHomePostalCode() {
        return homePostalCode;
    }

    public static String getOwnerFirstName() {
        return ownerFirstName;
    }

    public static void setOwnerFirstName(String ownerFirstName) {
        PortalParam.ownerFirstName = ownerFirstName;
    }

    public static void setHomePostalCode(String homePostalCode) {
        PortalParam.homePostalCode = homePostalCode;
    }

    public static String getMobileNumber() {
        return mobileNumber;
    }

    public static void setMobileNumber(String mobileNumber) {
        PortalParam.mobileNumber = mobileNumber;
    }

    public static String getPercentOwnership() {
        return percentOwnership;
    }

    public static void setPercentOwnership(String percentOwnership) {
        PortalParam.percentOwnership = percentOwnership;
    }

    public static String getEmailAddress() {
        return emailAddress;
    }

    public static void setEmailAddress(String emailAddress) {
        PortalParam.emailAddress = emailAddress;
    }

    public static String getDobDay() {
        return dobDay;
    }

    public static void setDobDay(String dobDay) {
        PortalParam.dobDay = dobDay;
    }

    public static String getDobMonth() {
        return dobMonth;
    }

    public static void setDobMonth(String dobMonth) {
        PortalParam.dobMonth = dobMonth;
    }

    public static String getDobYear() {
        return dobYear;
    }

    public static void setDobYear(String dobYear) {
        PortalParam.dobYear = dobYear;
    }

    public static String getState() {
        return state;
    }

    public static void setState(String state) {
        PortalParam.state = state;
    }

    public static String getLegalCompanyName() {
        return legalCompanyName;
    }

    public static void setLegalCompanyName(String legalCompanyName) {
        PortalParam.legalCompanyName = legalCompanyName;
    }

    public static String getOwnership() {
        return ownership;
    }

    public static void setOwnership(String ownership) {
        PortalParam.ownership = ownership;
    }

    public static String getBusinessTaxId() {
        return businessTaxId;
    }

    public static void setBusinessTaxId(String businessTaxId) {
        PortalParam.businessTaxId = businessTaxId;
    }

    public static String getEntityType() {
        return entityType;
    }

    public static void setEntityType(String entityType) {
        PortalParam.entityType = entityType;
    }

    public static String getEstMonth() {
        return estMonth;
    }

    public static void setEstMonth(String estMonth) {
        PortalParam.estMonth = estMonth;
    }

    public static String getEstDay() {
        return estDay;
    }

    public static void setEstDay(String estDay) {
        PortalParam.estDay = estDay;
    }

    public static String getEstYear() {
        return estYear;
    }

    public static void setEstYear(String estYear) {
        PortalParam.estYear = estYear;
    }

    public static String getTestid() {
        return testid;
    }

    public static void setTestid(String testid) {
        PortalParam.testid = testid;
    }

    public static String getBorrowerUrl() {
        return borrowerUrl;
    }

    public static void setBorrowerUrl(String borrowerUrl) {
        PortalParam.borrowerUrl = borrowerUrl;
    }

    public static String getCustom_report_location() {
        return custom_report_location;
    }

    public static void setCustom_report_location(String custom_report_location) {
        PortalParam.custom_report_location = custom_report_location;
    }

    public static String getPostalCode() {
        return postalCode;
    }

    public static void setPostalCode(String postalCode) {
        PortalParam.postalCode = postalCode;
    }

    public static String getTestId() {
        return testId;
    }

    public static void setTestId(String testId) {
        PortalParam.testId = testId;
    }

    public static String getBusinessName() {
        return businessName;
    }

    public static void setBusinessName(String businessName) {
        PortalParam.businessName = businessName;
    }

    public static String getFirstName() {
        return firstName;
    }

    public static void setFirstName(String firstName) {
        PortalParam.firstName = firstName;
    }

    public static String getLastName() {
        return lastName;
    }

    public static void setLastName(String lastName) {
        PortalParam.lastName = lastName;
    }

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        PortalParam.userName = userName;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        PortalParam.password = password;
    }

    public static String getPrimaryPhone() {
        return primaryPhone;
    }

    public static void setPrimaryPhone(String primaryPhone) {
        PortalParam.primaryPhone = primaryPhone;
    }

    public static String getPurposeOfFunds() {
        return purposeOfFunds;
    }

    public static void setPurposeOfFunds(String purposeOfFunds) {
        PortalParam.purposeOfFunds = purposeOfFunds;
    }

    public static String getIndustry() {
        return industry;
    }

    public static void setIndustry(String industry) {
        PortalParam.industry = industry;
    }

    public static String getBusinessAddress() {
        return businessAddress;
    }

    public static void setBusinessAddress(String businessAddress) {
        PortalParam.businessAddress = businessAddress;
    }

    public static String getCity() {
        return city;
    }

    public static void setCity(String city) {
        PortalParam.city = city;
    }


    public static String getBusinessPhone() {
        return businessPhone;
    }

    public static void setBusinessPhone(String businessPhone) {
        PortalParam.businessPhone = businessPhone;
    }

    public static String getTaxId() {
        return taxId;
    }

    public static void setTaxId(String taxId) {
        PortalParam.taxId = taxId;
    }

    public static String getNewTaxId() {
        return newTaxId;
    }

    public static void setNewTaxId(String newTaxId) {
        PortalParam.newTaxId = newTaxId;
    }

    public static String getPersonalName() {
        return personalName;
    }

    public static void setPersonalName(String personalName) {
        PortalParam.personalName = personalName;
    }

    public static String getSsn() {
        return ssn;
    }

    public static void setSsn(String ssn) {
        PortalParam.ssn = ssn;
    }

    public static String getBrowser() {
        return browser;
    }

    public static void setBrowser(String browser) {
        PortalParam.browser = browser;
    }

    public static String getEncryptedPassword() {
        return encryptedPassword;
    }

    public static void setEncryptedPassword(String encryptedPassword) {
        PortalParam.encryptedPassword = encryptedPassword;
    }

    public static String getAmountNeed() {
        return amountNeed;
    }

    public static void setAmountNeed(String amountNeed) {
        PortalParam.amountNeed = amountNeed;
    }

    public static String getDurationNeed() {
        return durationNeed;
    }

    public static void setDurationNeed(String durationNeed) {
        PortalParam.durationNeed = durationNeed;
    }
}
