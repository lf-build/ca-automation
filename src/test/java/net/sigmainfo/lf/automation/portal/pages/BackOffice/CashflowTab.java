package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 01-12-2017.
 */
public class CashflowTab extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(CashflowTab.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public CashflowTab(WebDriver driver) throws Exception {

        this.driver = driver;
        logger.info("=========== CashflowTab is loaded============");
    }
    public CashflowTab(){}
}
