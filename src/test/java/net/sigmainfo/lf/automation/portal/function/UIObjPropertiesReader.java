package net.sigmainfo.lf.automation.portal.function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by shaishav.s on 09-05-2017.
 */
@Component
public class UIObjPropertiesReader {
	
	@Value(value = "${backoffice.appDetailspage.activityLog.statusChanged}")
    private String backoffice_appDetailspage_activityLog_statusChanged;
	
	
	@Value(value = "${backoffice.homepage.actions.BankruptcyDetailsButton}")
    private String backoffice_homepage_actions_BankruptcyDetailsButton;
	
	
	@Value(value = "${backoffice.homepage.actionsButton.bankruptcyButton}")
    private String backoffice_homepage_actionsButton_bankruptcyButton;
	

	@Value(value = "${backoffice.homepage.actionsButton.bankruptcySearchButton}")
    private String backoffice_homepage_actionsButton_bankruptcySearchButton;

    @Value(value = "${borrower.loginpage.loginLabel}")
    private String borrower_loginpage_loginLabel;

    @Value(value = "${borrower.loginpage.emailTextBox}")
    private String borrower_loginpage_emailTextBox;

    @Value(value = "${borrower.loginpage.passwordTextBox}")
    private String borrower_loginpage_passwordTextBox;

    @Value(value = "${borrower.loginpage.submitButton}")
    private String borrower_loginpage_submitButton;

    @Value(value = "${homepage.newApplicationLabel}")
    private String homepage_newApplicationLabel;

    @Value(value = "${homepage.basicTab}")
    private String homepage_basicTab;

    @Value(value = "${homepage.businessTab}")
    private String homepage_businessTab;

    @Value(value = "${homepage.ownerTab}")
    private String homepage_ownerTab;

    @Value(value = "${homepage.finishTab}")
    private String homepage_finishTab;

    @Value(value = "${homepage.basicTab.howMuchDoYouNeedDropdown}")
    private String homepage_basicTab_howMuchDoYouNeedDropdown;

    @Value(value = "${homepage.basicTab.howSoonYouNeedDropdown}")
    private String homepage_basicTab_howSoonYouNeedDropdown;

    @Value(value = "${homepage.basicTab.purposeOfFundsDropdown}")
    private String homepage_basicTab_purposeOfFundsDropdown;

    @Value(value = "${homepage.basicTab.firstNameTextBox}")
    private String homepage_basicTab_firstNameTextBox;

    @Value(value = "${homepage.basicTab.lastNameTextBox}")
    private String homepage_basicTab_lastNameTextBox;

    @Value(value = "${homepage.basicTab.emailAddressTextBox}")
    private String homepage_basicTab_emailAddressTextBox;

    @Value(value = "${homepage.basicTab.primaryPhoneTextBox}")
    private String homepage_basicTab_primaryPhoneTextBox;

    @Value(value = "${homepage.basicTab.passwordTextBox}")
    private String homepage_basicTab_passwordTextBox;

    @Value(value = "${homepage.basicTab.confirmPasswordTextBox}")
    private String homepage_basicTab_confirmPasswordTextBox;

    @Value(value = "${homepage.basicTab.cancelButton}")
    private String homepage_basicTab_cancelButton;

    @Value(value = "${homepage.basicTab.nextButton}")
    private String homepage_basicTab_nextButton;

    @Value(value = "${homepage.businessTab.legalCompanyNameTextBox}")
    private String homepage_businessTab_legalCompanyNameTextBox;

    @Value(value = "${homepage.businessTab.businessAddressTextBox}")
    private String homepage_businessTab_businessAddressTextBox;

    @Value(value = "${homepage.busienssTab.dbaTextBox}")
    private String homepage_businessTab_dbaTextBox;

    @Value(value = "${homepage.businessTab.cityTextBox}")
    private String homepage_businessTab_cityTextBox;

    @Value(value = "${homepage.businessTab.stateDropdown}")
    private String homepage_businessTab_stateDropdown;

    @Value(value = "${homepage.businessTab.postalCodeTextBox}")
    private String homepage_businessTab_postalCodeTextBox;

    @Value(value = "${homepage.businessTab.rentOrOwnDropdown}")
    private String homepage_businessTab_rentOrOwnDropdown;

    @Value(value = "${homepage.businessTab.industryDropdown}")
    private String homepage_businessTab_industryDropdown;

    @Value(value = "${homepage.businessTab.businessTaxIdTextBox}")
    private String homepage_businessTab_businessTaxIdTextBox;
    
    @Value(value = "${homepage.businessTab.Country}")
    private String homepage_businessTab_Country;



	@Value(value = "${homepage.businessTab.entityTypeDropdown}")
    private String homepage_businessTab_entityTypeDropdown;

    @Value(value = "${homepage.businessTab.estMonthDropdown}")
    private String homepage_businessTab_estMonthDropdown;

    @Value(value = "${homepage.businessTab.estDayDropdown}")
    private String homepage_businessTab_estDayDropdown;

    @Value(value = "${homepage.businessTab.estYearDropdown}")
    private String homepage_businessTab_estYearDropdown;

    @Value(value = "${homepage.businessTab.cancelButton}")
    private String homepage_businessTab_cancelButton;

    @Value(value = "${homepage.businessTab.nextButton}")
    private String homepage_businessTab_nextButton;

    @Value(value = "${homepage.ownerTab.firstNameTextBox}")
    private String homepage_ownerTab_firstNameTextBox;

    @Value(value = "${homepage.ownerTab.lastNameTextBox}")
    private String homepage_ownerTab_lastNameTextBox;

    @Value(value = "${homepage.ownerTab.homeAddressTextBox}")
    private String homepage_ownerTab_homeAddressTextBox;

    @Value(value = "${homepage.ownerTab.homeCityTextBox}")
    private String homepage_ownerTab_homeCityTextBox;

    @Value(value = "${homepage.ownerTab.homeStateDropdown}")
    private String homepage_ownerTab_homeStateDropdown;

    @Value(value = "${homepage.ownerTab.postalCodeTextBox}")
    private String homepage_ownerTab_postalCodeTextBox;

    @Value(value = "${homepage.ownerTab.mobilePhoneTextBox}")
    private String homepage_ownerTab_mobilePhoneTextBox;

    @Value(value = "${homepage.ownerTab.ownershipTextBox}")
    private String homepage_ownerTab_ownershipTextBox;

    @Value(value = "${homepage.ownerTab.ssnTextBox}")
    private String homepage_ownerTab_ssnTextBox;

    @Value(value = "${homepage.ownerTab.mobDropdown}")
    private String homepage_ownerTab_mobDropdown;

    @Value(value = "${homepage.ownerTab.dobDropdown}")
    private String homepage_ownerTab_dobDropdown;

    @Value(value = "${homepage.ownerTab.yobDropdown}")
    private String homepage_ownerTab_yobDropdown;

    @Value(value = "${homepage.ownerTab.emailAddressTextBox}")
    private String homepage_ownerTab_emailAddressTextBox;

    @Value(value = "${homepage.ownerTab.cancelButton}")
    private String homepage_ownerTab_cancelButton;

    @Value(value = "${homepage.ownerTab.nextButton}")
    private String homepage_ownerTab_nextButton;
    
    @Value(value = "${homepage.ownerTab.Country}")
    private String homepage_ownerTab_Country;

    
	@Value(value = "${homepage.finishTab.annualRevenueTextBox}")
    private String homepage_finishTab_annualRevenueTextBox;

    @Value(value = "${homepage.finishTab.avgBankBalanceTextBox}")
    private String homepage_finishTab_avgBankBalanceTextBox;

    @Value(value = "${homepage.finishTab.existingLoanStatusDropdown}")
    private String homepage_finishTab_existingLoanStatusDropdown;

    @Value(value = "${homepage.finishTab.agreementCheckBox}")
    private String homepage_finishTab_agreementCheckBox;

    @Value(value = "${homepage.finishTab.cancelButton}")
    private String homepage_finishTab_cancelButton;

    @Value(value = "${homepage.finishTab.submitButton}")
    private String homepage_finishTab_submitButton;

    @Value(value = "${dashboard.overviewTab}")
    private String dashboard_overviewTab;

    @Value(value = "${dashboard.todoTab}")
    private String dashboard_todoTab;

    @Value(value = "${dashboard.profileTab}")
    private String dashboard_profileTab;

    @Value(value = "${dashboard.changePasswordTab}")
    private String dashboard_changePasswordTab;

    @Value(value = "${dashboard.borrowerNameLabel}")
    private String dashboard_borrowerNameLabel;

    @Value(value = "${dashboard.appIdLabel}")
    private String dashboard_appIdLabel;

    @Value(value = "${dashboard.phoneNumberLabel}")
    private String dashboard_phoneNumberLabel;

    @Value(value = "${dashboard.emailIdLabel}")
    private String dashboard_emailIdLabel;

    @Value(value = "${dashboard.loanAmountAppliedLabel}")
    private String dashboard_loanAmountAppliedLabel;

    @Value(value = "${dashboard.purposeOfFundsLabel}")
    private String dashboard_purposeOfFundsLabel;

    @Value(value = "${dashboard.bankVerificationRequestMsg}")
    private String dashboard_bankVerificationRequestMsg;

    @Value(value = "${dashboard.linkBankAccountButton}")
    private String dashboard_linkBankAccountButton;

    @Value(value = "${dashboard.profileDropdown}")
    private String dashboard_profileDropdown;

    @Value(value = "${dashboard.logoutLink}")
    private String dashboard_logoutLink;

    @Value(value = "${backoffice.loginPage.usernameTextBox}")
    private String backoffice_loginPage_usernameTextBox;

    @Value(value = "${backoffice.loginPage.passwordTextBox}")
    private String backoffice_loginPage_passwordTextBox;

    @Value(value = "${backoffice.loginPage.agreementCheckbox}")
    private String backoffice_loginPage_agreementCheckbox;

    @Value(value = "${backoffice.loginPage.forgetPasswordLink}")
    private String backoffice_loginPage_forgetPasswordLink;

    @Value(value = "${backoffice.loginPage.loginButton}")
    private String backoffice_loginPage_loginButton;

    @Value(value = "${backoffice.homepage.dashboardTab}")
    private String backoffice_homepage_dashboardTab;

    @Value(value = "${backoffice.homepage.newAppTab}")
    private String backoffice_homepage_newAppTab;

    @Value(value = "${backoffice.homepage.userManagementTab}")
    private String backoffice_homepage_userManagementTab;

    @Value(value = "${backoffice.homepage.allUsersTab}")
    private String backoffice_homepage_allUsersTab;

    @Value(value = "${backoffice.homepage.createNewUserTab}")
    private String backoffice_homepage_createNewUserTab;

    @Value(value = "${backoffice.homepage.searchTab}")
    private String backoffice_homepage_searchTab;

    @Value(value = "${backoffice.homepage.searchAppTab}")
    private String backoffice_homepage_searchAppTab;

    @Value(value = "${backoffice.homepage.myAppTab}")
    private String backoffice_homepage_myAppTab;

    @Value(value = "${backoffice.homepage.assignedAppsTab}")
    private String backoffice_homepage_assignedAppsTab;

    @Value(value = "${backoffice.homepage.unassignedAppsTab}")
    private String backoffice_homepage_unassignedAppsTab;

    @Value(value = "${backoffice.homepage.applicationsTab}")
    private String backoffice_homepage_applicationsTab;

    @Value(value = "${backoffice.homepage.creditTab}")
    private String backoffice_homepage_creditTab;

    @Value(value = "${backoffice.homepage.incompleteLeadsTab}")
    private String backoffice_homepage_incompleteLeadsTab;

    @Value(value = "${backoffice.homepage.pendingBankLinkingTab}")
    private String backoffice_homepage_pendingBankLinkingTab;

    @Value(value = "${backoffice.homepage.pendingVerificationTab}")
    private String backoffice_homepage_pendingVerificationTab;

    @Value(value = "${backoffice.homepage.underwriterTab}")
    private String backoffice_homepage_underwriterTab;

    @Value(value = "${backoffice.homepage.preApprovedTab}")
    private String backoffice_homepage_preApprovedTab;

    @Value(value = "${backoffice.homepage.offerGenerationTab}")
    private String backoffice_homepage_offerGenerationTab;

    @Value(value = "${backoffice.homepage.docsRequestedTab}")
    private String backoffice_homepage_docsRequestedTab;

    @Value(value = "${backoffice.homepage.agreementDocsOutTab}")
    private String backoffice_homepage_agreementDocsOutTab;

    @Value(value = "${backoffice.homepage.pendingSignTab}")
    private String backoffice_homepage_pendingSignTab;

    @Value(value = "${backoffice.homepage.fundingReviewTab}")
    private String backoffice_homepage_fundingReviewTab;

    @Value(value = "${backoffice.header.profileDropdown}")
    private String backoffice_header_profileDropdown;

    @Value(value = "${backoffice.header.logoutLink}")
    private String backoffice_header_logoutLink;

    @Value(value = "${backoffice.searchpage.applicationNumberTextBox}")
    private String backoffice_searchpage_applicationNumberTextBox;

    @Value(value = "${backoffice.searchpage.businessPhoneTextBox}")
    private String backoffice_searchpage_businessPhoneTextBox;

    @Value(value = "${backoffice.searchpage.firstNameTextBox}")
    private String backoffice_searchpage_firstNameTextBox;

    @Value(value = "${backoffice.searchpage.lastNameTextBox}")
    private String backoffice_searchpage_lastNameTextBox;

    @Value(value = "${backoffice.searchpage.emailTextBox}")
    private String backoffice_searchpage_emailTextBox;

    @Value(value = "${backoffice.searchpage.businessNameTextBox}")
    private String backoffice_searchpage_businessNameTextBox;

    @Value(value = "${backoffice.searchpage.statusDropdown}")
    private String backoffice_searchpage_statusDropdown;

    @Value(value = "${backoffice.searchpage.appDateDropdown}")
    private String backoffice_searchpage_appDateDropdown;

    @Value(value = "${backoffice.searchpage.expDateDropdown}")
    private String backoffice_searchpage_expDateDropdown;

    @Value(value = "${backoffice.searchpage.searchButton}")
    private String backoffice_searchpage_searchButton;

    @Value(value = "${backoffice.searchpage.foundAppIdLabel}")
    private String backoffice_searchpage_foundAppIdLabel;

    @Value(value = "${backoffice.searchpage.foundAppDateLabl}")
    private String backoffice_searchpage_foundAppDateLabl;

    @Value(value = "${backoffice.searchpage.foundBusinessNameLabel}")
    private String backoffice_searchpage_foundBusinessNameLabel;

    @Value(value = "${backoffice.searchpage.foundBusinessPhoneLabel}")
    private String backoffice_searchpage_foundBusinessPhoneLabel;

    @Value(value = "${backoffice.searchpage.foundBusinessLoanAmountLabel}")
    private String backoffice_searchpage_foundBusinessLoanAmountLabel;

    @Value(value = "${backoffice.searchpage.foundBusinessExpiryDateLabel}")
    private String backoffice_searchpage_foundBusinessExpiryDateLabel;

    @Value(value = "${backoffice.searchpage.foundBusinessStatusLabel}")
    private String backoffice_searchpage_foundBusinessStatusLabel;

    @Value(value = "${backoffice.searchpage.viewDetailsButton}")
    private String backoffice_searchpage_viewDetailsButton;

    @Value(value = "${backoffice.appDetailspage.headerStatus}")
    private String backoffice_appDetailspage_headerStatus;

    @Value(value = "${backoffice.appDetailspage.activityLog.email}")
    private String backoffice_appDetailspage_activityLog_email;

    @Value(value = "${backoffice.appDetailspage.activityLog.factVerification}")
    private String backoffice_appDetailspage_activityLog_factVerification;

    @Value(value = "${backoffice.appDetailspage.verificationDashboardTab}")
    private String backoffice_appDetailspage_verificationDashboardTab;

    @Value(value = "${backoffice.appDetailspage.cashflowTab}")
    private String backoffice_appDetailspage_cashflowTab;

    @Value(value = "${selectBank.selectBankLabel}")
    private String selectBank_selectBankLabel;

    @Value(value = "${selectBank.searchBankTextBox}")
    private String selectBank_searchBankTextBox;

    @Value(value = "${selectBank.sunTrustBank}")
    private String selectBank_sunTrustBank;

    @Value(value = "${selectBank.plaidUsernameTextBox}")
    private String selectBank_plaidUsernameTextBox;

    @Value(value = "${selectBank.plaidPasswordTextBox}")
    private String selectBank_plaidPasswordTextBox;

    @Value(value = "${selectBank.submitPlaidButton}")
    private String selectBank_submitPlaidButton;

    @Value(value = "${selectBank.sunTrustResultantBank}")
    private String selectBank_sunTrustResultantBank;

    @Value(value = "${selectBank.selectYourAccountLabel}")
    private String selectBank_selectYourAccountLabel;

    @Value(value = "${selectBank.plaidSavingAccount}")
    private String selectBank_plaidSavingAccount;

    @Value(value = "${selectBank.continueButton}")
    private String selectBank_continueButton;

    @Value(value = "${backoffice.appDetailspage.cashflowTab.editCashflowDropdown}")
    private String backoffice_appDetailspage_cashflowTab_editCashflowDropdown;

    @Value(value = "${backoffice.appDetailspage.cashflowTab.editCashflow.submitButton}")
    private String backoffice_appDetailspage_cashflowTab_editCashflow_submitButton;

    @Value(value = "${backoffice.appDetailspage.cashflowTab.editCashflowLink}")
    private String backoffice_appDetailspage_cashflowTab_editCashflowLink;

    @Value(value = "${backoffice.appDetailspage.cashflowTab.editCashflow.editCashflowSummaryTitle}")
    private String backoffice_appDetailspage_cashflowTab_editCashflow_editCashflowSummaryTitle;

    @Value(value = "${backoffice.appDetailspage.verificationdashboard.searchVerification.notinitiatedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel;

    @Value(value = "${backoffice.appDetailspage.verificationdashboard.searchVerification.initiateButton}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton;

    @Value(value = "${backoffice.appDetailspage.verificationdashboard.searchVerification.weblinkTextBox}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox;

    @Value(value = "${backoffice.appDetailspage.verificationdashboard.searchVerification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.searchVerification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.searchVerification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.searchVerification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.notinitiatedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.initiateButton}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.passVerificationCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.activityLog.cashFlowUpdate}")
    private String backoffice_appDetailspage_activityLog_cashFlowUpdate;

    @Value(value="${backoffice.appDetailspage.activityLog.refreshButton}")
    private String backoffice_appDetailspage_activityLog_refreshButton;

    @Value(value="${backoffice.homepage.actionsButton}")
    private String backoffice_homepage_actionsButton;

    @Value(value="${backoffice.homepage.actions.computeOffer}")
    private String backoffice_homepage_actions_computeOffer;

    @Value(value="${backoffice.homepage.actions.computerOfferButton}")
    private String backoffice_homepage_actions_computerOfferButton;

    @Value(value="${backoffice.searchpage.offerTab}")
    private String backoffice_searchpage_offerTab;

    @Value(value="${backoffice.searchpage.offerTab.dealGenerationLabel}")
    private String backoffice_searchpage_offerTab_dealGenerationLabel;

    @Value(value="${backoffice.searchpage.offerTab.typeOfPaymentDropdown}")
    private String backoffice_searchpage_offerTab_typeOfPaymentDropdown;

    @Value(value="${backoffice.searchpage.offerTab.generateDealButton}")
    private String backoffice_searchpage_offerTab_generateDealButton;

    @Value(value="${backoffice.searchpage.offerTab.dealSelectedLabel}")
    private String backoffice_searchpage_offerTab_dealSelectedLabel;

    @Value(value="${backoffice.homepage.actions.uploadDocsButton}")
    private String backoffice_homepage_actions_uploadDocsButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.idverification.idVerificationLabel}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_idVerificationLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.idScreenshot}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_idScreenshot;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.licenceCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_licenceCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.nameCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_nameCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.dobCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_dobCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.idverification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bankverification.idVerificationLabel}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bankverification.accountNumberTextbox}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bankverification.routingNumberTextbox}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bankverification.nameOfBusinessCheckBox}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bankverification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.bankverification.idScreenshot}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.bankverification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.bankverification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bankverification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_closeButton;

    @Value(value="${backoffice.homepage.actions.sendLoanAgreement}")
    private String backoffice_homepage_actions_sendLoanAgreement;

    @Value(value="${backoffice.homepage.actions.sendMCAAgreementButton}")
    private String backoffice_homepage_actions_sendMCAAgreementButton;

    @Value(value="${docusign.welcomeLabel}")
    private String docusign_welcomeLabel;

    @Value(value="${docusign.agreementCheckbox}")
    private String docusign_agreementCheckbox;

    @Value(value="${docusign.continueButton}")
    private String docusign_continueButton;

    @Value(value="${docusign.startNavigationButton}")
    private String docusign_startNavigationButton;

    @Value(value="${docusign.finishSignButton}")
    private String docusign_finishSignButton;

    @Value(value="${docusign.adopotSignatureLabel}")
    private String docusign_adopotSignatureLabel;

    @Value(value="${docusign.adoptAndSignButton}")
    private String docusign_adoptAndSignButton;

    @Value(value="${docusign.postSignThanksNoteLabel}")
    private String docusign_postSignThanksNoteLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.signedDocverification.idVerificationLabel}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_idVerificationLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.signedDocverification.idScreenshot}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_idScreenshot;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.signedDocverification.allSignatureCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_allSignatureCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.signedDocverification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.signedDocverification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.signedDocverification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.signedDocverification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.welcomeverification.welcomeVerificationLabel}")
    private String backoffice_appDetailspage_verificationdashboard_welcomeverification_welcomeVerificationLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.welcomeverification.acceptTermsCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_welcomeverification_acceptTermsCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.welcomeverification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_welcomeverification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.welcomeverification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.welcomeverification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.welcomeverification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_welcomeverification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.merchantVerificationLabel}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_merchantVerificationLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.idScreenshot}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_idScreenshot;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.addressCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_addressCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.propertyVerificationLabel}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_propertyVerificationLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.idScreenshot}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_idScreenshot;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.pgorrentCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_pgorrentCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.goodstandingCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_goodstandingCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.renewLeaseCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_renewLeaseCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.submitMortgageCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_submitMortgageCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.landlordPhoneTextbox}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_landlordPhoneTextbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.landlordNameTextbox}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_landlordNameTextbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_closeButton;

    @Value(value="${backoffice.homepage.closeUploadDocsButton}")
    private String backoffice_homepage_closeUploadDocsButton;

    @Value(value="${backoffice.homepage.actions.addFundingRequestMenuButton}")
    private String backoffice_homepage_actions_addFundingRequestMenuButton;

    @Value(value="${backoffice.homepage.actions.addFundingRequestButton}")
    private String backoffice_homepage_actions_addFundingRequestButton;

    @Value(value="${backoffice.homepage.actions.editMenuButton}")
    private String backoffice_homepage_actions_editMenuButton;

    @Value(value="${backoffice.appDetailspage.businessinfo.businessName}")
    private String backoffice_appDetailspage_businessinfo_businessName;

    @Value(value="${backoffice.appDetailspage.businessinfo.email}")
    private String backoffice_appDetailspage_businessinfo_email;

    @Value(value="${backoffice.appDetailspage.businessinfo.dba}")
    private String backoffice_appDetailspage_businessinfo_dba;

    @Value(value="${backoffice.appDetailspage.businessinfo.address}")
    private String backoffice_appDetailspage_businessinfo_address;

    @Value(value="${backoffice.appDetailspage.businessinfo.phone}")
    private String backoffice_appDetailspage_businessinfo_phone;

    @Value(value="${backoffice.appDetailspage.businessinfo.reqAmount}")
    private String backoffice_appDetailspage_businessinfo_reqAmount;

    @Value(value="${backoffice.appDetailspage.businessinfo.businessTaxId}")
    private String backoffice_appDetailspage_businessinfo_businessTaxId;

    @Value(value="${backoffice.appDetailspage.businessinfo.annualRevenue}")
    private String backoffice_appDetailspage_businessinfo_annualRevenue;

    @Value(value="${backoffice.appDetailspage.businessinfo.estDate}")
    private String backoffice_appDetailspage_businessinfo_estDate;

    @Value(value="${backoffice.appDetailspage.businessinfo.businessOwnership}")
    private String backoffice_appDetailspage_businessinfo_businessOwnership;

    @Value(value="${backoffice.appDetailspage.businessinfo.avgBankBalance}")
    private String backoffice_appDetailspage_businessinfo_avgBankBalance;

    @Value(value="${backoffice.appDetailspage.businessinfo.existingLoan}")
    private String backoffice_appDetailspage_businessinfo_existingLoan;

    @Value(value="${backoffice.appDetailspage.businessinfo.industry}")
    private String backoffice_appDetailspage_businessinfo_industry;

    @Value(value="${backoffice.appDetailspage.businessinfo.purposeOfFunds}")
    private String backoffice_appDetailspage_businessinfo_purposeOfFunds;

    @Value(value="${backoffice.appDetailspage.businessinfo.entityType}")
    private String backoffice_appDetailspage_businessinfo_entityType;

    @Value(value="${backoffice.appDetailspage.businessinfo.alertsTab}")
    private String backoffice_appDetailspage_businessinfo_alertsTab;

    @Value(value="${backoffice.appDetailspage.businessinfo.alerts.alertName}")
    private String backoffice_appDetailspage_businessinfo_alerts_alertName;

    @Value(value="${backoffice.appDetailspage.businessinfo.alerts.alertDescription}")
    private String backoffice_appDetailspage_businessinfo_alerts_alertDescription;

    @Value(value="${backoffice.appDetailspage.businessinfo.alerts.alertStatus}")
    private String backoffice_appDetailspage_businessinfo_alerts_alertStatus;

    @Value(value="${backoffice.appDetailspage.businessinfo.alerts.alertTag}")
    private String backoffice_appDetailspage_businessinfo_alerts_alertTag;

    @Value(value="${backoffice.appDetailspage.businessinfo.alerts.dismissButton}")
    private String backoffice_appDetailspage_businessinfo_alerts_dismissButton;

    @Value(value="${backoffice.appDetailspage.activityLog.declineUpdate}")
    private String backoffice_appDetailspage_activityLog_declineUpdate;

    @Value(value="${backoffice.appDetailspage.scoringTab}")
    private String backoffice_appDetailspage_scoringTab;

    @Value(value="${backoffice.appDetailspage.scoringTab.ruleExecutionTab}")
    private String backoffice_appDetailspage_scoringTab_ruleExecutionTab;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.creditScoreVerification.notinitiatedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.creditScoreVerification.pscoreTab}")
    private String backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.creditScoreVerification.pscoreValueLabel}")
    private String backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel;

    @Value(value="${backoffice.appDetailspage.scoringTab.ruleExecutionTab.pScoreTab}")
    private String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab;

    @Value(value="${backoffice.appDetailspage.scoringTab.ruleExecutionTab.pScoreTab.intermediateTab}")
    private String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab;

    @Value(value="${backoffice.appDetailspage.scoringTab.ruleExecutionTab.pScoreTab.sourceTab}")
    private String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab;

    @Value(value="${backoffice.appDetailspage.scoringTab.ruleExecutionTab.pScoreTab.intermediateTab.pScoreLabel}")
    private String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab_pScoreLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.creditScoreVerification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton;

    @Value(value="${backoffice.appDetailspage.scoringTab.ruleExecutionTab.pScoreTab.sourceTab.modelTab}")
    private String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab_modelTab;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.restrictedIndustryVerification.notVerifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_notVerifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.restrictedIndustryVerification.passCheckBox}")
    private String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_passCheckBox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.restrictedIndustryVerification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.restrictedIndustryVerification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.restrictedIndustryVerification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.cashflowTab.uploadBankStatement.bankNameTextBox}")
    private String backoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox;

    @Value(value="${backoffice.appDetailspage.cashflowTab.uploadBankStatement.accountNumberTextBox}")
    private String backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox;

    @Value(value="${backoffice.appDetailspage.cashflowTab.uploadBankStatement.accountTypeComboBox}")
    private String backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox;

    @Value(value="${backoffice.appDetailspage.cashflowTab.uploadBankStatement.selectFileButton}")
    private String backoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton;

    @Value(value="${backoffice.appDetailspage.cashflowTab.uploadBankStatement.submitFileButton}")
    private String backoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton;

    @Value(value="${backoffice.homepage.actions.uploadBankStatementButton}")
    private String backoffice_homepage_actions_uploadBankStatementButton;

    @Value(value="${backoffice.appDetailspage.activityLog.bankLinkedUpdate}")
    private String backoffice_appDetailspage_activityLog_bankLinkedUpdate;

    @Value(value="${backoffice.appDetailspage.cashflowTab.editCashflowDropdown.setAsFundingAccount}")
    private String backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount;

    @Value(value="${backoffice.appDetailspage.cashflowTab.editCashflowDropdown.setAsCashflowAccount}")
    private String backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount;

    @Value(value="${backoffice.searchpage.productIdTextBox}")
    private String backoffice_searchpage_productIdTextBox;

    @Value(value="${backoffice.appDetailspage.cashflowTab.setAsCashflowAccountLink}")
    private  String backoffice_appDetailspage_cashflowTab_setAsCashflowAccountLink;

    @Value(value="${backoffice.appDetailspage.businessinfo.leadSource}")
    private  String backoffice_appDetailspage_businessinfo_leadSource;

    @Value(value="${backoffice.appDetailspage.verificationDashboard}")
    private String backoffice_appDetailspage_verificationDashboard;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.annualRevenueDetailsVerification}")
    private String backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.annualRevenueDetailsVerification.CheckBox}")
    private String backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_CheckBox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.annualRevenueDetailsVerification.VerifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.annualRevenueDetailsVerification.VerificationStatus}")
    private String backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.annualRevenueDetailsVerification.VerificationPopUpClose}")
    private String backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationPopUpClose;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.timeInBusinessVerification.verifystatus}")
    private String backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifystatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.timeInBusinessVerification.verificationdetails}")
    private String backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationdetails;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.timeInBusinessVerification.verificationcheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.timeInBusinessVerification.verifybutton}")
    private String backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifybutton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.timeInBusinessVerification.verifyclosebutton}")
    private String backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifyclosebutton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.addressVerification.verifystatus}")
    private String backoffice_appDetailspage_verificationdashboard_addressVerification_verifystatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.addressVerification.verifyDetails}")
    private String backoffice_appDetailspage_verificationdashboard_addressVerification_verifyDetails;

    @Value(value="${backoffice.homepage.actions.uploadDocsCloseButton}")
    private  String backoffice_homepage_actions_uploadDocsCloseButton;


    @Value(value="${backoffice.appDetailspage.verificationdashboard.addressVerification.verifypopupclose}")
    private String backoffice_appDetailspage_verificationdashboard_addressVerification_verifypopupclose;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.dataMerchNoVerification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.dataMerchNoVerification.verifyCloseButton}")
    private String backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyCloseButton;

    @Value(value="${backoffice.appDetailspage.3rdPartyData.personalReportTab.experianPersonalReport}")
    private String backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport;

    @Value(value="${backoffice.appDetailspage.3rdPartyData.personalReportTab.experianPersonalReport.firstName}")
    private String backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_firstName;

    @Value(value="${backoffice.appDetailspage.3rdPartyData.personalReportTab.experianPersonalReport.frozeMessage}")
    private String backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage;

    @Value(value="${backoffice.appDetailspage.3rdPartyData.personalReportTab.experianPersonalReport.noRecordMessage}")
    private String backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage;
    
    @Value(value="${backoffice.homepage.actions.businessCreditReport}")
    private String backoffice_homepage_actions_businessCreditReport;

    @Value(value="${backoffice.homepage.actions.businessCreditReportButton}")
    private String backoffice_homepage_actions_businessCreditReportButton;
    
    @Value(value="${backoffice.appDetailspage.3rdPartyData.ruleExecution.bp2Attributes}")
    private String backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes;
    
    @Value(value="${backoffice.appDetailspage.3rdPartyData.ruleExecution.bp2Attributes.source}")
    private String backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source;
    
    @Value(value="${backoffice.appDetailspage.3rdPartyData.ruleExecution.bp2Attributes.Intermediate}")
    private String backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate;
    
    @Value(value="${backoffice.appDetailspage.3rdPartyData.ruleExecution.bp2Attributes.bp2score}")
    private String backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score;
    
    @Value(value="${backoffice.homepage.actions.businessCreditReportCallExperian}")
    private String backoffice_homepage_actions_businessCreditReportCallExperian;
    
    @Value(value="${backoffice.homepage.actions.businessCreditReportCallRecord}")
    private String backoffice_homepage_actions_businessCreditReportCallRecord;
    @Value(value="${backoffice.homepage.actions.editButton}")
    private String  backoffice_homepage_actions_editButton;
    
    @Value(value="${backoffice.homepage.actions.editButton.edit}")
    private String  backoffice_homepage_actions_editButton_edit;
    
    @Value(value="${backoffice.homepage.actions.editButton.edit.contactName}")
    private String  backoffice_homepage_actions_editButton_edit_contactName;
    
    @Value(value="${backoffice.homepage.actions.editButton.edit.contactPhone}")
    private String  backoffice_homepage_actions_editButton_edit_contactPhone;
    
    @Value(value="${backoffice.homepage.actions.editButton.edit.saveChanges}")
    private String  backoffice_homepage_actions_editButton_edit_saveChanges;
    
    @Value(value="${backoffice.homepage.actions.endiButton.edit.businessTab}")
    private String  backoffice_homepage_actions_endiButton_edit_businessTab;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.businessTab.legalBusinessName}")
    private String  backoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName;

    @Value(value="${backoffice.homepage.actions.endiButton.edit.businessTab.businessTaxId}")
    private String  backoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.businessTab.SICCode}")
    private String  backoffice_homepage_actions_endiButton_edit_businessTab_SICCode;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab}")
    private String  backoffice_homepage_actions_endiButton_edit_ownerTab;
    
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner}")
    private String  backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.firstName}")
    private String  backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.lastName}")
    private String  backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName;
 
    @Value(value="${backoffice.homepage.actions.editButton.edit.Ownerdetails}")
    private String  backoffice_homepage_actions_editButton_edit_Ownerdetails;
    
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.address}")
    private String  backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.city}")
    private String  backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.state}")
    private String   backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.postalCode}")
    private String   backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.mobileNumber}")
    private String   backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber;
  
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.DOB}")
    private String   backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB;
    
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.email}")
    private String   backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.SSN}")
    private String   backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.ownership}")
    private String   backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.selectDay}")
    private String   backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_selectDay;
 
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.closeButton}")
    private String   backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton;
    
    @Value(value="${backoffice.newAppTab.Basic.howMuchDoYouNeed}")
    private String backoffice_newAppTab_Basic_howMuchDoYouNeed;
    
    @Value(value="${backoffice.newAppTab.Basic.howSoonDoYouNeedIt}")
    private String backoffice_newAppTab_Basic_howSoonDoYouNeedIt;
    
    @Value(value="${backoffice.newAppTab.Basic.purposeOfLoan}")
    private String backoffice_newAppTab_Basic_purposeOfLoan;
    
    @Value(value="${backoffice.newAppTab.Basic.contactFirstName}")
    private String backoffice_newAppTab_Basic_contactFirstName;
    
    @Value(value="${backoffice.newAppTab.Basic.contactLastName}")
    private String backoffice_newAppTab_Basic_contactLastName;
    
    @Value(value="${backoffice.newAppTab.Basic.phoneNumber}")
    private String backoffice_newAppTab_Basic_phoneNumber;
    
    @Value(value="${backoffice.newAppTab.Basic.email}")
    private String backoffice_newAppTab_Basic_email;
    
    @Value(value="${backoffice.newapplication.Basic.saveAndContinue}")
    private String backoffice_newAppTab_Basic_saveAndContinue;
    
    @Value(value="${backoffice.newAppTab.Business.legalBusinessName}")
    private String backoffice_newAppTab_Business_legalBusinessName;
    
    @Value(value="${backoffice.newAppTab.Business.dba}")
    private String backoffice_newAppTab_Business_dba;
    
    @Value(value="${backoffice.newAppTab.Business.businessAddress}")
    private String backoffice_newAppTab_Business_businessAddress;
    
    @Value(value="${backoffice.newAppTab.Business.city}")
    private String backoffice_newAppTab_Business_city;
    
    @Value(value="${backoffice.newAppTab.Business.postalCode}")
    private String backoffice_newAppTab_Business_postalCode;
    
    @Value(value="${backoffice.newAppTab.Business.rentorown}")
    private String backoffice_newAppTab_Business_rentorown;
    
    @Value(value="${backoffice.newAppTab.Business.industry}")
    private String backoffice_newAppTab_Business_industry;
    
    @Value(value="${backoffice.newAppTab.Business.businessTaxId}")
    private String backoffice_newAppTab_Business_businessTaxId;
    
    @Value(value="${backoffice.newAppTab.Business.sicCode}")
    private String backoffice_newAppTab_Business_sicCode;
    
    @Value(value="${backoffice.newAppTab.Business.entityType}")
    private String backoffice_newAppTab_Business_entityType;
    
    @Value(value="${backoffice.newAppTab.Business.country}")
    private String backoffice_newAppTab_Business_country;
    
   

	@Value(value="${backoffice.newAppTab.Business.dateEstablished}")
    private String backoffice_newAppTab_Business_dateEstablished;
    
     @Value(value="${backoffice.newAppTab.Business.saveAndContinue}")
    private String backoffice_newAppTab_Business_saveAndContinue;
     

 	@Value(value="${backoffice.newAppTab.Business.state}")
      private String backoffice_newAppTab_Business_state;
 	
 	 @Value(value="${backoffice.newAppTab.Owner.firstName}")
 	 private String backoffice_newAppTab_Owner_firstName;
 	 
 	 @Value(value="${backoffice.newAppTab.Owner.lastName}")
 	 private String backoffice_newAppTab_Owner_lastName;
 	 
 	 @Value(value="${backoffice.newAppTab.Owner.homeAddress}")
 	 private String backoffice_newAppTab_Owner_homeAddress;
 	 
 	 @Value(value="${backoffice.newAppTab.Owner.city}")
 	 private String backoffice_newAppTab_Owner_city;
 	 
 	 @Value(value="${backoffice.newAppTab.Owner.state}")
 	 private String backoffice_newAppTab_Owner_state;
 	 
 	 @Value(value="${backoffice.newAppTab.Owner.postalCode}")
 	 private String backoffice_newAppTab_Owner_postalCode;
 	 
 	 @Value(value="${backoffice.newAppTab.Owner.mobilePhone}")
 	 private String backoffice_newAppTab_Owner_mobilePhone;
 	 
 	 @Value(value="${backoffice.newAppTab.Owner.email}")
 	 private String backoffice_newAppTab_Owner_email;
 	 
 	 @Value(value="${backoffice.newAppTab.Owner.OfOwnership}")
 	 private String backoffice_newAppTab_Owner_OfOwnership;
 	 
 	 
 	 @Value(value="${backoffice.newAppTab.Owner.ssn}")
 	 private String backoffice_newAppTab_Owner_ssn;
 	 
 	 @Value(value="${backoffice.newAppTab.Owner.country}")
	 private String backoffice_newAppTab_Owner_country;
	 
 	
	@Value(value="${backoffice.newAppTab.Owner.dob}")
 	 private String backoffice_newAppTab_Owner_dob;
 	 
 	 
 	  @Value(value="${backoffice.newAppTab.Owner.saveAndContinue}")
 	 private String backoffice_newAppTab_Owner_saveAndContinue;
     
 	 @Value(value="${backoffice.newAppTab.Finish.annualRevenue}")
 	 private String backoffice_newAppTab_Finish_annualRevenue;
 	 
 	 @Value(value="${backoffice.newAppTab.Finish.averageBankBalances}")
 	 private String backoffice_newAppTab_Finish_averageBankBalances;
 	 
 	 @Value(value="${backoffice.newAppTab.Finish.checkBox}")
 	 private String backoffice_newAppTab_Finish_checkBox;
 	 
 	 @Value(value="${backoffice.newAppTab.Finish.submit}")
 	 private String backoffice_newAppTab_Finish_submit;
 	 
 	 @Value(value="${backoffice.newAppTab.Business.dateEstablished.popup}")
 	 private String backoffice_newAppTab_Business_dateEstablished_popup;
 	 
 	@Value(value="${backoffice.newAppTab.Business.dateEstablished.selectDate}")
	 private String backoffice_newAppTab_Business_dateEstablished_selectDate;

    @Value(value="${backoffice.homepage.actions.editButton.sendPendingEmail}")
    private String   backoffice_homepage_actions_editButton_sendPendingEmail;
    
    @Value(value="${backoffice.homepage.actions.editButton.sendPendingEmail.sendPendingEmailButton}")
    private String   backoffice_homepage_actions_editButton_sendPendingEmail_sendPendingEmailButton;
 
    @Value(value="${backoffice.searchpage.offerTab.editDealButton}")
    private String   backoffice_searchpage_offerTab_editDealButton;
    
    @Value(value="${backoffice.searchpage.offerTab.editAmount}")
    private String   backoffice_searchpage_offerTab_editAmount;
 
    @Value(value="${backoffice.searchpage.offerTab.editOriginationFee}")
    private String   backoffice_searchpage_offerTab_editOriginationFee;
    
    @Value(value="${backoffice.searchpage.offerTab.selectedAmount}")
    private String   backoffice_searchpage_offerTab_selectedAmount;
 
    @Value(value="${backoffice.searchpage.offerTab.selectedOriginationFee}")
    private String   backoffice_searchpage_offerTab_selectedOriginationFee;
    
    @Value(value="${backoffice.homepage.actions.switchOwner}")
    private String  backoffice_homepage_actions_switchOwner;
  
    @Value(value="${backoffice.homepage.actions.switchOwner.message}")
    private String  backoffice_homepage_actions_switchOwner_message;
  
    @Value(value="${backoffice.homepage.actions.switchOwner.closeButton}")
    private String  backoffice_homepage_actions_switchOwner_closeButton;
    
    @Value(value="${backoffice.homepage.actions.switchOwner.newOwner}")
    private String backoffice_homepage_actions_switchOwner_newOwner;
 
    @Value(value="${backoffice.homepage.actions.switchOwner.AllOwners}")
    private String backoffice_homepage_actions_switchOwner_AllOwners;
 
    @Value(value="${backoffice.homepage.actions.switchOwner.switchOwnerButton}")
    private String backoffice_homepage_actions_switchOwner_switchOwnerButton;
 
    @Value(value="${backoffice.homepage.actions.switchOwner.switchActivity}")
    private String backoffice_homepage_actions_switchOwner_switchActivity;
    
    @Value(value="${backoffice.homepage.actions.endiButton.edit.ownerTab.addSecondOwner.country}")
    private String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country;
    
    @Value(value="${backoffice.homepage.actionButton.reapplyAction}")
    private String backoffice_homepage_actionButton_reapplyAction;
    
    @Value(value="${backoffice.homepage.actionButton.reapplyButton}")
    private String backoffice_homepage_actionButton_reapplyButton;
    
    @Value(value="${borrower.todo.reapplyButton}")
    private String borrower_todo_reapplyButton;
    
    @Value(value="${borrower.todo.reapplyButton.yes}")
    private String borrower_todo_reapplyButton_yes;
 
    
    public String getBorrower_todo_reapplyButton() {
		return borrower_todo_reapplyButton;
	}

	public void setBorrower_todo_reapplyButton(String borrower_todo_reapplyButton) {
		this.borrower_todo_reapplyButton = borrower_todo_reapplyButton;
	}

	public String getBorrower_todo_reapplyButton_yes() {
		return borrower_todo_reapplyButton_yes;
	}

	public void setBorrower_todo_reapplyButton_yes(String borrower_todo_reapplyButton_yes) {
		this.borrower_todo_reapplyButton_yes = borrower_todo_reapplyButton_yes;
	}

	public String getBackoffice_homepage_actionButton_reapplyAction() {
		return backoffice_homepage_actionButton_reapplyAction;
	}

	public void setBackoffice_homepage_actionButton_reapplyAction(String backoffice_homepage_actionButton_reapplyAction) {
		this.backoffice_homepage_actionButton_reapplyAction = backoffice_homepage_actionButton_reapplyAction;
	}

	public String getBackoffice_homepage_actionButton_reapplyButton() {
		return backoffice_homepage_actionButton_reapplyButton;
	}

	public void setBackoffice_homepage_actionButton_reapplyButton(String backoffice_homepage_actionButton_reapplyButton) {
		this.backoffice_homepage_actionButton_reapplyButton = backoffice_homepage_actionButton_reapplyButton;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country;
	}

	public String getBackoffice_homepage_actions_switchOwner_switchActivity() {
		return backoffice_homepage_actions_switchOwner_switchActivity;
	}

	public void setBackoffice_homepage_actions_switchOwner_switchActivity(
			String backoffice_homepage_actions_switchOwner_switchActivity) {
		this.backoffice_homepage_actions_switchOwner_switchActivity = backoffice_homepage_actions_switchOwner_switchActivity;
	}

	public String getBackoffice_homepage_actions_switchOwner_switchOwnerButton() {
		return backoffice_homepage_actions_switchOwner_switchOwnerButton;
	}

	public void setBackoffice_homepage_actions_switchOwner_switchOwnerButton(
			String backoffice_homepage_actions_switchOwner_switchOwnerButton) {
		this.backoffice_homepage_actions_switchOwner_switchOwnerButton = backoffice_homepage_actions_switchOwner_switchOwnerButton;
	}

	public String getBackoffice_homepage_actions_switchOwner_AllOwners() {
		return backoffice_homepage_actions_switchOwner_AllOwners;
	}

	public void setBackoffice_homepage_actions_switchOwner_AllOwners(
			String backoffice_homepage_actions_switchOwner_AllOwners) {
		this.backoffice_homepage_actions_switchOwner_AllOwners = backoffice_homepage_actions_switchOwner_AllOwners;
	}

	public String getBackoffice_homepage_actions_switchOwner_newOwner() {
		return backoffice_homepage_actions_switchOwner_newOwner;
	}

	public void setBackoffice_homepage_actions_switchOwner_newOwner(
			String backoffice_homepage_actions_switchOwner_newOwner) {
		this.backoffice_homepage_actions_switchOwner_newOwner = backoffice_homepage_actions_switchOwner_newOwner;
	}

	public String getBackoffice_homepage_actions_switchOwner_closeButton() {
		return backoffice_homepage_actions_switchOwner_closeButton;
	}

	public void setBackoffice_homepage_actions_switchOwner_closeButton(
			String backoffice_homepage_actions_switchOwner_closeButton) {
		this.backoffice_homepage_actions_switchOwner_closeButton = backoffice_homepage_actions_switchOwner_closeButton;
	}

	public String getBackoffice_homepage_actions_switchOwner_message() {
		return backoffice_homepage_actions_switchOwner_message;
	}

	public void setBackoffice_homepage_actions_switchOwner_message(String backoffice_homepage_actions_switchOwner_message) {
		this.backoffice_homepage_actions_switchOwner_message = backoffice_homepage_actions_switchOwner_message;
	}

	public String getBackoffice_homepage_actions_switchOwner() {
		return backoffice_homepage_actions_switchOwner;
	}

	public void setBackoffice_homepage_actions_switchOwner(String backoffice_homepage_actions_switchOwner) {
		this.backoffice_homepage_actions_switchOwner = backoffice_homepage_actions_switchOwner;
	}
    
    public String getBackoffice_searchpage_offerTab_selectedOriginationFee() {
		return backoffice_searchpage_offerTab_selectedOriginationFee;
	}

	public void setBackoffice_searchpage_offerTab_selectedOriginationFee(
			String backoffice_searchpage_offerTab_selectedOriginationFee) {
		this.backoffice_searchpage_offerTab_selectedOriginationFee = backoffice_searchpage_offerTab_selectedOriginationFee;
	}

	public String getBackoffice_searchpage_offerTab_selectedAmount() {
		return backoffice_searchpage_offerTab_selectedAmount;
	}

	public void setBackoffice_searchpage_offerTab_selectedAmount(String backoffice_searchpage_offerTab_selectedAmount) {
		this.backoffice_searchpage_offerTab_selectedAmount = backoffice_searchpage_offerTab_selectedAmount;
	}

	public String getBackoffice_searchpage_offerTab_editOriginationFee() {
		return backoffice_searchpage_offerTab_editOriginationFee;
	}

	public void setBackoffice_searchpage_offerTab_editOriginationFee(
			String backoffice_searchpage_offerTab_editOriginationFee) {
		this.backoffice_searchpage_offerTab_editOriginationFee = backoffice_searchpage_offerTab_editOriginationFee;
	}

	public String getBackoffice_searchpage_offerTab_editAmount() {
		return backoffice_searchpage_offerTab_editAmount;
	}

	public void setBackoffice_searchpage_offerTab_editAmount(String backoffice_searchpage_offerTab_editAmount) {
		this.backoffice_searchpage_offerTab_editAmount = backoffice_searchpage_offerTab_editAmount;
	}

	public String getBackoffice_searchpage_offerTab_editDealButton() {
		return backoffice_searchpage_offerTab_editDealButton;
	}

	public void setBackoffice_searchpage_offerTab_editDealButton(String backoffice_searchpage_offerTab_editDealButton) {
		this.backoffice_searchpage_offerTab_editDealButton = backoffice_searchpage_offerTab_editDealButton;
	}

	public String getBackoffice_homepage_actions_editButton_sendPendingEmail_sendPendingEmailButton() {
		return backoffice_homepage_actions_editButton_sendPendingEmail_sendPendingEmailButton;
	}

	public void setBackoffice_homepage_actions_editButton_sendPendingEmail_sendPendingEmailButton(
			String backoffice_homepage_actions_editButton_sendPendingEmail_sendPendingEmailButton) {
		this.backoffice_homepage_actions_editButton_sendPendingEmail_sendPendingEmailButton = backoffice_homepage_actions_editButton_sendPendingEmail_sendPendingEmailButton;
	}

	public String getBackoffice_homepage_actions_editButton_sendPendingEmail() {
		return backoffice_homepage_actions_editButton_sendPendingEmail;
	}

	public void setBackoffice_homepage_actions_editButton_sendPendingEmail(
			String backoffice_homepage_actions_editButton_sendPendingEmail) {
		this.backoffice_homepage_actions_editButton_sendPendingEmail = backoffice_homepage_actions_editButton_sendPendingEmail;
	}

    	 
    public String getBackoffice_newAppTab_Business_dateEstablished_selectDate() {
		return backoffice_newAppTab_Business_dateEstablished_selectDate;
	}

	public void setBackoffice_newAppTab_Business_dateEstablished_selectDate(
			String backoffice_newAppTab_Business_dateEstablished_selectDate) {
		this.backoffice_newAppTab_Business_dateEstablished_selectDate = backoffice_newAppTab_Business_dateEstablished_selectDate;
	}

	public String getBackoffice_newAppTab_Business_dateEstablished_popup() {
		return backoffice_newAppTab_Business_dateEstablished_popup;
	}

	public void setBackoffice_newAppTab_Business_dateEstablished_popup(
			String backoffice_newAppTab_Business_dateEstablished_popup) {
		this.backoffice_newAppTab_Business_dateEstablished_popup = backoffice_newAppTab_Business_dateEstablished_popup;
	}

	public String getBackoffice_newAppTab_Basic_howSoonDoYouNeedIt() {
		return backoffice_newAppTab_Basic_howSoonDoYouNeedIt;
	}

	public void setBackoffice_newAppTab_Basic_howSoonDoYouNeedIt(String backoffice_newAppTab_Basic_howSoonDoYouNeedIt) {
		this.backoffice_newAppTab_Basic_howSoonDoYouNeedIt = backoffice_newAppTab_Basic_howSoonDoYouNeedIt;
	}

	public String getBackoffice_newAppTab_Basic_purposeOfLoan() {
		return backoffice_newAppTab_Basic_purposeOfLoan;
	}

	public void setBackoffice_newAppTab_Basic_purposeOfLoan(String backoffice_newAppTab_Basic_purposeOfLoan) {
		this.backoffice_newAppTab_Basic_purposeOfLoan = backoffice_newAppTab_Basic_purposeOfLoan;
	}

	public String getBackoffice_newAppTab_Basic_contactFirstName() {
		return backoffice_newAppTab_Basic_contactFirstName;
	}

	public void setBackoffice_newAppTab_Basic_contactFirstName(String backoffice_newAppTab_Basic_contactFirstName) {
		this.backoffice_newAppTab_Basic_contactFirstName = backoffice_newAppTab_Basic_contactFirstName;
	}

	public String getBackoffice_newAppTab_Basic_contactLastName() {
		return backoffice_newAppTab_Basic_contactLastName;
	}

	public void setBackoffice_newAppTab_Basic_contactLastName(String backoffice_newAppTab_Basic_contactLastName) {
		this.backoffice_newAppTab_Basic_contactLastName = backoffice_newAppTab_Basic_contactLastName;
	}

	public String getBackoffice_newAppTab_Basic_phoneNumber() {
		return backoffice_newAppTab_Basic_phoneNumber;
	}

	public void setBackoffice_newAppTab_Basic_phoneNumber(String backoffice_newAppTab_Basic_phoneNumber) {
		this.backoffice_newAppTab_Basic_phoneNumber = backoffice_newAppTab_Basic_phoneNumber;
	}

	public String getBackoffice_newAppTab_Basic_email() {
		return backoffice_newAppTab_Basic_email;
	}

	public void setBackoffice_newAppTab_Basic_email(String backoffice_newAppTab_Basic_email) {
		this.backoffice_newAppTab_Basic_email = backoffice_newAppTab_Basic_email;
	}

	public String getBackoffice_newAppTab_Basic_saveAndContinue() {
		return backoffice_newAppTab_Basic_saveAndContinue;
	}

	public void setBackoffice_newAppTab_Basic_saveAndContinue(String backoffice_newAppTab_Basic_saveAndContinue) {
		this.backoffice_newAppTab_Basic_saveAndContinue = backoffice_newAppTab_Basic_saveAndContinue;
	}

	public String getBackoffice_newAppTab_Business_legalBusinessName() {
		return backoffice_newAppTab_Business_legalBusinessName;
	}

	public void setBackoffice_newAppTab_Business_legalBusinessName(String backoffice_newAppTab_Business_legalBusinessName) {
		this.backoffice_newAppTab_Business_legalBusinessName = backoffice_newAppTab_Business_legalBusinessName;
	}

	public String getBackoffice_newAppTab_Business_dba() {
		return backoffice_newAppTab_Business_dba;
	}

	public void setBackoffice_newAppTab_Business_dba(String backoffice_newAppTab_Business_dba) {
		this.backoffice_newAppTab_Business_dba = backoffice_newAppTab_Business_dba;
	}

	public String getBackoffice_newAppTab_Business_businessAddress() {
		return backoffice_newAppTab_Business_businessAddress;
	}

	public void setBackoffice_newAppTab_Business_businessAddress(String backoffice_newAppTab_Business_businessAddress) {
		this.backoffice_newAppTab_Business_businessAddress = backoffice_newAppTab_Business_businessAddress;
	}

	public String getBackoffice_newAppTab_Business_city() {
		return backoffice_newAppTab_Business_city;
	}

	public void setBackoffice_newAppTab_Business_city(String backoffice_newAppTab_Business_city) {
		this.backoffice_newAppTab_Business_city = backoffice_newAppTab_Business_city;
	}

	public String getBackoffice_newAppTab_Business_postalCode() {
		return backoffice_newAppTab_Business_postalCode;
	}

	public void setBackoffice_newAppTab_Business_postalCode(String backoffice_newAppTab_Business_postalCode) {
		this.backoffice_newAppTab_Business_postalCode = backoffice_newAppTab_Business_postalCode;
	}

	public String getBackoffice_newAppTab_Business_rentorown() {
		return backoffice_newAppTab_Business_rentorown;
	}

	public void setBackoffice_newAppTab_Business_rentorown(String backoffice_newAppTab_Business_rentorown) {
		this.backoffice_newAppTab_Business_rentorown = backoffice_newAppTab_Business_rentorown;
	}

	public String getBackoffice_newAppTab_Business_industry() {
		return backoffice_newAppTab_Business_industry;
	}

	public void setBackoffice_newAppTab_Business_industry(String backoffice_newAppTab_Business_industry) {
		this.backoffice_newAppTab_Business_industry = backoffice_newAppTab_Business_industry;
	}

	public String getBackoffice_newAppTab_Business_businessTaxId() {
		return backoffice_newAppTab_Business_businessTaxId;
	}

	public void setBackoffice_newAppTab_Business_businessTaxId(String backoffice_newAppTab_Business_businessTaxId) {
		this.backoffice_newAppTab_Business_businessTaxId = backoffice_newAppTab_Business_businessTaxId;
	}

	public String getBackoffice_newAppTab_Business_sicCode() {
		return backoffice_newAppTab_Business_sicCode;
	}

	public void setBackoffice_newAppTab_Business_sicCode(String backoffice_newAppTab_Business_sicCode) {
		this.backoffice_newAppTab_Business_sicCode = backoffice_newAppTab_Business_sicCode;
	}

	public String getBackoffice_newAppTab_Business_entityType() {
		return backoffice_newAppTab_Business_entityType;
	}

	public void setBackoffice_newAppTab_Business_entityType(String backoffice_newAppTab_Business_entityType) {
		this.backoffice_newAppTab_Business_entityType = backoffice_newAppTab_Business_entityType;
	}

	public String getBackoffice_newAppTab_Business_dateEstablished() {
		return backoffice_newAppTab_Business_dateEstablished;
	}

	public void setBackoffice_newAppTab_Business_dateEstablished(String backoffice_newAppTab_Business_dateEstablished) {
		this.backoffice_newAppTab_Business_dateEstablished = backoffice_newAppTab_Business_dateEstablished;
	}

	public String getBackoffice_newAppTab_Business_saveAndContinue() {
		return backoffice_newAppTab_Business_saveAndContinue;
	}

	public void setBackoffice_newAppTab_Business_saveAndContinue(String backoffice_newAppTab_Business_saveAndContinue) {
		this.backoffice_newAppTab_Business_saveAndContinue = backoffice_newAppTab_Business_saveAndContinue;
	}

	public String getBackoffice_newAppTab_Business_state() {
		return backoffice_newAppTab_Business_state;
	}

	public void setBackoffice_newAppTab_Business_state(String backoffice_newAppTab_Business_state) {
		this.backoffice_newAppTab_Business_state = backoffice_newAppTab_Business_state;
	}

	public String getBackoffice_newAppTab_Owner_firstName() {
		return backoffice_newAppTab_Owner_firstName;
	}

	public void setBackoffice_newAppTab_Owner_firstName(String backoffice_newAppTab_Owner_firstName) {
		this.backoffice_newAppTab_Owner_firstName = backoffice_newAppTab_Owner_firstName;
	}

	public String getBackoffice_newAppTab_Owner_lastName() {
		return backoffice_newAppTab_Owner_lastName;
	}

	public void setBackoffice_newAppTab_Owner_lastName(String backoffice_newAppTab_Owner_lastName) {
		this.backoffice_newAppTab_Owner_lastName = backoffice_newAppTab_Owner_lastName;
	}

	public String getBackoffice_newAppTab_Owner_homeAddress() {
		return backoffice_newAppTab_Owner_homeAddress;
	}

	public void setBackoffice_newAppTab_Owner_homeAddress(String backoffice_newAppTab_Owner_homeAddress) {
		this.backoffice_newAppTab_Owner_homeAddress = backoffice_newAppTab_Owner_homeAddress;
	}

	public String getBackoffice_newAppTab_Owner_city() {
		return backoffice_newAppTab_Owner_city;
	}

	public void setBackoffice_newAppTab_Owner_city(String backoffice_newAppTab_Owner_city) {
		this.backoffice_newAppTab_Owner_city = backoffice_newAppTab_Owner_city;
	}

	public String getBackoffice_newAppTab_Owner_state() {
		return backoffice_newAppTab_Owner_state;
	}

	public void setBackoffice_newAppTab_Owner_state(String backoffice_newAppTab_Owner_state) {
		this.backoffice_newAppTab_Owner_state = backoffice_newAppTab_Owner_state;
	}

	public String getBackoffice_newAppTab_Owner_postalCode() {
		return backoffice_newAppTab_Owner_postalCode;
	}

	public void setBackoffice_newAppTab_Owner_postalCode(String backoffice_newAppTab_Owner_postalCode) {
		this.backoffice_newAppTab_Owner_postalCode = backoffice_newAppTab_Owner_postalCode;
	}

	public String getBackoffice_newAppTab_Owner_mobilePhone() {
		return backoffice_newAppTab_Owner_mobilePhone;
	}

	public void setBackoffice_newAppTab_Owner_mobilePhone(String backoffice_newAppTab_Owner_mobilePhone) {
		this.backoffice_newAppTab_Owner_mobilePhone = backoffice_newAppTab_Owner_mobilePhone;
	}

	public String getBackoffice_newAppTab_Owner_email() {
		return backoffice_newAppTab_Owner_email;
	}

	public void setBackoffice_newAppTab_Owner_email(String backoffice_newAppTab_Owner_email) {
		this.backoffice_newAppTab_Owner_email = backoffice_newAppTab_Owner_email;
	}

	public String getBackoffice_newAppTab_Owner_OfOwnership() {
		return backoffice_newAppTab_Owner_OfOwnership;
	}

	public void setBackoffice_newAppTab_Owner_OfOwnership(String backoffice_newAppTab_Owner_OfOwnership) {
		this.backoffice_newAppTab_Owner_OfOwnership = backoffice_newAppTab_Owner_OfOwnership;
	}

	public String getBackoffice_newAppTab_Owner_dob() {
		return backoffice_newAppTab_Owner_dob;
	}

	public void setBackoffice_newAppTab_Owner_dob(String backoffice_newAppTab_Owner_dob) {
		this.backoffice_newAppTab_Owner_dob = backoffice_newAppTab_Owner_dob;
	}

	public String getBackoffice_newAppTab_Owner_saveAndContinue() {
		return backoffice_newAppTab_Owner_saveAndContinue;
	}

	public void setBackoffice_newAppTab_Owner_saveAndContinue(String backoffice_newAppTab_Owner_saveAndContinue) {
		this.backoffice_newAppTab_Owner_saveAndContinue = backoffice_newAppTab_Owner_saveAndContinue;
	}
	public String getBackoffice_newAppTab_Owner_ssn() {
		return backoffice_newAppTab_Owner_ssn;
	}

	public void setBackoffice_newAppTab_Owner_ssn(String backoffice_newAppTab_Owner_ssn) {
		this.backoffice_newAppTab_Owner_ssn = backoffice_newAppTab_Owner_ssn;
	}

	public String getBackoffice_newAppTab_Finish_annualRevenue() {
		return backoffice_newAppTab_Finish_annualRevenue;
	}

	public void setBackoffice_newAppTab_Finish_annualRevenue(String backoffice_newAppTab_Finish_annualRevenue) {
		this.backoffice_newAppTab_Finish_annualRevenue = backoffice_newAppTab_Finish_annualRevenue;
	}

	public String getBackoffice_newAppTab_Finish_averageBankBalances() {
		return backoffice_newAppTab_Finish_averageBankBalances;
	}

	public void setBackoffice_newAppTab_Finish_averageBankBalances(String backoffice_newAppTab_Finish_averageBankBalances) {
		this.backoffice_newAppTab_Finish_averageBankBalances = backoffice_newAppTab_Finish_averageBankBalances;
	}

	public String getBackoffice_newAppTab_Finish_checkBox() {
		return backoffice_newAppTab_Finish_checkBox;
	}

	public void setBackoffice_newAppTab_Finish_checkBox(String backoffice_newAppTab_Finish_checkBox) {
		this.backoffice_newAppTab_Finish_checkBox = backoffice_newAppTab_Finish_checkBox;
	}

	public String getBackoffice_newAppTab_Finish_submit() {
		return backoffice_newAppTab_Finish_submit;
	}

	public void setBackoffice_newAppTab_Finish_submit(String backoffice_newAppTab_Finish_submit) {
		this.backoffice_newAppTab_Finish_submit = backoffice_newAppTab_Finish_submit;
	}

	public String getBackoffice_newAppTab_Basic_howMuchDoYouNeed() {
		return backoffice_newAppTab_Basic_howMuchDoYouNeed;
	}

	public void setBackoffice_newAppTab_Basic_howMuchDoYouNeed(String backoffice_newAppTab_Basic_howMuchDoYouNeed) {
		this.backoffice_newAppTab_Basic_howMuchDoYouNeed = backoffice_newAppTab_Basic_howMuchDoYouNeed;
	}

 
    public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_selectDay() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_selectDay;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_selectDay(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_selectDay) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_selectDay = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_selectDay;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber;
	}

    public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email;
	}

    public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership;
	}

	
    public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN;
	}

 
    public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address;
	}

	public String getBackoffice_homepage_actions_editButton_edit_Ownerdetails() {
		return backoffice_homepage_actions_editButton_edit_Ownerdetails;
	}

	public void setBackoffice_homepage_actions_editButton_edit_Ownerdetails(
			String backoffice_homepage_actions_editButton_edit_Ownerdetails) {
		this.backoffice_homepage_actions_editButton_edit_Ownerdetails = backoffice_homepage_actions_editButton_edit_Ownerdetails;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner(
			String backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner = backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_ownerTab() {
		return backoffice_homepage_actions_endiButton_edit_ownerTab;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_ownerTab(
			String backoffice_homepage_actions_endiButton_edit_ownerTab) {
		this.backoffice_homepage_actions_endiButton_edit_ownerTab = backoffice_homepage_actions_endiButton_edit_ownerTab;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_businessTab_SICCode() {
		return backoffice_homepage_actions_endiButton_edit_businessTab_SICCode;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_businessTab_SICCode(
			String backoffice_homepage_actions_endiButton_edit_businessTab_SICCode) {
		this.backoffice_homepage_actions_endiButton_edit_businessTab_SICCode = backoffice_homepage_actions_endiButton_edit_businessTab_SICCode;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId() {
		return backoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId(
			String backoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId) {
		this.backoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId = backoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName() {
		return backoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName(
			String backoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName) {
		this.backoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName = backoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName;
	}

	public String getBackoffice_homepage_actions_endiButton_edit_businessTab() {
		return backoffice_homepage_actions_endiButton_edit_businessTab;
	}

	public void setBackoffice_homepage_actions_endiButton_edit_businessTab(
			String backoffice_homepage_actions_endiButton_edit_businessTab) {
		this.backoffice_homepage_actions_endiButton_edit_businessTab = backoffice_homepage_actions_endiButton_edit_businessTab;
	}

	public String getBackoffice_homepage_actions_editButton_edit_saveChanges() {
		return backoffice_homepage_actions_editButton_edit_saveChanges;
	}

	public void setBackoffice_homepage_actions_editButton_edit_saveChanges(
			String backoffice_homepage_actions_editButton_edit_saveChanges) {
		this.backoffice_homepage_actions_editButton_edit_saveChanges = backoffice_homepage_actions_editButton_edit_saveChanges;
	}

	public String getBackoffice_homepage_actions_editButton_edit_contactPhone() {
		return backoffice_homepage_actions_editButton_edit_contactPhone;
	}

	public void setBackoffice_homepage_actions_editButton_edit_contactPhone(
			String backoffice_homepage_actions_editButton_edit_contactPhone) {
		this.backoffice_homepage_actions_editButton_edit_contactPhone = backoffice_homepage_actions_editButton_edit_contactPhone;
	}

	public String getBackoffice_homepage_actions_editButton_edit_contactName() {
		return backoffice_homepage_actions_editButton_edit_contactName;
	}

	public void setBackoffice_homepage_actions_editButton_edit_contactName(
			String backoffice_homepage_actions_editButton_edit_contactName) {
		this.backoffice_homepage_actions_editButton_edit_contactName = backoffice_homepage_actions_editButton_edit_contactName;
	}

	public String getBackoffice_homepage_actions_editButton_edit() {
		return backoffice_homepage_actions_editButton_edit;
	}

	public void setBackoffice_homepage_actions_editButton_edit(String backoffice_homepage_actions_editButton_edit) {
		this.backoffice_homepage_actions_editButton_edit = backoffice_homepage_actions_editButton_edit;
	}

	public String getBackoffice_homepage_actions_editButton() {
		return backoffice_homepage_actions_editButton;
	}

	public void setBackoffice_homepage_actions_editButton(String backoffice_homepage_actions_editButton) {
		this.backoffice_homepage_actions_editButton = backoffice_homepage_actions_editButton;
	}
    
    public String getBackoffice_homepage_actions_businessCreditReportCallRecord() {
    return backoffice_homepage_actions_businessCreditReportCallRecord;
    }

    public void setBackoffice_homepage_actions_businessCreditReportCallRecord(
    String backoffice_homepage_actions_businessCreditReportCallRecord) {
    this.backoffice_homepage_actions_businessCreditReportCallRecord = backoffice_homepage_actions_businessCreditReportCallRecord;
    }

    public String getBackoffice_homepage_actions_businessCreditReportCallExperian() {
    return backoffice_homepage_actions_businessCreditReportCallExperian;
    }

    public void setBackoffice_homepage_actions_businessCreditReportCallExperian(
    String backoffice_homepage_actions_businessCreditReportCallExperian) {
    this.backoffice_homepage_actions_businessCreditReportCallExperian = backoffice_homepage_actions_businessCreditReportCallExperian;
    }
    

    public String getBackoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score() {
    return backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score;
    }

    public void setBackoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score(
    String backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score) {
    this.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score = backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score;
    }

    public String getBackoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate() {
    return backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate;
    }

    public void setBackoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate(
    String backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate) {
    this.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate = backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate;
    }

    public String getBackoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source() {
    return backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source;
    }

    public void setBackoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source(
    String backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source) {
    this.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source = backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source;
    }

    public String getBackoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes() {
    return backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes;
    }

    public void setBackoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes(
    String backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes) {
    this.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes = backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes;
    }

    public String getBackoffice_homepage_actions_businessCreditReportButton() {
    return backoffice_homepage_actions_businessCreditReportButton;
    }

    public void setBackoffice_homepage_actions_businessCreditReportButton(
    String backoffice_homepage_actions_businessCreditReportButton) {
    this.backoffice_homepage_actions_businessCreditReportButton = backoffice_homepage_actions_businessCreditReportButton;
    }

    public String getBackoffice_homepage_actions_businessCreditReport() {
    return backoffice_homepage_actions_businessCreditReport;
    }

    public void setBackoffice_homepage_actions_businessCreditReport(
    String backoffice_homepage_actions_businessCreditReport) {
    this.backoffice_homepage_actions_businessCreditReport = backoffice_homepage_actions_businessCreditReport;
    }


    public String getBackoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage() {
        return backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage;
    }

    public void setBackoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage(
            String backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage) {
        this.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage = backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage;
    }

    public String getBackoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage() {
        return backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage;
    }

    public void setBackoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage(
            String backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage) {
        this.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage = backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage;
    }

    public String getBackoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_firstName() {
        return backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_firstName;
    }

    public void setBackoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_firstName(
            String backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_firstName) {
        this.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_firstName = backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_firstName;
    }

    public String getBackoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport() {
        return backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport;
    }

    public void setBackoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport(
            String backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport) {
        this.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport = backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyCloseButton() {
        return backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyCloseButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyCloseButton(
            String backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyCloseButton) {
        this.backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyCloseButton = backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyCloseButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyButton() {
        return backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyButton(
            String backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyButton) {
        this.backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyButton = backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyButton;
    }

    public String getBackoffice_homepage_actions_uploadDocsCloseButton() {
        return backoffice_homepage_actions_uploadDocsCloseButton;
    }

    public void setBackoffice_homepage_actions_uploadDocsCloseButton(String backoffice_homepage_actions_uploadDocsCloseButton) {
        this.backoffice_homepage_actions_uploadDocsCloseButton = backoffice_homepage_actions_uploadDocsCloseButton;

    }
    public String getBackoffice_appDetailspage_verificationdashboard_addressVerification_verifyDetails() {
        return backoffice_appDetailspage_verificationdashboard_addressVerification_verifyDetails;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_addressVerification_verifyDetails(
            String backoffice_appDetailspage_verificationdashboard_addressVerification_verifyDetails) {
        this.backoffice_appDetailspage_verificationdashboard_addressVerification_verifyDetails = backoffice_appDetailspage_verificationdashboard_addressVerification_verifyDetails;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_addressVerification_verifypopupclose() {
        return backoffice_appDetailspage_verificationdashboard_addressVerification_verifypopupclose;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_addressVerification_verifypopupclose(
            String backoffice_appDetailspage_verificationdashboard_addressVerification_verifypopupclose) {
        this.backoffice_appDetailspage_verificationdashboard_addressVerification_verifypopupclose = backoffice_appDetailspage_verificationdashboard_addressVerification_verifypopupclose;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_addressVerification_verifystatus() {
        return backoffice_appDetailspage_verificationdashboard_addressVerification_verifystatus;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_addressVerification_verifystatus(
            String backoffice_appDetailspage_verificationdashboard_addressVerification_verifystatus) {
        this.backoffice_appDetailspage_verificationdashboard_addressVerification_verifystatus = backoffice_appDetailspage_verificationdashboard_addressVerification_verifystatus;
    }


    public String getBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifyclosebutton() {
        return backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifyclosebutton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifyclosebutton(
            String backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifyclosebutton) {
        this.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifyclosebutton = backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifyclosebutton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifybutton() {
        return backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifybutton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifybutton(
            String backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifybutton) {
        this.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifybutton = backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifybutton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox() {
        return backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox(
            String backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox) {
        this.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox = backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationdetails() {
        return backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationdetails;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationdetails(
            String backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationdetails) {
        this.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationdetails = backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationdetails;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifystatus() {
        return backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifystatus;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifystatus(
            String backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifystatus) {
        this.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifystatus = backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifystatus;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationPopUpClose() {
        return backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationPopUpClose;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationPopUpClose(
            String backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationPopUpClose) {
        this.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationPopUpClose = backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationPopUpClose;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationStatus() {
        return backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationStatus;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationStatus(
            String backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationStatus) {
        this.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationStatus = backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationStatus;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerifyButton() {
        return backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerifyButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerifyButton(
            String backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerifyButton) {
        this.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerifyButton = backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerifyButton;
    }



    public String getBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_CheckBox() {
        return backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_CheckBox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_CheckBox(
            String backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_CheckBox) {
        this.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_CheckBox = backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_CheckBox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification() {
        return backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification(
            String backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification) {
        this.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification = backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification;
    }

    public String getBackoffice_appDetailspage_verificationDashboard() {
        return backoffice_appDetailspage_verificationDashboard;
    }

    public void setBackoffice_appDetailspage_verificationDashboard(String backoffice_appDetailspage_verificationDashboard) {
        this.backoffice_appDetailspage_verificationDashboard = backoffice_appDetailspage_verificationDashboard;
    }



    public String getBackoffice_appDetailspage_businessinfo_leadSource() {
        return backoffice_appDetailspage_businessinfo_leadSource;
    }

    public void setBackoffice_appDetailspage_businessinfo_leadSource(String backoffice_appDetailspage_businessinfo_leadSource) {
        this.backoffice_appDetailspage_businessinfo_leadSource = backoffice_appDetailspage_businessinfo_leadSource;
    }

    public String getBackoffice_appDetailspage_cashflowTab_setAsCashflowAccountLink() {
        return backoffice_appDetailspage_cashflowTab_setAsCashflowAccountLink;
    }

    public void setBackoffice_appDetailspage_cashflowTab_setAsCashflowAccountLink(String backoffice_appDetailspage_cashflowTab_setAsCashflowAccountLink) {
        this.backoffice_appDetailspage_cashflowTab_setAsCashflowAccountLink = backoffice_appDetailspage_cashflowTab_setAsCashflowAccountLink;
    }

    @Value(value="${backoffice.appDetailspage.verificationdashboard.signedDocverification.signedAgreementLabel}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_signedAgreementLabel;

    public String getBackoffice_appDetailspage_verificationdashboard_signedDocverification_signedAgreementLabel() {
        return backoffice_appDetailspage_verificationdashboard_signedDocverification_signedAgreementLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_signedDocverification_signedAgreementLabel(String backoffice_appDetailspage_verificationdashboard_signedDocverification_signedAgreementLabel) {
        this.backoffice_appDetailspage_verificationdashboard_signedDocverification_signedAgreementLabel = backoffice_appDetailspage_verificationdashboard_signedDocverification_signedAgreementLabel;
    }

    public String getBackoffice_searchpage_productIdTextBox() {
        return backoffice_searchpage_productIdTextBox;
    }

    public void setBackoffice_searchpage_productIdTextBox(String backoffice_searchpage_productIdTextBox) {
        this.backoffice_searchpage_productIdTextBox = backoffice_searchpage_productIdTextBox;
    }

    public String getBackoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount() {
        return backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount;
    }

    public void setBackoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount(String backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount) {
        this.backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount = backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount;
    }

    public String getBackoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount() {
        return backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount;
    }

    public void setBackoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount(String backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount) {
        this.backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount = backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount;
    }

    public String getBackoffice_appDetailspage_activityLog_bankLinkedUpdate() {
        return backoffice_appDetailspage_activityLog_bankLinkedUpdate;
    }

    public void setBackoffice_appDetailspage_activityLog_bankLinkedUpdate(String backoffice_appDetailspage_activityLog_bankLinkedUpdate) {
        this.backoffice_appDetailspage_activityLog_bankLinkedUpdate = backoffice_appDetailspage_activityLog_bankLinkedUpdate;
    }

    public String getBackoffice_homepage_actions_uploadBankStatementButton() {
        return backoffice_homepage_actions_uploadBankStatementButton;
    }

    public void setBackoffice_homepage_actions_uploadBankStatementButton(String backoffice_homepage_actions_uploadBankStatementButton) {
        this.backoffice_homepage_actions_uploadBankStatementButton = backoffice_homepage_actions_uploadBankStatementButton;
    }

    public String getBackoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton() {
        return backoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton;
    }

    public void setBackoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton(String backoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton) {
        this.backoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton = backoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton;
    }

    public String getBackoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton() {
        return backoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton;
    }

    public void setBackoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton(String backoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton) {
        this.backoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton = backoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton;
    }

    public String getBackoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox() {
        return backoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox;
    }

    public void setBackoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox(String backoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox) {
        this.backoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox = backoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox;
    }

    public String getBackoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox() {
        return backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox;
    }

    public void setBackoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox(String backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox) {
        this.backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox = backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox;
    }

    public String getBackoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox() {
        return backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox;
    }

    public void setBackoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox(String backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox) {
        this.backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox = backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifiedLabel() {
        return backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifiedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifiedLabel(String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifiedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifiedLabel = backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifiedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_notVerifiedLabel() {
        return backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_notVerifiedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_notVerifiedLabel(String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_notVerifiedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_notVerifiedLabel = backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_notVerifiedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_passCheckBox() {
        return backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_passCheckBox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_passCheckBox(String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_passCheckBox) {
        this.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_passCheckBox = backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_passCheckBox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifyButton() {
        return backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifyButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifyButton(String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifyButton) {
        this.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifyButton = backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifyButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_closeButton() {
        return backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_closeButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_closeButton(String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_closeButton) {
        this.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_closeButton = backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_closeButton;
    }

    public String getBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab_modelTab() {
        return backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab_modelTab;
    }

    public void setBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab_modelTab(String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab_modelTab) {
        this.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab_modelTab = backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab_modelTab;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton() {
        return backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton(String backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton) {
        this.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton = backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton;
    }

    public String getBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab() {
        return backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab;
    }

    public void setBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab(String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab) {
        this.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab = backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab;
    }

    public String getBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab_pScoreLabel() {
        return backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab_pScoreLabel;
    }

    public void setBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab_pScoreLabel(String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab_pScoreLabel) {
        this.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab_pScoreLabel = backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab_pScoreLabel;
    }

    public String getBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab() {
        return backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab;
    }

    public void setBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab(String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab) {
        this.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab = backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab;
    }

    public String getBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab() {
        return backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab;
    }

    public void setBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab(String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab) {
        this.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab = backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel() {
        return backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel(String backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel = backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab() {
        return backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab(String backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab) {
        this.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab = backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel() {
        return backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel(String backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel) {
        this.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel = backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel;
    }

    public String getBackoffice_appDetailspage_scoringTab() {
        return backoffice_appDetailspage_scoringTab;
    }

    public void setBackoffice_appDetailspage_scoringTab(String backoffice_appDetailspage_scoringTab) {
        this.backoffice_appDetailspage_scoringTab = backoffice_appDetailspage_scoringTab;
    }

    public String getBackoffice_appDetailspage_scoringTab_ruleExecutionTab() {
        return backoffice_appDetailspage_scoringTab_ruleExecutionTab;
    }

    public void setBackoffice_appDetailspage_scoringTab_ruleExecutionTab(String backoffice_appDetailspage_scoringTab_ruleExecutionTab) {
        this.backoffice_appDetailspage_scoringTab_ruleExecutionTab = backoffice_appDetailspage_scoringTab_ruleExecutionTab;
    }

    public String getBackoffice_appDetailspage_activityLog_declineUpdate() {
        return backoffice_appDetailspage_activityLog_declineUpdate;
    }

    public void setBackoffice_appDetailspage_activityLog_declineUpdate(String backoffice_appDetailspage_activityLog_declineUpdate) {
        this.backoffice_appDetailspage_activityLog_declineUpdate = backoffice_appDetailspage_activityLog_declineUpdate;
    }

    public String getBackoffice_appDetailspage_businessinfo_businessName() {
        return backoffice_appDetailspage_businessinfo_businessName;
    }

    public void setBackoffice_appDetailspage_businessinfo_businessName(String backoffice_appDetailspage_businessinfo_businessName) {
        this.backoffice_appDetailspage_businessinfo_businessName = backoffice_appDetailspage_businessinfo_businessName;
    }

    public String getBackoffice_appDetailspage_businessinfo_email() {
        return backoffice_appDetailspage_businessinfo_email;
    }

    public void setBackoffice_appDetailspage_businessinfo_email(String backoffice_appDetailspage_businessinfo_email) {
        this.backoffice_appDetailspage_businessinfo_email = backoffice_appDetailspage_businessinfo_email;
    }

    public String getBackoffice_appDetailspage_businessinfo_dba() {
        return backoffice_appDetailspage_businessinfo_dba;
    }

    public void setBackoffice_appDetailspage_businessinfo_dba(String backoffice_appDetailspage_businessinfo_dba) {
        this.backoffice_appDetailspage_businessinfo_dba = backoffice_appDetailspage_businessinfo_dba;
    }

    public String getBackoffice_appDetailspage_businessinfo_address() {
        return backoffice_appDetailspage_businessinfo_address;
    }

    public void setBackoffice_appDetailspage_businessinfo_address(String backoffice_appDetailspage_businessinfo_address) {
        this.backoffice_appDetailspage_businessinfo_address = backoffice_appDetailspage_businessinfo_address;
    }

    public String getBackoffice_appDetailspage_businessinfo_phone() {
        return backoffice_appDetailspage_businessinfo_phone;
    }

    public void setBackoffice_appDetailspage_businessinfo_phone(String backoffice_appDetailspage_businessinfo_phone) {
        this.backoffice_appDetailspage_businessinfo_phone = backoffice_appDetailspage_businessinfo_phone;
    }

    public String getBackoffice_appDetailspage_businessinfo_reqAmount() {
        return backoffice_appDetailspage_businessinfo_reqAmount;
    }

    public void setBackoffice_appDetailspage_businessinfo_reqAmount(String backoffice_appDetailspage_businessinfo_reqAmount) {
        this.backoffice_appDetailspage_businessinfo_reqAmount = backoffice_appDetailspage_businessinfo_reqAmount;
    }

    public String getBackoffice_appDetailspage_businessinfo_businessTaxId() {
        return backoffice_appDetailspage_businessinfo_businessTaxId;
    }

    public void setBackoffice_appDetailspage_businessinfo_businessTaxId(String backoffice_appDetailspage_businessinfo_businessTaxId) {
        this.backoffice_appDetailspage_businessinfo_businessTaxId = backoffice_appDetailspage_businessinfo_businessTaxId;
    }

    public String getBackoffice_appDetailspage_businessinfo_annualRevenue() {
        return backoffice_appDetailspage_businessinfo_annualRevenue;
    }

    public void setBackoffice_appDetailspage_businessinfo_annualRevenue(String backoffice_appDetailspage_businessinfo_annualRevenue) {
        this.backoffice_appDetailspage_businessinfo_annualRevenue = backoffice_appDetailspage_businessinfo_annualRevenue;
    }

    public String getBackoffice_appDetailspage_businessinfo_estDate() {
        return backoffice_appDetailspage_businessinfo_estDate;
    }

    public void setBackoffice_appDetailspage_businessinfo_estDate(String backoffice_appDetailspage_businessinfo_estDate) {
        this.backoffice_appDetailspage_businessinfo_estDate = backoffice_appDetailspage_businessinfo_estDate;
    }

    public String getBackoffice_appDetailspage_businessinfo_businessOwnership() {
        return backoffice_appDetailspage_businessinfo_businessOwnership;
    }

    public void setBackoffice_appDetailspage_businessinfo_businessOwnership(String backoffice_appDetailspage_businessinfo_businessOwnership) {
        this.backoffice_appDetailspage_businessinfo_businessOwnership = backoffice_appDetailspage_businessinfo_businessOwnership;
    }

    public String getBackoffice_appDetailspage_businessinfo_avgBankBalance() {
        return backoffice_appDetailspage_businessinfo_avgBankBalance;
    }

    public void setBackoffice_appDetailspage_businessinfo_avgBankBalance(String backoffice_appDetailspage_businessinfo_avgBankBalance) {
        this.backoffice_appDetailspage_businessinfo_avgBankBalance = backoffice_appDetailspage_businessinfo_avgBankBalance;
    }

    public String getBackoffice_appDetailspage_businessinfo_existingLoan() {
        return backoffice_appDetailspage_businessinfo_existingLoan;
    }

    public void setBackoffice_appDetailspage_businessinfo_existingLoan(String backoffice_appDetailspage_businessinfo_existingLoan) {
        this.backoffice_appDetailspage_businessinfo_existingLoan = backoffice_appDetailspage_businessinfo_existingLoan;
    }

    public String getBackoffice_appDetailspage_businessinfo_industry() {
        return backoffice_appDetailspage_businessinfo_industry;
    }

    public void setBackoffice_appDetailspage_businessinfo_industry(String backoffice_appDetailspage_businessinfo_industry) {
        this.backoffice_appDetailspage_businessinfo_industry = backoffice_appDetailspage_businessinfo_industry;
    }

    public String getBackoffice_appDetailspage_businessinfo_purposeOfFunds() {
        return backoffice_appDetailspage_businessinfo_purposeOfFunds;
    }

    public void setBackoffice_appDetailspage_businessinfo_purposeOfFunds(String backoffice_appDetailspage_businessinfo_purposeOfFunds) {
        this.backoffice_appDetailspage_businessinfo_purposeOfFunds = backoffice_appDetailspage_businessinfo_purposeOfFunds;
    }

    public String getBackoffice_appDetailspage_businessinfo_entityType() {
        return backoffice_appDetailspage_businessinfo_entityType;
    }

    public void setBackoffice_appDetailspage_businessinfo_entityType(String backoffice_appDetailspage_businessinfo_entityType) {
        this.backoffice_appDetailspage_businessinfo_entityType = backoffice_appDetailspage_businessinfo_entityType;
    }

    public String getBackoffice_appDetailspage_businessinfo_alertsTab() {
        return backoffice_appDetailspage_businessinfo_alertsTab;
    }

    public void setBackoffice_appDetailspage_businessinfo_alertsTab(String backoffice_appDetailspage_businessinfo_alertsTab) {
        this.backoffice_appDetailspage_businessinfo_alertsTab = backoffice_appDetailspage_businessinfo_alertsTab;
    }

    public String getBackoffice_appDetailspage_businessinfo_alerts_alertName() {
        return backoffice_appDetailspage_businessinfo_alerts_alertName;
    }

    public void setBackoffice_appDetailspage_businessinfo_alerts_alertName(String backoffice_appDetailspage_businessinfo_alerts_alertName) {
        this.backoffice_appDetailspage_businessinfo_alerts_alertName = backoffice_appDetailspage_businessinfo_alerts_alertName;
    }

    public String getBackoffice_appDetailspage_businessinfo_alerts_alertDescription() {
        return backoffice_appDetailspage_businessinfo_alerts_alertDescription;
    }

    public void setBackoffice_appDetailspage_businessinfo_alerts_alertDescription(String backoffice_appDetailspage_businessinfo_alerts_alertDescription) {
        this.backoffice_appDetailspage_businessinfo_alerts_alertDescription = backoffice_appDetailspage_businessinfo_alerts_alertDescription;
    }

    public String getBackoffice_appDetailspage_businessinfo_alerts_alertStatus() {
        return backoffice_appDetailspage_businessinfo_alerts_alertStatus;
    }

    public void setBackoffice_appDetailspage_businessinfo_alerts_alertStatus(String backoffice_appDetailspage_businessinfo_alerts_alertStatus) {
        this.backoffice_appDetailspage_businessinfo_alerts_alertStatus = backoffice_appDetailspage_businessinfo_alerts_alertStatus;
    }

    public String getBackoffice_appDetailspage_businessinfo_alerts_alertTag() {
        return backoffice_appDetailspage_businessinfo_alerts_alertTag;
    }

    public void setBackoffice_appDetailspage_businessinfo_alerts_alertTag(String backoffice_appDetailspage_businessinfo_alerts_alertTag) {
        this.backoffice_appDetailspage_businessinfo_alerts_alertTag = backoffice_appDetailspage_businessinfo_alerts_alertTag;
    }

    public String getBackoffice_appDetailspage_businessinfo_alerts_dismissButton() {
        return backoffice_appDetailspage_businessinfo_alerts_dismissButton;
    }

    public void setBackoffice_appDetailspage_businessinfo_alerts_dismissButton(String backoffice_appDetailspage_businessinfo_alerts_dismissButton) {
        this.backoffice_appDetailspage_businessinfo_alerts_dismissButton = backoffice_appDetailspage_businessinfo_alerts_dismissButton;
    }

    public String getBackoffice_homepage_actions_editMenuButton() {
        return backoffice_homepage_actions_editMenuButton;
    }

    public void setBackoffice_homepage_actions_editMenuButton(String backoffice_homepage_actions_editMenuButton) {
        this.backoffice_homepage_actions_editMenuButton = backoffice_homepage_actions_editMenuButton;
    }

    public String getBackoffice_homepage_actions_addFundingRequestMenuButton() {
        return backoffice_homepage_actions_addFundingRequestMenuButton;
    }

    public void setBackoffice_homepage_actions_addFundingRequestMenuButton(String backoffice_homepage_actions_addFundingRequestMenuButton) {
        this.backoffice_homepage_actions_addFundingRequestMenuButton = backoffice_homepage_actions_addFundingRequestMenuButton;
    }

    public String getBackoffice_homepage_actions_addFundingRequestButton() {
        return backoffice_homepage_actions_addFundingRequestButton;
    }

    public void setBackoffice_homepage_actions_addFundingRequestButton(String backoffice_homepage_actions_addFundingRequestButton) {
        this.backoffice_homepage_actions_addFundingRequestButton = backoffice_homepage_actions_addFundingRequestButton;
    }

    public String getBackoffice_homepage_closeUploadDocsButton() {
        return backoffice_homepage_closeUploadDocsButton;
    }

    public void setBackoffice_homepage_closeUploadDocsButton(String backoffice_homepage_closeUploadDocsButton) {
        this.backoffice_homepage_closeUploadDocsButton = backoffice_homepage_closeUploadDocsButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_propertyverification_propertyVerificationLabel() {
        return backoffice_appDetailspage_verificationdashboard_propertyverification_propertyVerificationLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_propertyverification_propertyVerificationLabel(String backoffice_appDetailspage_verificationdashboard_propertyverification_propertyVerificationLabel) {
        this.backoffice_appDetailspage_verificationdashboard_propertyverification_propertyVerificationLabel = backoffice_appDetailspage_verificationdashboard_propertyverification_propertyVerificationLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_propertyverification_idScreenshot() {
        return backoffice_appDetailspage_verificationdashboard_propertyverification_idScreenshot;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_propertyverification_idScreenshot(String backoffice_appDetailspage_verificationdashboard_propertyverification_idScreenshot) {
        this.backoffice_appDetailspage_verificationdashboard_propertyverification_idScreenshot = backoffice_appDetailspage_verificationdashboard_propertyverification_idScreenshot;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_propertyverification_pgorrentCheckbox() {
        return backoffice_appDetailspage_verificationdashboard_propertyverification_pgorrentCheckbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_propertyverification_pgorrentCheckbox(String backoffice_appDetailspage_verificationdashboard_propertyverification_pgorrentCheckbox) {
        this.backoffice_appDetailspage_verificationdashboard_propertyverification_pgorrentCheckbox = backoffice_appDetailspage_verificationdashboard_propertyverification_pgorrentCheckbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_propertyverification_goodstandingCheckbox() {
        return backoffice_appDetailspage_verificationdashboard_propertyverification_goodstandingCheckbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_propertyverification_goodstandingCheckbox(String backoffice_appDetailspage_verificationdashboard_propertyverification_goodstandingCheckbox) {
        this.backoffice_appDetailspage_verificationdashboard_propertyverification_goodstandingCheckbox = backoffice_appDetailspage_verificationdashboard_propertyverification_goodstandingCheckbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_propertyverification_renewLeaseCheckbox() {
        return backoffice_appDetailspage_verificationdashboard_propertyverification_renewLeaseCheckbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_propertyverification_renewLeaseCheckbox(String backoffice_appDetailspage_verificationdashboard_propertyverification_renewLeaseCheckbox) {
        this.backoffice_appDetailspage_verificationdashboard_propertyverification_renewLeaseCheckbox = backoffice_appDetailspage_verificationdashboard_propertyverification_renewLeaseCheckbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_propertyverification_submitMortgageCheckbox() {
        return backoffice_appDetailspage_verificationdashboard_propertyverification_submitMortgageCheckbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_propertyverification_submitMortgageCheckbox(String backoffice_appDetailspage_verificationdashboard_propertyverification_submitMortgageCheckbox) {
        this.backoffice_appDetailspage_verificationdashboard_propertyverification_submitMortgageCheckbox = backoffice_appDetailspage_verificationdashboard_propertyverification_submitMortgageCheckbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_propertyverification_landlordPhoneTextbox() {
        return backoffice_appDetailspage_verificationdashboard_propertyverification_landlordPhoneTextbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_propertyverification_landlordPhoneTextbox(String backoffice_appDetailspage_verificationdashboard_propertyverification_landlordPhoneTextbox) {
        this.backoffice_appDetailspage_verificationdashboard_propertyverification_landlordPhoneTextbox = backoffice_appDetailspage_verificationdashboard_propertyverification_landlordPhoneTextbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_propertyverification_landlordNameTextbox() {
        return backoffice_appDetailspage_verificationdashboard_propertyverification_landlordNameTextbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_propertyverification_landlordNameTextbox(String backoffice_appDetailspage_verificationdashboard_propertyverification_landlordNameTextbox) {
        this.backoffice_appDetailspage_verificationdashboard_propertyverification_landlordNameTextbox = backoffice_appDetailspage_verificationdashboard_propertyverification_landlordNameTextbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_propertyverification_verifyButton() {
        return backoffice_appDetailspage_verificationdashboard_propertyverification_verifyButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_propertyverification_verifyButton(String backoffice_appDetailspage_verificationdashboard_propertyverification_verifyButton) {
        this.backoffice_appDetailspage_verificationdashboard_propertyverification_verifyButton = backoffice_appDetailspage_verificationdashboard_propertyverification_verifyButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_propertyverification_verifiedLabel() {
        return backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_propertyverification_verifiedLabel(String backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedLabel = backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_propertyverification_verifiedStatus() {
        return backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedStatus;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_propertyverification_verifiedStatus(String backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedStatus) {
        this.backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedStatus = backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedStatus;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_propertyverification_closeButton() {
        return backoffice_appDetailspage_verificationdashboard_propertyverification_closeButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_propertyverification_closeButton(String backoffice_appDetailspage_verificationdashboard_propertyverification_closeButton) {
        this.backoffice_appDetailspage_verificationdashboard_propertyverification_closeButton = backoffice_appDetailspage_verificationdashboard_propertyverification_closeButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_merchantverification_merchantVerificationLabel() {
        return backoffice_appDetailspage_verificationdashboard_merchantverification_merchantVerificationLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_merchantverification_merchantVerificationLabel(String backoffice_appDetailspage_verificationdashboard_merchantverification_merchantVerificationLabel) {
        this.backoffice_appDetailspage_verificationdashboard_merchantverification_merchantVerificationLabel = backoffice_appDetailspage_verificationdashboard_merchantverification_merchantVerificationLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_merchantverification_idScreenshot() {
        return backoffice_appDetailspage_verificationdashboard_merchantverification_idScreenshot;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_merchantverification_idScreenshot(String backoffice_appDetailspage_verificationdashboard_merchantverification_idScreenshot) {
        this.backoffice_appDetailspage_verificationdashboard_merchantverification_idScreenshot = backoffice_appDetailspage_verificationdashboard_merchantverification_idScreenshot;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_merchantverification_addressCheckbox() {
        return backoffice_appDetailspage_verificationdashboard_merchantverification_addressCheckbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_merchantverification_addressCheckbox(String backoffice_appDetailspage_verificationdashboard_merchantverification_addressCheckbox) {
        this.backoffice_appDetailspage_verificationdashboard_merchantverification_addressCheckbox = backoffice_appDetailspage_verificationdashboard_merchantverification_addressCheckbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_merchantverification_verifyButton() {
        return backoffice_appDetailspage_verificationdashboard_merchantverification_verifyButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_merchantverification_verifyButton(String backoffice_appDetailspage_verificationdashboard_merchantverification_verifyButton) {
        this.backoffice_appDetailspage_verificationdashboard_merchantverification_verifyButton = backoffice_appDetailspage_verificationdashboard_merchantverification_verifyButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_merchantverification_verifiedLabel() {
        return backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_merchantverification_verifiedLabel(String backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedLabel = backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_merchantverification_verifiedStatus() {
        return backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedStatus;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_merchantverification_verifiedStatus(String backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedStatus) {
        this.backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedStatus = backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedStatus;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_merchantverification_closeButton() {
        return backoffice_appDetailspage_verificationdashboard_merchantverification_closeButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_merchantverification_closeButton(String backoffice_appDetailspage_verificationdashboard_merchantverification_closeButton) {
        this.backoffice_appDetailspage_verificationdashboard_merchantverification_closeButton = backoffice_appDetailspage_verificationdashboard_merchantverification_closeButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_welcomeverification_welcomeVerificationLabel() {
        return backoffice_appDetailspage_verificationdashboard_welcomeverification_welcomeVerificationLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_welcomeverification_welcomeVerificationLabel(String backoffice_appDetailspage_verificationdashboard_welcomeverification_welcomeVerificationLabel) {
        this.backoffice_appDetailspage_verificationdashboard_welcomeverification_welcomeVerificationLabel = backoffice_appDetailspage_verificationdashboard_welcomeverification_welcomeVerificationLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_welcomeverification_acceptTermsCheckbox() {
        return backoffice_appDetailspage_verificationdashboard_welcomeverification_acceptTermsCheckbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_welcomeverification_acceptTermsCheckbox(String backoffice_appDetailspage_verificationdashboard_welcomeverification_acceptTermsCheckbox) {
        this.backoffice_appDetailspage_verificationdashboard_welcomeverification_acceptTermsCheckbox = backoffice_appDetailspage_verificationdashboard_welcomeverification_acceptTermsCheckbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_welcomeverification_verifyButton() {
        return backoffice_appDetailspage_verificationdashboard_welcomeverification_verifyButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_welcomeverification_verifyButton(String backoffice_appDetailspage_verificationdashboard_welcomeverification_verifyButton) {
        this.backoffice_appDetailspage_verificationdashboard_welcomeverification_verifyButton = backoffice_appDetailspage_verificationdashboard_welcomeverification_verifyButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedLabel() {
        return backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedLabel(String backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedLabel = backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedStatus() {
        return backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedStatus;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedStatus(String backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedStatus) {
        this.backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedStatus = backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedStatus;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_welcomeverification_closeButton() {
        return backoffice_appDetailspage_verificationdashboard_welcomeverification_closeButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_welcomeverification_closeButton(String backoffice_appDetailspage_verificationdashboard_welcomeverification_closeButton) {
        this.backoffice_appDetailspage_verificationdashboard_welcomeverification_closeButton = backoffice_appDetailspage_verificationdashboard_welcomeverification_closeButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_signedDocverification_idVerificationLabel() {
        return backoffice_appDetailspage_verificationdashboard_signedDocverification_idVerificationLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_signedDocverification_idVerificationLabel(String backoffice_appDetailspage_verificationdashboard_signedDocverification_idVerificationLabel) {
        this.backoffice_appDetailspage_verificationdashboard_signedDocverification_idVerificationLabel = backoffice_appDetailspage_verificationdashboard_signedDocverification_idVerificationLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_signedDocverification_idScreenshot() {
        return backoffice_appDetailspage_verificationdashboard_signedDocverification_idScreenshot;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_signedDocverification_idScreenshot(String backoffice_appDetailspage_verificationdashboard_signedDocverification_idScreenshot) {
        this.backoffice_appDetailspage_verificationdashboard_signedDocverification_idScreenshot = backoffice_appDetailspage_verificationdashboard_signedDocverification_idScreenshot;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_signedDocverification_allSignatureCheckbox() {
        return backoffice_appDetailspage_verificationdashboard_signedDocverification_allSignatureCheckbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_signedDocverification_allSignatureCheckbox(String backoffice_appDetailspage_verificationdashboard_signedDocverification_allSignatureCheckbox) {
        this.backoffice_appDetailspage_verificationdashboard_signedDocverification_allSignatureCheckbox = backoffice_appDetailspage_verificationdashboard_signedDocverification_allSignatureCheckbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_signedDocverification_verifyButton() {
        return backoffice_appDetailspage_verificationdashboard_signedDocverification_verifyButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_signedDocverification_verifyButton(String backoffice_appDetailspage_verificationdashboard_signedDocverification_verifyButton) {
        this.backoffice_appDetailspage_verificationdashboard_signedDocverification_verifyButton = backoffice_appDetailspage_verificationdashboard_signedDocverification_verifyButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedLabel() {
        return backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedLabel(String backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedLabel = backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedStatus() {
        return backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedStatus;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedStatus(String backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedStatus) {
        this.backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedStatus = backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedStatus;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_signedDocverification_closeButton() {
        return backoffice_appDetailspage_verificationdashboard_signedDocverification_closeButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_signedDocverification_closeButton(String backoffice_appDetailspage_verificationdashboard_signedDocverification_closeButton) {
        this.backoffice_appDetailspage_verificationdashboard_signedDocverification_closeButton = backoffice_appDetailspage_verificationdashboard_signedDocverification_closeButton;
    }

    public String getDocusign_welcomeLabel() {
        return docusign_welcomeLabel;
    }

    public void setDocusign_welcomeLabel(String docusign_welcomeLabel) {
        this.docusign_welcomeLabel = docusign_welcomeLabel;
    }

    public String getDocusign_agreementCheckbox() {
        return docusign_agreementCheckbox;
    }

    public void setDocusign_agreementCheckbox(String docusign_agreementCheckbox) {
        this.docusign_agreementCheckbox = docusign_agreementCheckbox;
    }

    public String getDocusign_continueButton() {
        return docusign_continueButton;
    }

    public void setDocusign_continueButton(String docusign_continueButton) {
        this.docusign_continueButton = docusign_continueButton;
    }

    public String getDocusign_startNavigationButton() {
        return docusign_startNavigationButton;
    }

    public void setDocusign_startNavigationButton(String docusign_startNavigationButton) {
        this.docusign_startNavigationButton = docusign_startNavigationButton;
    }

    public String getDocusign_finishSignButton() {
        return docusign_finishSignButton;
    }

    public void setDocusign_finishSignButton(String docusign_finishSignButton) {
        this.docusign_finishSignButton = docusign_finishSignButton;
    }

    public String getDocusign_adopotSignatureLabel() {
        return docusign_adopotSignatureLabel;
    }

    public void setDocusign_adopotSignatureLabel(String docusign_adopotSignatureLabel) {
        this.docusign_adopotSignatureLabel = docusign_adopotSignatureLabel;
    }

    public String getDocusign_adoptAndSignButton() {
        return docusign_adoptAndSignButton;
    }

    public void setDocusign_adoptAndSignButton(String docusign_adoptAndSignButton) {
        this.docusign_adoptAndSignButton = docusign_adoptAndSignButton;
    }

    public String getDocusign_postSignThanksNoteLabel() {
        return docusign_postSignThanksNoteLabel;
    }

    public void setDocusign_postSignThanksNoteLabel(String docusign_postSignThanksNoteLabel) {
        this.docusign_postSignThanksNoteLabel = docusign_postSignThanksNoteLabel;
    }

    public String getBackoffice_homepage_actions_sendMCAAgreementButton() {
        return backoffice_homepage_actions_sendMCAAgreementButton;
    }

    public void setBackoffice_homepage_actions_sendMCAAgreementButton(String backoffice_homepage_actions_sendMCAAgreementButton) {
        this.backoffice_homepage_actions_sendMCAAgreementButton = backoffice_homepage_actions_sendMCAAgreementButton;
    }

    public String getBackoffice_homepage_actions_sendLoanAgreement() {
        return backoffice_homepage_actions_sendLoanAgreement;
    }

    public void setBackoffice_homepage_actions_sendLoanAgreement(String backoffice_homepage_actions_sendLoanAgreement) {
        this.backoffice_homepage_actions_sendLoanAgreement = backoffice_homepage_actions_sendLoanAgreement;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel() {
        return backoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel(String backoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel = backoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus() {
        return backoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus(String backoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus) {
        this.backoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus = backoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot() {
        return backoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot(String backoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot) {
        this.backoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot = backoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel() {
        return backoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel(String backoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel) {
        this.backoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel = backoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox() {
        return backoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox(String backoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox) {
        this.backoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox = backoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox() {
        return backoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox(String backoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox) {
        this.backoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox = backoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox() {
        return backoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox(String backoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox) {
        this.backoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox = backoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bankverification_verifyButton() {
        return backoffice_appDetailspage_verificationdashboard_bankverification_verifyButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bankverification_verifyButton(String backoffice_appDetailspage_verificationdashboard_bankverification_verifyButton) {
        this.backoffice_appDetailspage_verificationdashboard_bankverification_verifyButton = backoffice_appDetailspage_verificationdashboard_bankverification_verifyButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bankverification_closeButton() {
        return backoffice_appDetailspage_verificationdashboard_bankverification_closeButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bankverification_closeButton(String backoffice_appDetailspage_verificationdashboard_bankverification_closeButton) {
        this.backoffice_appDetailspage_verificationdashboard_bankverification_closeButton = backoffice_appDetailspage_verificationdashboard_bankverification_closeButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_idverification_closeButton() {
        return backoffice_appDetailspage_verificationdashboard_idverification_closeButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_idverification_closeButton(String backoffice_appDetailspage_verificationdashboard_idverification_closeButton) {
        this.backoffice_appDetailspage_verificationdashboard_idverification_closeButton = backoffice_appDetailspage_verificationdashboard_idverification_closeButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_idverification_verifiedLabel() {
        return backoffice_appDetailspage_verificationdashboard_idverification_verifiedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_idverification_verifiedLabel(String backoffice_appDetailspage_verificationdashboard_idverification_verifiedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_idverification_verifiedLabel = backoffice_appDetailspage_verificationdashboard_idverification_verifiedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_idverification_verifiedStatus() {
        return backoffice_appDetailspage_verificationdashboard_idverification_verifiedStatus;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_idverification_verifiedStatus(String backoffice_appDetailspage_verificationdashboard_idverification_verifiedStatus) {
        this.backoffice_appDetailspage_verificationdashboard_idverification_verifiedStatus = backoffice_appDetailspage_verificationdashboard_idverification_verifiedStatus;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_idverification_verifyButton() {
        return backoffice_appDetailspage_verificationdashboard_idverification_verifyButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_idverification_verifyButton(String backoffice_appDetailspage_verificationdashboard_idverification_verifyButton) {
        this.backoffice_appDetailspage_verificationdashboard_idverification_verifyButton = backoffice_appDetailspage_verificationdashboard_idverification_verifyButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_idverification_licenceCheckbox() {
        return backoffice_appDetailspage_verificationdashboard_idverification_licenceCheckbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_idverification_licenceCheckbox(String backoffice_appDetailspage_verificationdashboard_idverification_licenceCheckbox) {
        this.backoffice_appDetailspage_verificationdashboard_idverification_licenceCheckbox = backoffice_appDetailspage_verificationdashboard_idverification_licenceCheckbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_idverification_nameCheckbox() {
        return backoffice_appDetailspage_verificationdashboard_idverification_nameCheckbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_idverification_nameCheckbox(String backoffice_appDetailspage_verificationdashboard_idverification_nameCheckbox) {
        this.backoffice_appDetailspage_verificationdashboard_idverification_nameCheckbox = backoffice_appDetailspage_verificationdashboard_idverification_nameCheckbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_idverification_dobCheckbox() {
        return backoffice_appDetailspage_verificationdashboard_idverification_dobCheckbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_idverification_dobCheckbox(String backoffice_appDetailspage_verificationdashboard_idverification_dobCheckbox) {
        this.backoffice_appDetailspage_verificationdashboard_idverification_dobCheckbox = backoffice_appDetailspage_verificationdashboard_idverification_dobCheckbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_idverification_idScreenshot() {
        return backoffice_appDetailspage_verificationdashboard_idverification_idScreenshot;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_idverification_idScreenshot(String backoffice_appDetailspage_verificationdashboard_idverification_idScreenshot) {
        this.backoffice_appDetailspage_verificationdashboard_idverification_idScreenshot = backoffice_appDetailspage_verificationdashboard_idverification_idScreenshot;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_idverification_idVerificationLabel() {
        return backoffice_appDetailspage_verificationdashboard_idverification_idVerificationLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_idverification_idVerificationLabel(String backoffice_appDetailspage_verificationdashboard_idverification_idVerificationLabel) {
        this.backoffice_appDetailspage_verificationdashboard_idverification_idVerificationLabel = backoffice_appDetailspage_verificationdashboard_idverification_idVerificationLabel;
    }

    public String getBackoffice_homepage_actions_uploadDocsButton() {
        return backoffice_homepage_actions_uploadDocsButton;
    }

    public void setBackoffice_homepage_actions_uploadDocsButton(String backoffice_homepage_actions_uploadDocsButton) {
        this.backoffice_homepage_actions_uploadDocsButton = backoffice_homepage_actions_uploadDocsButton;
    }

    public String getBackoffice_searchpage_offerTab_typeOfPaymentDropdown() {
        return backoffice_searchpage_offerTab_typeOfPaymentDropdown;
    }

    public void setBackoffice_searchpage_offerTab_typeOfPaymentDropdown(String backoffice_searchpage_offerTab_typeOfPaymentDropdown) {
        this.backoffice_searchpage_offerTab_typeOfPaymentDropdown = backoffice_searchpage_offerTab_typeOfPaymentDropdown;
    }

    public String getBackoffice_searchpage_offerTab_generateDealButton() {
        return backoffice_searchpage_offerTab_generateDealButton;
    }

    public void setBackoffice_searchpage_offerTab_generateDealButton(String backoffice_searchpage_offerTab_generateDealButton) {
        this.backoffice_searchpage_offerTab_generateDealButton = backoffice_searchpage_offerTab_generateDealButton;
    }

    public String getBackoffice_searchpage_offerTab_dealSelectedLabel() {
        return backoffice_searchpage_offerTab_dealSelectedLabel;
    }

    public void setBackoffice_searchpage_offerTab_dealSelectedLabel(String backoffice_searchpage_offerTab_dealSelectedLabel) {
        this.backoffice_searchpage_offerTab_dealSelectedLabel = backoffice_searchpage_offerTab_dealSelectedLabel;
    }

    public String getBackoffice_searchpage_offerTab_dealGenerationLabel() {
        return backoffice_searchpage_offerTab_dealGenerationLabel;
    }

    public void setBackoffice_searchpage_offerTab_dealGenerationLabel(String backoffice_searchpage_offerTab_dealGenerationLabel) {
        this.backoffice_searchpage_offerTab_dealGenerationLabel = backoffice_searchpage_offerTab_dealGenerationLabel;
    }

    public String getBackoffice_searchpage_offerTab() {
        return backoffice_searchpage_offerTab;
    }

    public void setBackoffice_searchpage_offerTab(String backoffice_searchpage_offerTab) {
        this.backoffice_searchpage_offerTab = backoffice_searchpage_offerTab;
    }

    public String getBackoffice_homepage_actions_computerOfferButton() {
        return backoffice_homepage_actions_computerOfferButton;
    }

    public void setBackoffice_homepage_actions_computerOfferButton(String backoffice_homepage_actions_computerOfferButton) {
        this.backoffice_homepage_actions_computerOfferButton = backoffice_homepage_actions_computerOfferButton;
    }

    public String getBackoffice_homepage_actionsButton() {
        return backoffice_homepage_actionsButton;
    }

    public void setBackoffice_homepage_actionsButton(String backoffice_homepage_actionsButton) {
        this.backoffice_homepage_actionsButton = backoffice_homepage_actionsButton;
    }

    public String getBackoffice_homepage_actions_computeOffer() {
        return backoffice_homepage_actions_computeOffer;
    }

    public void setBackoffice_homepage_actions_computeOffer(String backoffice_homepage_actions_computeOffer) {
        this.backoffice_homepage_actions_computeOffer = backoffice_homepage_actions_computeOffer;
    }

    public String getBackoffice_appDetailspage_activityLog_refreshButton() {
        return backoffice_appDetailspage_activityLog_refreshButton;
    }

    public void setBackoffice_appDetailspage_activityLog_refreshButton(String backoffice_appDetailspage_activityLog_refreshButton) {
        this.backoffice_appDetailspage_activityLog_refreshButton = backoffice_appDetailspage_activityLog_refreshButton;
    }

    public String getBackoffice_appDetailspage_activityLog_cashFlowUpdate() {
        return backoffice_appDetailspage_activityLog_cashFlowUpdate;
    }

    public void setBackoffice_appDetailspage_activityLog_cashFlowUpdate(String backoffice_appDetailspage_activityLog_cashFlowUpdate) {
        this.backoffice_appDetailspage_activityLog_cashFlowUpdate = backoffice_appDetailspage_activityLog_cashFlowUpdate;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton() {
        return backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton(String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton) {
        this.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton = backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel() {
        return backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel(String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel = backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus() {
        return backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus(String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus) {
        this.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus = backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox() {
        return backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox(String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox) {
        this.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox = backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton() {
        return backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton(String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton) {
        this.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton = backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton() {
        return backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton(String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton) {
        this.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton = backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel() {
        return backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel(String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel = backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_searchVerification_closeButton() {
        return backoffice_appDetailspage_verificationdashboard_searchVerification_closeButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_searchVerification_closeButton(String backoffice_appDetailspage_verificationdashboard_searchVerification_closeButton) {
        this.backoffice_appDetailspage_verificationdashboard_searchVerification_closeButton = backoffice_appDetailspage_verificationdashboard_searchVerification_closeButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel() {
        return backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel(String backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel = backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus() {
        return backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus(String backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus) {
        this.backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus = backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus;
    }

    public String getBackoffice_appDetailspage_activityLog_email() {
        return backoffice_appDetailspage_activityLog_email;
    }

    public void setBackoffice_appDetailspage_activityLog_email(String backoffice_appDetailspage_activityLog_email) {
        this.backoffice_appDetailspage_activityLog_email = backoffice_appDetailspage_activityLog_email;
    }

    public String getBackoffice_appDetailspage_activityLog_factVerification() {
        return backoffice_appDetailspage_activityLog_factVerification;
    }

    public void setBackoffice_appDetailspage_activityLog_factVerification(String backoffice_appDetailspage_activityLog_factVerification) {
        this.backoffice_appDetailspage_activityLog_factVerification = backoffice_appDetailspage_activityLog_factVerification;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton() {
        return backoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton(String backoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton) {
        this.backoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton = backoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox() {
        return backoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox(String backoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox) {
        this.backoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox = backoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel() {
        return backoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel(String backoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel) {
        this.backoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel = backoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel;
    }

    public String getBackoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton() {
        return backoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton;
    }

    public void setBackoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton(String backoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton) {
        this.backoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton = backoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton;
    }

    public String getBackoffice_appDetailspage_cashflowTab_editCashflow_editCashflowSummaryTitle() {
        return backoffice_appDetailspage_cashflowTab_editCashflow_editCashflowSummaryTitle;
    }

    public void setBackoffice_appDetailspage_cashflowTab_editCashflow_editCashflowSummaryTitle(String backoffice_appDetailspage_cashflowTab_editCashflow_editCashflowSummaryTitle) {
        this.backoffice_appDetailspage_cashflowTab_editCashflow_editCashflowSummaryTitle = backoffice_appDetailspage_cashflowTab_editCashflow_editCashflowSummaryTitle;
    }

    public String getBackoffice_appDetailspage_cashflowTab_editCashflow_submitButton() {
        return backoffice_appDetailspage_cashflowTab_editCashflow_submitButton;
    }

    public void setBackoffice_appDetailspage_cashflowTab_editCashflow_submitButton(String backoffice_appDetailspage_cashflowTab_editCashflow_submitButton) {
        this.backoffice_appDetailspage_cashflowTab_editCashflow_submitButton = backoffice_appDetailspage_cashflowTab_editCashflow_submitButton;
    }

    public String getBackoffice_appDetailspage_cashflowTab_editCashflowDropdown() {
        return backoffice_appDetailspage_cashflowTab_editCashflowDropdown;
    }

    public void setBackoffice_appDetailspage_cashflowTab_editCashflowDropdown(String backoffice_appDetailspage_cashflowTab_editCashflowDropdown) {
        this.backoffice_appDetailspage_cashflowTab_editCashflowDropdown = backoffice_appDetailspage_cashflowTab_editCashflowDropdown;
    }

    public String getBackoffice_appDetailspage_cashflowTab_editCashflowLink() {
        return backoffice_appDetailspage_cashflowTab_editCashflowLink;
    }

    public void setBackoffice_appDetailspage_cashflowTab_editCashflowLink(String backoffice_appDetailspage_cashflowTab_editCashflowLink) {
        this.backoffice_appDetailspage_cashflowTab_editCashflowLink = backoffice_appDetailspage_cashflowTab_editCashflowLink;
    }

    public String getBorrower_loginpage_loginLabel() {
        return borrower_loginpage_loginLabel;
    }

    public void setBorrower_loginpage_loginLabel(String borrower_loginpage_loginLabel) {
        this.borrower_loginpage_loginLabel = borrower_loginpage_loginLabel;
    }

    public String getBorrower_loginpage_emailTextBox() {
        return borrower_loginpage_emailTextBox;
    }

    public void setBorrower_loginpage_emailTextBox(String borrower_loginpage_emailTextBox) {
        this.borrower_loginpage_emailTextBox = borrower_loginpage_emailTextBox;
    }

    public String getBorrower_loginpage_passwordTextBox() {
        return borrower_loginpage_passwordTextBox;
    }

    public void setBorrower_loginpage_passwordTextBox(String borrower_loginpage_passwordTextBox) {
        this.borrower_loginpage_passwordTextBox = borrower_loginpage_passwordTextBox;
    }

    public String getBorrower_loginpage_submitButton() {
        return borrower_loginpage_submitButton;
    }

    public void setBorrower_loginpage_submitButton(String borrower_loginpage_submitButton) {
        this.borrower_loginpage_submitButton = borrower_loginpage_submitButton;
    }

    public String getBackoffice_appDetailspage_verificationDashboardTab() {
        return backoffice_appDetailspage_verificationDashboardTab;
    }

    public void setBackoffice_appDetailspage_verificationDashboardTab(String backoffice_appDetailspage_verificationDashboardTab) {
        this.backoffice_appDetailspage_verificationDashboardTab = backoffice_appDetailspage_verificationDashboardTab;
    }

    public String getBackoffice_appDetailspage_cashflowTab() {
        return backoffice_appDetailspage_cashflowTab;
    }

    public void setBackoffice_appDetailspage_cashflowTab(String backoffice_appDetailspage_cashflowTab) {
        this.backoffice_appDetailspage_cashflowTab = backoffice_appDetailspage_cashflowTab;
    }

    public String getSelectBank_continueButton() {
        return selectBank_continueButton;
    }

    public void setSelectBank_continueButton(String selectBank_continueButton) {
        this.selectBank_continueButton = selectBank_continueButton;
    }

    public String getSelectBank_selectYourAccountLabel() {
        return selectBank_selectYourAccountLabel;
    }

    public void setSelectBank_selectYourAccountLabel(String selectBank_selectYourAccountLabel) {
        this.selectBank_selectYourAccountLabel = selectBank_selectYourAccountLabel;
    }

    public String getSelectBank_plaidSavingAccount() {
        return selectBank_plaidSavingAccount;
    }

    public void setSelectBank_plaidSavingAccount(String selectBank_plaidSavingAccount) {
        this.selectBank_plaidSavingAccount = selectBank_plaidSavingAccount;
    }

    public String getSelectBank_sunTrustResultantBank() {
        return selectBank_sunTrustResultantBank;
    }

    public void setSelectBank_sunTrustResultantBank(String selectBank_sunTrustResultantBank) {
        this.selectBank_sunTrustResultantBank = selectBank_sunTrustResultantBank;
    }

    public String getSelectBank_selectBankLabel() {
        return selectBank_selectBankLabel;
    }

    public void setSelectBank_selectBankLabel(String selectBank_selectBankLabel) {
        this.selectBank_selectBankLabel = selectBank_selectBankLabel;
    }

    public String getSelectBank_searchBankTextBox() {
        return selectBank_searchBankTextBox;
    }

    public void setSelectBank_searchBankTextBox(String selectBank_searchBankTextBox) {
        this.selectBank_searchBankTextBox = selectBank_searchBankTextBox;
    }

    public String getSelectBank_sunTrustBank() {
        return selectBank_sunTrustBank;
    }

    public void setSelectBank_sunTrustBank(String selectBank_sunTrustBank) {
        this.selectBank_sunTrustBank = selectBank_sunTrustBank;
    }

    public String getSelectBank_plaidUsernameTextBox() {
        return selectBank_plaidUsernameTextBox;
    }

    public void setSelectBank_plaidUsernameTextBox(String selectBank_plaidUsernameTextBox) {
        this.selectBank_plaidUsernameTextBox = selectBank_plaidUsernameTextBox;
    }

    public String getSelectBank_plaidPasswordTextBox() {
        return selectBank_plaidPasswordTextBox;
    }

    public void setSelectBank_plaidPasswordTextBox(String selectBank_plaidPasswordTextBox) {
        this.selectBank_plaidPasswordTextBox = selectBank_plaidPasswordTextBox;
    }

    public String getSelectBank_submitPlaidButton() {
        return selectBank_submitPlaidButton;
    }

    public void setSelectBank_submitPlaidButton(String selectBank_submitPlaidButton) {
        this.selectBank_submitPlaidButton = selectBank_submitPlaidButton;
    }

    public String getBackoffice_appDetailspage_headerStatus() {
        return backoffice_appDetailspage_headerStatus;
    }

    public void setBackoffice_appDetailspage_headerStatus(String backoffice_appDetailspage_headerStatus) {
        this.backoffice_appDetailspage_headerStatus = backoffice_appDetailspage_headerStatus;
    }

    public String getBackoffice_searchpage_foundAppIdLabel() {
        return backoffice_searchpage_foundAppIdLabel;
    }

    public void setBackoffice_searchpage_foundAppIdLabel(String backoffice_searchpage_foundAppIdLabel) {
        this.backoffice_searchpage_foundAppIdLabel = backoffice_searchpage_foundAppIdLabel;
    }

    public String getBackoffice_searchpage_foundAppDateLabl() {
        return backoffice_searchpage_foundAppDateLabl;
    }

    public void setBackoffice_searchpage_foundAppDateLabl(String backoffice_searchpage_foundAppDateLabl) {
        this.backoffice_searchpage_foundAppDateLabl = backoffice_searchpage_foundAppDateLabl;
    }

    public String getBackoffice_searchpage_foundBusinessNameLabel() {
        return backoffice_searchpage_foundBusinessNameLabel;
    }

    public void setBackoffice_searchpage_foundBusinessNameLabel(String backoffice_searchpage_foundBusinessNameLabel) {
        this.backoffice_searchpage_foundBusinessNameLabel = backoffice_searchpage_foundBusinessNameLabel;
    }

    public String getBackoffice_searchpage_foundBusinessPhoneLabel() {
        return backoffice_searchpage_foundBusinessPhoneLabel;
    }

    public void setBackoffice_searchpage_foundBusinessPhoneLabel(String backoffice_searchpage_foundBusinessPhoneLabel) {
        this.backoffice_searchpage_foundBusinessPhoneLabel = backoffice_searchpage_foundBusinessPhoneLabel;
    }

    public String getBackoffice_searchpage_foundBusinessLoanAmountLabel() {
        return backoffice_searchpage_foundBusinessLoanAmountLabel;
    }

    public void setBackoffice_searchpage_foundBusinessLoanAmountLabel(String backoffice_searchpage_foundBusinessLoanAmountLabel) {
        this.backoffice_searchpage_foundBusinessLoanAmountLabel = backoffice_searchpage_foundBusinessLoanAmountLabel;
    }

    public String getBackoffice_searchpage_foundBusinessExpiryDateLabel() {
        return backoffice_searchpage_foundBusinessExpiryDateLabel;
    }

    public void setBackoffice_searchpage_foundBusinessExpiryDateLabel(String backoffice_searchpage_foundBusinessExpiryDateLabel) {
        this.backoffice_searchpage_foundBusinessExpiryDateLabel = backoffice_searchpage_foundBusinessExpiryDateLabel;
    }

    public String getBackoffice_searchpage_foundBusinessStatusLabel() {
        return backoffice_searchpage_foundBusinessStatusLabel;
    }

    public void setBackoffice_searchpage_foundBusinessStatusLabel(String backoffice_searchpage_foundBusinessStatusLabel) {
        this.backoffice_searchpage_foundBusinessStatusLabel = backoffice_searchpage_foundBusinessStatusLabel;
    }

    public String getBackoffice_searchpage_viewDetailsButton() {
        return backoffice_searchpage_viewDetailsButton;
    }

    public void setBackoffice_searchpage_viewDetailsButton(String backoffice_searchpage_viewDetailsButton) {
        this.backoffice_searchpage_viewDetailsButton = backoffice_searchpage_viewDetailsButton;
    }

    public String getBackoffice_searchpage_applicationNumberTextBox() {
        return backoffice_searchpage_applicationNumberTextBox;
    }

    public void setBackoffice_searchpage_applicationNumberTextBox(String backoffice_searchpage_applicationNumberTextBox) {
        this.backoffice_searchpage_applicationNumberTextBox = backoffice_searchpage_applicationNumberTextBox;
    }

    public String getBackoffice_searchpage_businessPhoneTextBox() {
        return backoffice_searchpage_businessPhoneTextBox;
    }

    public void setBackoffice_searchpage_businessPhoneTextBox(String backoffice_searchpage_businessPhoneTextBox) {
        this.backoffice_searchpage_businessPhoneTextBox = backoffice_searchpage_businessPhoneTextBox;
    }

    public String getBackoffice_searchpage_firstNameTextBox() {
        return backoffice_searchpage_firstNameTextBox;
    }

    public void setBackoffice_searchpage_firstNameTextBox(String backoffice_searchpage_firstNameTextBox) {
        this.backoffice_searchpage_firstNameTextBox = backoffice_searchpage_firstNameTextBox;
    }

    public String getBackoffice_searchpage_lastNameTextBox() {
        return backoffice_searchpage_lastNameTextBox;
    }

    public void setBackoffice_searchpage_lastNameTextBox(String backoffice_searchpage_lastNameTextBox) {
        this.backoffice_searchpage_lastNameTextBox = backoffice_searchpage_lastNameTextBox;
    }

    public String getBackoffice_searchpage_emailTextBox() {
        return backoffice_searchpage_emailTextBox;
    }

    public void setBackoffice_searchpage_emailTextBox(String backoffice_searchpage_emailTextBox) {
        this.backoffice_searchpage_emailTextBox = backoffice_searchpage_emailTextBox;
    }

    public String getBackoffice_searchpage_businessNameTextBox() {
        return backoffice_searchpage_businessNameTextBox;
    }

    public void setBackoffice_searchpage_businessNameTextBox(String backoffice_searchpage_businessNameTextBox) {
        this.backoffice_searchpage_businessNameTextBox = backoffice_searchpage_businessNameTextBox;
    }

    public String getBackoffice_searchpage_statusDropdown() {
        return backoffice_searchpage_statusDropdown;
    }

    public void setBackoffice_searchpage_statusDropdown(String backoffice_searchpage_statusDropdown) {
        this.backoffice_searchpage_statusDropdown = backoffice_searchpage_statusDropdown;
    }

    public String getBackoffice_searchpage_appDateDropdown() {
        return backoffice_searchpage_appDateDropdown;
    }

    public void setBackoffice_searchpage_appDateDropdown(String backoffice_searchpage_appDateDropdown) {
        this.backoffice_searchpage_appDateDropdown = backoffice_searchpage_appDateDropdown;
    }

    public String getBackoffice_searchpage_expDateDropdown() {
        return backoffice_searchpage_expDateDropdown;
    }

    public void setBackoffice_searchpage_expDateDropdown(String backoffice_searchpage_expDateDropdown) {
        this.backoffice_searchpage_expDateDropdown = backoffice_searchpage_expDateDropdown;
    }

    public String getBackoffice_searchpage_searchButton() {
        return backoffice_searchpage_searchButton;
    }

    public void setBackoffice_searchpage_searchButton(String backoffice_searchpage_searchButton) {
        this.backoffice_searchpage_searchButton = backoffice_searchpage_searchButton;
    }

    public String getBackoffice_header_profileDropdown() {
        return backoffice_header_profileDropdown;
    }

    public void setBackoffice_header_profileDropdown(String backoffice_header_profileDropdown) {
        this.backoffice_header_profileDropdown = backoffice_header_profileDropdown;
    }

    public String getBackoffice_header_logoutLink() {
        return backoffice_header_logoutLink;
    }

    public void setBackoffice_header_logoutLink(String backoffice_header_logoutLink) {
        this.backoffice_header_logoutLink = backoffice_header_logoutLink;
    }

    public String getBackoffice_homepage_dashboardTab() {
        return backoffice_homepage_dashboardTab;
    }

    public void setBackoffice_homepage_dashboardTab(String backoffice_homepage_dashboardTab) {
        this.backoffice_homepage_dashboardTab = backoffice_homepage_dashboardTab;
    }

    public String getBackoffice_homepage_newAppTab() {
        return backoffice_homepage_newAppTab;
    }

    public void setBackoffice_homepage_newAppTab(String backoffice_homepage_newAppTab) {
        this.backoffice_homepage_newAppTab = backoffice_homepage_newAppTab;
    }

    public String getBackoffice_homepage_userManagementTab() {
        return backoffice_homepage_userManagementTab;
    }

    public void setBackoffice_homepage_userManagementTab(String backoffice_homepage_userManagementTab) {
        this.backoffice_homepage_userManagementTab = backoffice_homepage_userManagementTab;
    }

    public String getBackoffice_homepage_allUsersTab() {
        return backoffice_homepage_allUsersTab;
    }

    public void setBackoffice_homepage_allUsersTab(String backoffice_homepage_allUsersTab) {
        this.backoffice_homepage_allUsersTab = backoffice_homepage_allUsersTab;
    }

    public String getBackoffice_homepage_createNewUserTab() {
        return backoffice_homepage_createNewUserTab;
    }

    public void setBackoffice_homepage_createNewUserTab(String backoffice_homepage_createNewUserTab) {
        this.backoffice_homepage_createNewUserTab = backoffice_homepage_createNewUserTab;
    }

    public String getBackoffice_homepage_searchTab() {
        return backoffice_homepage_searchTab;
    }

    public void setBackoffice_homepage_searchTab(String backoffice_homepage_searchTab) {
        this.backoffice_homepage_searchTab = backoffice_homepage_searchTab;
    }

    public String getBackoffice_homepage_searchAppTab() {
        return backoffice_homepage_searchAppTab;
    }

    public void setBackoffice_homepage_searchAppTab(String backoffice_homepage_searchAppTab) {
        this.backoffice_homepage_searchAppTab = backoffice_homepage_searchAppTab;
    }

    public String getBackoffice_homepage_myAppTab() {
        return backoffice_homepage_myAppTab;
    }

    public void setBackoffice_homepage_myAppTab(String backoffice_homepage_myAppTab) {
        this.backoffice_homepage_myAppTab = backoffice_homepage_myAppTab;
    }

    public String getBackoffice_homepage_assignedAppsTab() {
        return backoffice_homepage_assignedAppsTab;
    }

    public void setBackoffice_homepage_assignedAppsTab(String backoffice_homepage_assignedAppsTab) {
        this.backoffice_homepage_assignedAppsTab = backoffice_homepage_assignedAppsTab;
    }

    public String getBackoffice_homepage_unassignedAppsTab() {
        return backoffice_homepage_unassignedAppsTab;
    }

    public void setBackoffice_homepage_unassignedAppsTab(String backoffice_homepage_unassignedAppsTab) {
        this.backoffice_homepage_unassignedAppsTab = backoffice_homepage_unassignedAppsTab;
    }

    public String getBackoffice_homepage_applicationsTab() {
        return backoffice_homepage_applicationsTab;
    }

    public void setBackoffice_homepage_applicationsTab(String backoffice_homepage_applicationsTab) {
        this.backoffice_homepage_applicationsTab = backoffice_homepage_applicationsTab;
    }

    public String getBackoffice_homepage_creditTab() {
        return backoffice_homepage_creditTab;
    }

    public void setBackoffice_homepage_creditTab(String backoffice_homepage_creditTab) {
        this.backoffice_homepage_creditTab = backoffice_homepage_creditTab;
    }

    public String getBackoffice_homepage_incompleteLeadsTab() {
        return backoffice_homepage_incompleteLeadsTab;
    }

    public void setBackoffice_homepage_incompleteLeadsTab(String backoffice_homepage_incompleteLeadsTab) {
        this.backoffice_homepage_incompleteLeadsTab = backoffice_homepage_incompleteLeadsTab;
    }

    public String getBackoffice_homepage_pendingBankLinkingTab() {
        return backoffice_homepage_pendingBankLinkingTab;
    }

    public void setBackoffice_homepage_pendingBankLinkingTab(String backoffice_homepage_pendingBankLinkingTab) {
        this.backoffice_homepage_pendingBankLinkingTab = backoffice_homepage_pendingBankLinkingTab;
    }

    public String getBackoffice_homepage_pendingVerificationTab() {
        return backoffice_homepage_pendingVerificationTab;
    }

    public void setBackoffice_homepage_pendingVerificationTab(String backoffice_homepage_pendingVerificationTab) {
        this.backoffice_homepage_pendingVerificationTab = backoffice_homepage_pendingVerificationTab;
    }

    public String getBackoffice_homepage_underwriterTab() {
        return backoffice_homepage_underwriterTab;
    }

    public void setBackoffice_homepage_underwriterTab(String backoffice_homepage_underwriterTab) {
        this.backoffice_homepage_underwriterTab = backoffice_homepage_underwriterTab;
    }

    public String getBackoffice_homepage_preApprovedTab() {
        return backoffice_homepage_preApprovedTab;
    }

    public void setBackoffice_homepage_preApprovedTab(String backoffice_homepage_preApprovedTab) {
        this.backoffice_homepage_preApprovedTab = backoffice_homepage_preApprovedTab;
    }

    public String getBackoffice_homepage_offerGenerationTab() {
        return backoffice_homepage_offerGenerationTab;
    }

    public void setBackoffice_homepage_offerGenerationTab(String backoffice_homepage_offerGenerationTab) {
        this.backoffice_homepage_offerGenerationTab = backoffice_homepage_offerGenerationTab;
    }

    public String getBackoffice_homepage_docsRequestedTab() {
        return backoffice_homepage_docsRequestedTab;
    }

    public void setBackoffice_homepage_docsRequestedTab(String backoffice_homepage_docsRequestedTab) {
        this.backoffice_homepage_docsRequestedTab = backoffice_homepage_docsRequestedTab;
    }

    public String getBackoffice_homepage_agreementDocsOutTab() {
        return backoffice_homepage_agreementDocsOutTab;
    }

    public void setBackoffice_homepage_agreementDocsOutTab(String backoffice_homepage_agreementDocsOutTab) {
        this.backoffice_homepage_agreementDocsOutTab = backoffice_homepage_agreementDocsOutTab;
    }

    public String getBackoffice_homepage_pendingSignTab() {
        return backoffice_homepage_pendingSignTab;
    }

    public void setBackoffice_homepage_pendingSignTab(String backoffice_homepage_pendingSignTab) {
        this.backoffice_homepage_pendingSignTab = backoffice_homepage_pendingSignTab;
    }

    public String getBackoffice_homepage_fundingReviewTab() {
        return backoffice_homepage_fundingReviewTab;
    }

    public void setBackoffice_homepage_fundingReviewTab(String backoffice_homepage_fundingReviewTab) {
        this.backoffice_homepage_fundingReviewTab = backoffice_homepage_fundingReviewTab;
    }

    public String getBackoffice_loginPage_usernameTextBox() {
        return backoffice_loginPage_usernameTextBox;
    }

    public void setBackoffice_loginPage_usernameTextBox(String backoffice_loginPage_usernameTextBox) {
        this.backoffice_loginPage_usernameTextBox = backoffice_loginPage_usernameTextBox;
    }

    public String getBackoffice_loginPage_passwordTextBox() {
        return backoffice_loginPage_passwordTextBox;
    }

    public void setBackoffice_loginPage_passwordTextBox(String backoffice_loginPage_passwordTextBox) {
        this.backoffice_loginPage_passwordTextBox = backoffice_loginPage_passwordTextBox;
    }

    public String getBackoffice_loginPage_agreementCheckbox() {
        return backoffice_loginPage_agreementCheckbox;
    }

    public void setBackoffice_loginPage_agreementCheckbox(String backoffice_loginPage_agreementCheckbox) {
        this.backoffice_loginPage_agreementCheckbox = backoffice_loginPage_agreementCheckbox;
    }

    public String getBackoffice_loginPage_forgetPasswordLink() {
        return backoffice_loginPage_forgetPasswordLink;
    }

    public void setBackoffice_loginPage_forgetPasswordLink(String backoffice_loginPage_forgetPasswordLink) {
        this.backoffice_loginPage_forgetPasswordLink = backoffice_loginPage_forgetPasswordLink;
    }

    public String getBackoffice_loginPage_loginButton() {
        return backoffice_loginPage_loginButton;
    }

    public void setBackoffice_loginPage_loginButton(String backoffice_loginPage_loginButton) {
        this.backoffice_loginPage_loginButton = backoffice_loginPage_loginButton;
    }

    public String getDashboard_profileDropdown() {
        return dashboard_profileDropdown;
    }

    public void setDashboard_profileDropdown(String dashboard_profileDropdown) {
        this.dashboard_profileDropdown = dashboard_profileDropdown;
    }

    public String getDashboard_logoutLink() {
        return dashboard_logoutLink;
    }

    public void setDashboard_logoutLink(String dashboard_logoutLink) {
        this.dashboard_logoutLink = dashboard_logoutLink;
    }

    public String getDashboard_overviewTab() {
        return dashboard_overviewTab;
    }

    public void setDashboard_overviewTab(String dashboard_overviewTab) {
        this.dashboard_overviewTab = dashboard_overviewTab;
    }

    public String getDashboard_todoTab() {
        return dashboard_todoTab;
    }

    public void setDashboard_todoTab(String dashboard_todoTab) {
        this.dashboard_todoTab = dashboard_todoTab;
    }

    public String getDashboard_profileTab() {
        return dashboard_profileTab;
    }

    public void setDashboard_profileTab(String dashboard_profileTab) {
        this.dashboard_profileTab = dashboard_profileTab;
    }

    public String getDashboard_changePasswordTab() {
        return dashboard_changePasswordTab;
    }

    public void setDashboard_changePasswordTab(String dashboard_changePasswordTab) {
        this.dashboard_changePasswordTab = dashboard_changePasswordTab;
    }

    public String getDashboard_borrowerNameLabel() {
        return dashboard_borrowerNameLabel;
    }

    public void setDashboard_borrowerNameLabel(String dashboard_borrowerNameLabel) {
        this.dashboard_borrowerNameLabel = dashboard_borrowerNameLabel;
    }

    public String getDashboard_appIdLabel() {
        return dashboard_appIdLabel;
    }

    public void setDashboard_appIdLabel(String dashboard_appIdLabel) {
        this.dashboard_appIdLabel = dashboard_appIdLabel;
    }

    public String getDashboard_phoneNumberLabel() {
        return dashboard_phoneNumberLabel;
    }

    public void setDashboard_phoneNumberLabel(String dashboard_phoneNumberLabel) {
        this.dashboard_phoneNumberLabel = dashboard_phoneNumberLabel;
    }

    public String getDashboard_emailIdLabel() {
        return dashboard_emailIdLabel;
    }

    public void setDashboard_emailIdLabel(String dashboard_emailIdLabel) {
        this.dashboard_emailIdLabel = dashboard_emailIdLabel;
    }

    public String getDashboard_loanAmountAppliedLabel() {
        return dashboard_loanAmountAppliedLabel;
    }

    public void setDashboard_loanAmountAppliedLabel(String dashboard_loanAmountAppliedLabel) {
        this.dashboard_loanAmountAppliedLabel = dashboard_loanAmountAppliedLabel;
    }

    public String getDashboard_purposeOfFundsLabel() {
        return dashboard_purposeOfFundsLabel;
    }

    public void setDashboard_purposeOfFundsLabel(String dashboard_purposeOfFundsLabel) {
        this.dashboard_purposeOfFundsLabel = dashboard_purposeOfFundsLabel;
    }

    public String getDashboard_bankVerificationRequestMsg() {
        return dashboard_bankVerificationRequestMsg;
    }

    public void setDashboard_bankVerificationRequestMsg(String dashboard_bankVerificationRequestMsg) {
        this.dashboard_bankVerificationRequestMsg = dashboard_bankVerificationRequestMsg;
    }

    public String getDashboard_linkBankAccountButton() {
        return dashboard_linkBankAccountButton;
    }

    public void setDashboard_linkBankAccountButton(String dashboard_linkBankAccountButton) {
        this.dashboard_linkBankAccountButton = dashboard_linkBankAccountButton;
    }

    public String getHomepage_finishTab_annualRevenueTextBox() {
        return homepage_finishTab_annualRevenueTextBox;
    }

    public void setHomepage_finishTab_annualRevenueTextBox(String homepage_finishTab_annualRevenueTextBox) {
        this.homepage_finishTab_annualRevenueTextBox = homepage_finishTab_annualRevenueTextBox;
    }

    public String getHomepage_finishTab_avgBankBalanceTextBox() {
        return homepage_finishTab_avgBankBalanceTextBox;
    }

    public void setHomepage_finishTab_avgBankBalanceTextBox(String homepage_finishTab_avgBankBalanceTextBox) {
        this.homepage_finishTab_avgBankBalanceTextBox = homepage_finishTab_avgBankBalanceTextBox;
    }

    public String getHomepage_finishTab_existingLoanStatusDropdown() {
        return homepage_finishTab_existingLoanStatusDropdown;
    }

    public void setHomepage_finishTab_existingLoanStatusDropdown(String homepage_finishTab_existingLoanStatusDropdown) {
        this.homepage_finishTab_existingLoanStatusDropdown = homepage_finishTab_existingLoanStatusDropdown;
    }

    public String getHomepage_finishTab_agreementCheckBox() {
        return homepage_finishTab_agreementCheckBox;
    }

    public void setHomepage_finishTab_agreementCheckBox(String homepage_finishTab_agreementCheckBox) {
        this.homepage_finishTab_agreementCheckBox = homepage_finishTab_agreementCheckBox;
    }

    public String getHomepage_finishTab_cancelButton() {
        return homepage_finishTab_cancelButton;
    }

    public void setHomepage_finishTab_cancelButton(String homepage_finishTab_cancelButton) {
        this.homepage_finishTab_cancelButton = homepage_finishTab_cancelButton;
    }

    public String getHomepage_finishTab_submitButton() {
        return homepage_finishTab_submitButton;
    }

    public void setHomepage_finishTab_submitButton(String homepage_finishTab_submitButton) {
        this.homepage_finishTab_submitButton = homepage_finishTab_submitButton;
    }

    public String getHomepage_ownerTab_cancelButton() {
        return homepage_ownerTab_cancelButton;
    }

    public void setHomepage_ownerTab_cancelButton(String homepage_ownerTab_cancelButton) {
        this.homepage_ownerTab_cancelButton = homepage_ownerTab_cancelButton;
    }

    public String getHomepage_ownerTab_nextButton() {
        return homepage_ownerTab_nextButton;
    }

    public void setHomepage_ownerTab_nextButton(String homepage_ownerTab_nextButton) {
        this.homepage_ownerTab_nextButton = homepage_ownerTab_nextButton;
    }

    public String getHomepage_ownerTab_firstNameTextBox() {
        return homepage_ownerTab_firstNameTextBox;
    }

    public void setHomepage_ownerTab_firstNameTextBox(String homepage_ownerTab_firstNameTextBox) {
        this.homepage_ownerTab_firstNameTextBox = homepage_ownerTab_firstNameTextBox;
    }

    public String getHomepage_ownerTab_lastNameTextBox() {
        return homepage_ownerTab_lastNameTextBox;
    }

    public void setHomepage_ownerTab_lastNameTextBox(String homepage_ownerTab_lastNameTextBox) {
        this.homepage_ownerTab_lastNameTextBox = homepage_ownerTab_lastNameTextBox;
    }

    public String getHomepage_ownerTab_homeAddressTextBox() {
        return homepage_ownerTab_homeAddressTextBox;
    }

    public void setHomepage_ownerTab_homeAddressTextBox(String homepage_ownerTab_homeAddressTextBox) {
        this.homepage_ownerTab_homeAddressTextBox = homepage_ownerTab_homeAddressTextBox;
    }

    public String getHomepage_ownerTab_homeCityTextBox() {
        return homepage_ownerTab_homeCityTextBox;
    }

    public void setHomepage_ownerTab_homeCityTextBox(String homepage_ownerTab_homeCityTextBox) {
        this.homepage_ownerTab_homeCityTextBox = homepage_ownerTab_homeCityTextBox;
    }

    public String getHomepage_ownerTab_homeStateDropdown() {
        return homepage_ownerTab_homeStateDropdown;
    }

    public void setHomepage_ownerTab_homeStateDropdown(String homepage_ownerTab_homeStateDropdown) {
        this.homepage_ownerTab_homeStateDropdown = homepage_ownerTab_homeStateDropdown;
    }

    public String getHomepage_ownerTab_postalCodeTextBox() {
        return homepage_ownerTab_postalCodeTextBox;
    }

    public void setHomepage_ownerTab_postalCodeTextBox(String homepage_ownerTab_postalCodeTextBox) {
        this.homepage_ownerTab_postalCodeTextBox = homepage_ownerTab_postalCodeTextBox;
    }

    public String getHomepage_ownerTab_mobilePhoneTextBox() {
        return homepage_ownerTab_mobilePhoneTextBox;
    }

    public void setHomepage_ownerTab_mobilePhoneTextBox(String homepage_ownerTab_mobilePhoneTextBox) {
        this.homepage_ownerTab_mobilePhoneTextBox = homepage_ownerTab_mobilePhoneTextBox;
    }

    public String getHomepage_ownerTab_ownershipTextBox() {
        return homepage_ownerTab_ownershipTextBox;
    }

    public void setHomepage_ownerTab_ownershipTextBox(String homepage_ownerTab_ownershipTextBox) {
        this.homepage_ownerTab_ownershipTextBox = homepage_ownerTab_ownershipTextBox;
    }

    public String getHomepage_ownerTab_ssnTextBox() {
        return homepage_ownerTab_ssnTextBox;
    }

    public void setHomepage_ownerTab_ssnTextBox(String homepage_ownerTab_ssnTextBox) {
        this.homepage_ownerTab_ssnTextBox = homepage_ownerTab_ssnTextBox;
    }

    public String getHomepage_ownerTab_mobDropdown() {
        return homepage_ownerTab_mobDropdown;
    }

    public void setHomepage_ownerTab_mobDropdown(String homepage_ownerTab_mobDropdown) {
        this.homepage_ownerTab_mobDropdown = homepage_ownerTab_mobDropdown;
    }

    public String getHomepage_ownerTab_dobDropdown() {
        return homepage_ownerTab_dobDropdown;
    }

    public void setHomepage_ownerTab_dobDropdown(String homepage_ownerTab_dobDropdown) {
        this.homepage_ownerTab_dobDropdown = homepage_ownerTab_dobDropdown;
    }

    public String getHomepage_ownerTab_yobDropdown() {
        return homepage_ownerTab_yobDropdown;
    }

    public void setHomepage_ownerTab_yobDropdown(String homepage_ownerTab_yobDropdown) {
        this.homepage_ownerTab_yobDropdown = homepage_ownerTab_yobDropdown;
    }

    public String getHomepage_ownerTab_emailAddressTextBox() {
        return homepage_ownerTab_emailAddressTextBox;
    }

    public void setHomepage_ownerTab_emailAddressTextBox(String homepage_ownerTab_emailAddressTextBox) {
        this.homepage_ownerTab_emailAddressTextBox = homepage_ownerTab_emailAddressTextBox;
    }

    public String getHomepage_businessTab_legalCompanyNameTextBox() {
        return homepage_businessTab_legalCompanyNameTextBox;
    }

    public void setHomepage_businessTab_legalCompanyNameTextBox(String homepage_businessTab_legalCompanyNameTextBox) {
        this.homepage_businessTab_legalCompanyNameTextBox = homepage_businessTab_legalCompanyNameTextBox;
    }

    public String getHomepage_businessTab_businessAddressTextBox() {
        return homepage_businessTab_businessAddressTextBox;
    }

    public void setHomepage_businessTab_businessAddressTextBox(String homepage_businessTab_businessAddressTextBox) {
        this.homepage_businessTab_businessAddressTextBox = homepage_businessTab_businessAddressTextBox;
    }

    public String getHomepage_businessTab_cityTextBox() {
        return homepage_businessTab_cityTextBox;
    }

    public void setHomepage_businessTab_cityTextBox(String homepage_businessTab_cityTextBox) {
        this.homepage_businessTab_cityTextBox = homepage_businessTab_cityTextBox;
    }

    public String getHomepage_businessTab_stateDropdown() {
        return homepage_businessTab_stateDropdown;
    }

    public void setHomepage_businessTab_stateDropdown(String homepage_businessTab_stateDropdown) {
        this.homepage_businessTab_stateDropdown = homepage_businessTab_stateDropdown;
    }

    public String getHomepage_businessTab_postalCodeTextBox() {
        return homepage_businessTab_postalCodeTextBox;
    }

    public void setHomepage_businessTab_postalCodeTextBox(String homepage_businessTab_postalCodeTextBox) {
        this.homepage_businessTab_postalCodeTextBox = homepage_businessTab_postalCodeTextBox;
    }

    public String getHomepage_businessTab_rentOrOwnDropdown() {
        return homepage_businessTab_rentOrOwnDropdown;
    }

    public void setHomepage_businessTab_rentOrOwnDropdown(String homepage_businessTab_rentOrOwnDropdown) {
        this.homepage_businessTab_rentOrOwnDropdown = homepage_businessTab_rentOrOwnDropdown;
    }

    public String getHomepage_businessTab_industryDropdown() {
        return homepage_businessTab_industryDropdown;
    }

    public void setHomepage_businessTab_industryDropdown(String homepage_businessTab_industryDropdown) {
        this.homepage_businessTab_industryDropdown = homepage_businessTab_industryDropdown;
    }

    public String getHomepage_businessTab_businessTaxIdTextBox() {
        return homepage_businessTab_businessTaxIdTextBox;
    }

    public void setHomepage_businessTab_businessTaxIdTextBox(String homepage_businessTab_businessTaxIdTextBox) {
        this.homepage_businessTab_businessTaxIdTextBox = homepage_businessTab_businessTaxIdTextBox;
    }

    public String getHomepage_businessTab_entityTypeDropdown() {
        return homepage_businessTab_entityTypeDropdown;
    }

    public void setHomepage_businessTab_entityTypeDropdown(String homepage_businessTab_entityTypeDropdown) {
        this.homepage_businessTab_entityTypeDropdown = homepage_businessTab_entityTypeDropdown;
    }

    public String getHomepage_businessTab_estMonthDropdown() {
        return homepage_businessTab_estMonthDropdown;
    }

    public void setHomepage_businessTab_estMonthDropdown(String homepage_businessTab_estMonthDropdown) {
        this.homepage_businessTab_estMonthDropdown = homepage_businessTab_estMonthDropdown;
    }

    public String getHomepage_businessTab_estDayDropdown() {
        return homepage_businessTab_estDayDropdown;
    }

    public void setHomepage_businessTab_estDayDropdown(String homepage_businessTab_estDayDropdown) {
        this.homepage_businessTab_estDayDropdown = homepage_businessTab_estDayDropdown;
    }

    public String getHomepage_businessTab_estYearDropdown() {
        return homepage_businessTab_estYearDropdown;
    }

    public void setHomepage_businessTab_estYearDropdown(String homepage_businessTab_estYearDropdown) {
        this.homepage_businessTab_estYearDropdown = homepage_businessTab_estYearDropdown;
    }

    public String getHomepage_businessTab_cancelButton() {
        return homepage_businessTab_cancelButton;
    }

    public void setHomepage_businessTab_cancelButton(String homepage_businessTab_cancelButton) {
        this.homepage_businessTab_cancelButton = homepage_businessTab_cancelButton;
    }

    public String getHomepage_businessTab_nextButton() {
        return homepage_businessTab_nextButton;
    }

    public void setHomepage_businessTab_nextButton(String homepage_businessTab_nextButton) {
        this.homepage_businessTab_nextButton = homepage_businessTab_nextButton;
    }

    public void setHomepage_newApplicationLabel(String homepage_newApplicationLabel) {
        this.homepage_newApplicationLabel = homepage_newApplicationLabel;
    }

    public void setHomepage_basicTab(String homepage_basicTab) {
        this.homepage_basicTab = homepage_basicTab;
    }

    public void setHomepage_businessTab(String homepage_businessTab) {
        this.homepage_businessTab = homepage_businessTab;
    }

    public void setHomepage_ownerTab(String homepage_ownerTab) {
        this.homepage_ownerTab = homepage_ownerTab;
    }

    public void setHomepage_finishTab(String homepage_finishTab) {
        this.homepage_finishTab = homepage_finishTab;
    }

    public void setHomepage_basicTab_howMuchDoYouNeedDropdown(String homepage_basicTab_howMuchDoYouNeedDropdown) {
        this.homepage_basicTab_howMuchDoYouNeedDropdown = homepage_basicTab_howMuchDoYouNeedDropdown;
    }

    public void setHomepage_basicTab_howSoonYouNeedDropdown(String homepage_basicTab_howSoonYouNeedDropdown) {
        this.homepage_basicTab_howSoonYouNeedDropdown = homepage_basicTab_howSoonYouNeedDropdown;
    }

    public void setHomepage_basicTab_firstNameTextBox(String homepage_basicTab_firstNameTextBox) {
        this.homepage_basicTab_firstNameTextBox = homepage_basicTab_firstNameTextBox;
    }

    public void setHomepage_basicTab_lastNameTextBox(String homepage_basicTab_lastNameTextBox) {
        this.homepage_basicTab_lastNameTextBox = homepage_basicTab_lastNameTextBox;
    }

    public void setHomepage_basicTab_emailAddressTextBox(String homepage_basicTab_emailAddressTextBox) {
        this.homepage_basicTab_emailAddressTextBox = homepage_basicTab_emailAddressTextBox;
    }

    public void setHomepage_basicTab_primaryPhoneTextBox(String homepage_basicTab_primaryPhoneTextBox) {
        this.homepage_basicTab_primaryPhoneTextBox = homepage_basicTab_primaryPhoneTextBox;
    }

    public void setHomepage_basicTab_passwordTextBox(String homepage_basicTab_passwordTextBox) {
        this.homepage_basicTab_passwordTextBox = homepage_basicTab_passwordTextBox;
    }

    public void setHomepage_basicTab_confirmPasswordTextBox(String homepage_basicTab_confirmPasswordTextBox) {
        this.homepage_basicTab_confirmPasswordTextBox = homepage_basicTab_confirmPasswordTextBox;
    }

    public void setHomepage_basicTab_cancelButton(String homepage_basicTab_cancelButton) {
        this.homepage_basicTab_cancelButton = homepage_basicTab_cancelButton;
    }

    public void setHomepage_basicTab_nextButton(String homepage_basicTab_nextButton) {
        this.homepage_basicTab_nextButton = homepage_basicTab_nextButton;
    }

    public String getHomepage_basicTab_purposeOfFundsDropdown() {
        return homepage_basicTab_purposeOfFundsDropdown;
    }

    public void setHomepage_basicTab_purposeOfFundsDropdown(String homepage_basicTab_purposeOfFundsDropdown) {
        this.homepage_basicTab_purposeOfFundsDropdown = homepage_basicTab_purposeOfFundsDropdown;
    }

    public String getHomepage_newApplicationLabel() {
        return homepage_newApplicationLabel;
    }

    public String getHomepage_basicTab() {
        return homepage_basicTab;
    }

    public String getHomepage_businessTab() {
        return homepage_businessTab;
    }

    public String getHomepage_ownerTab() {
        return homepage_ownerTab;
    }

    public String getHomepage_finishTab() {
        return homepage_finishTab;
    }

    public String getHomepage_basicTab_howMuchDoYouNeedDropdown() {
        return homepage_basicTab_howMuchDoYouNeedDropdown;
    }

    public String getHomepage_basicTab_howSoonYouNeedDropdown() {
        return homepage_basicTab_howSoonYouNeedDropdown;
    }

    public String getHomepage_basicTab_firstNameTextBox() {
        return homepage_basicTab_firstNameTextBox;
    }

    public String getHomepage_basicTab_lastNameTextBox() {
        return homepage_basicTab_lastNameTextBox;
    }

    public String getHomepage_basicTab_emailAddressTextBox() {
        return homepage_basicTab_emailAddressTextBox;
    }

    public String getHomepage_basicTab_primaryPhoneTextBox() {
        return homepage_basicTab_primaryPhoneTextBox;
    }

    public String getHomepage_basicTab_passwordTextBox() {
        return homepage_basicTab_passwordTextBox;
    }

    public String getHomepage_basicTab_confirmPasswordTextBox() {
        return homepage_basicTab_confirmPasswordTextBox;
    }

    public String getHomepage_basicTab_cancelButton() {
        return homepage_basicTab_cancelButton;
    }

    public String getHomepage_basicTab_nextButton() {
        return homepage_basicTab_nextButton;
    }

    public String getHomepage_businessTab_dbaTextBox() {
        return homepage_businessTab_dbaTextBox;
    }

    public void setHomepage_businessTab_dbaTextBox(String homepage_businessTab_dbaTextBox) {
        this.homepage_businessTab_dbaTextBox = homepage_businessTab_dbaTextBox;
    }
    public String getBackoffice_homepage_actions_BankruptcyDetailsButton() {
		return backoffice_homepage_actions_BankruptcyDetailsButton;
	}

	public void setBackoffice_homepage_actions_BankruptcyDetailsButton(
			String backoffice_homepage_actions_BankruptcyDetailsButton) {
		this.backoffice_homepage_actions_BankruptcyDetailsButton = backoffice_homepage_actions_BankruptcyDetailsButton;
	}
	public String getBackoffice_appDetailspage_activityLog_statusChanged() {
		return backoffice_appDetailspage_activityLog_statusChanged;
	}

	public void setBackoffice_appDetailspage_activityLog_statusChanged(
			String backoffice_appDetailspage_activityLog_statusChanged) {
		this.backoffice_appDetailspage_activityLog_statusChanged = backoffice_appDetailspage_activityLog_statusChanged;
	}
	public String getBackoffice_homepage_actionsButton_bankruptcyButton() {
		return backoffice_homepage_actionsButton_bankruptcyButton;
	}

	public void setBackoffice_homepage_actionsButton_bankruptcyButton(
			String backoffice_homepage_actionsButton_bankruptcyButton) {
		this.backoffice_homepage_actionsButton_bankruptcyButton = backoffice_homepage_actionsButton_bankruptcyButton;
	}

	public String getBackoffice_homepage_actionsButton_bankruptcySearchButton() {
		return backoffice_homepage_actionsButton_bankruptcySearchButton;
	}

	public void setBackoffice_homepage_actionsButton_bankruptcySearchButton(
			String backoffice_homepage_actionsButton_bankruptcySearchButton) {
		this.backoffice_homepage_actionsButton_bankruptcySearchButton = backoffice_homepage_actionsButton_bankruptcySearchButton;
	}
	

    public String getHomepage_businessTab_Country() {
		return homepage_businessTab_Country;
	}

	public void setHomepage_businessTab_Country(String homepage_businessTab_Country) {
		this.homepage_businessTab_Country = homepage_businessTab_Country;
	}
	
	public String getHomepage_ownerTab_Country() {
		return homepage_ownerTab_Country;
	}

	public void setHomepage_ownerTab_country(String homepage_ownerTab_country) {
		this.homepage_ownerTab_Country = homepage_ownerTab_Country;
	}
	
	 public String getBackoffice_newAppTab_Owner_country() {
		   return backoffice_newAppTab_Owner_country;
		}

	public void setBackoffice_newAppTab_Owner_country(String backoffice_newAppTab_Owner_country) {
			this.backoffice_newAppTab_Owner_country = backoffice_newAppTab_Owner_country;
		}
		
	 public String getBackoffice_newAppTab_Business_country() {
			return backoffice_newAppTab_Business_country;
		}

    public void setBackoffice_newAppTab_Business_country(String backoffice_newAppTab_Business_country) {
			this.backoffice_newAppTab_Business_country = backoffice_newAppTab_Business_country;
		}


	

}
