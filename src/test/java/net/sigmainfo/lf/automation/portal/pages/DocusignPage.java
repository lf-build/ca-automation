package net.sigmainfo.lf.automation.portal.pages;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 07-12-2017.
 */
public class DocusignPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(DocusignPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public DocusignPage(WebDriver driver) throws Exception {

        this.driver = driver;
        PortalFuncUtils.waitForPageToLoad(driver);
        logger.info("=========== DocusignPage is loaded============");
    }
    public DocusignPage(){}

    public void submitSignedDoc() {
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.docusign_welcomeLabel)));
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.docusign_agreementCheckbox)));
            wait.until(ExpectedConditions.elementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.docusign_agreementCheckbox)));
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.docusign_agreementCheckbox),"use electronic records and signatures");
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.docusign_continueButton)));
            wait.until(ExpectedConditions.elementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.docusign_continueButton)));
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.docusign_continueButton),"Continue");
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.docusign_startNavigationButton)));
            wait.until(ExpectedConditions.elementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.docusign_startNavigationButton)));
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.docusign_startNavigationButton),"Start navigation");
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.docusign_startNavigationButton)));
            wait.until(ExpectedConditions.elementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.docusign_startNavigationButton)));
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.docusign_startNavigationButton),"Start navigation");
            int signButtons=driver.findElements(By.xpath("//button[@class='tab-button' AND @type='button' AND starts-with(@data-qa,'signature-tab-required-')]")).size();
            PortalFuncUtils.waitUntilSignButtons(driver,signButtons);
            for(int i=1;i<=signButtons;i++)
            {

            }




            //wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
