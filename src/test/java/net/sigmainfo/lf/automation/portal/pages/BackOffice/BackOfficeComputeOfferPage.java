package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import com.google.common.base.Predicate;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerHomePage;
import net.sigmainfo.lf.automation.portal.pages.DocusignPage;
import org.apache.poi.poifs.eventfilesystem.POIFSReaderListener;
import org.openqa.selenium.*;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import javax.sound.sampled.Port;
import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import com.google.common.base.Function;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.*;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 30-11-2017.
 */
public class BackOfficeComputeOfferPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(BackOfficeComputeOfferPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public BackOfficeComputeOfferPage(WebDriver driver) throws Exception {

        this.driver = driver;
        //wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog)));
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 90);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for BackOfficeAppDetailsPage Load Request to complete.");
        }
        logger.info("=========== BackOfficeAppDetailsPage is loaded============");
    }
    public BackOfficeComputeOfferPage() throws Exception{}

    public void verifyOfferDetails(String appId,Boolean isManualUpload) throws Exception {
        PortalFuncUtils.waitForPageToLoad(driver);
       /* if (isManualUpload) {
            try {
                Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(360, SECONDS).pollingEvery(30, SECONDS).ignoring(NoSuchElementException.class);
                Boolean function = wait.until(new Function<WebDriver, Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        driver.navigate().refresh();
                        System.out.println("Verifying dimension");
                        System.out.println(driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'Fact BusinessTaxIdVerification verified for " + appId + " and result was Passed')]")).size());
                        return (driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'Fact BusinessTaxIdVerification verified for " + appId + " and result was Passed')]")).size() == 2);
                    }
                });
            }catch (Exception e)
            {
                assertTrue(driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'Fact BusinessTaxIdVerification verified for " + appId + " and result was Passed')]")).size() == 2,"Timed out waiting for BusinessTaxIdVerification.");
            }
        } else {
            try {
            Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(360, SECONDS).pollingEvery(30, SECONDS).ignoring(NoSuchElementException.class);
            Boolean function = wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                        driver.navigate().refresh();
                        System.out.println("Verifying dimension");
                        System.out.println(driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'Bank Linked')]")).size());
                        return (driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'Bank Linked')]")).size() > 0);
                }
            });
            }catch (Exception e)
            {
                assertTrue(driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'Bank Linked')]")).size() > 0,"Timed out waiting for bank linking.");
            }
        }
        Thread.sleep(5000);
        if (!isManualUpload) {
            assertTrue(driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'Bank Linked')]")).size() > 2, "Cashflow transaction not completed within given time.");
        }
        else
        {
            assertTrue(driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'Fact BusinessTaxIdVerification verified for " + appId + " and result was Passed')]")).size() == 2, "Fact verification could not be done within given time.");
        }*/
        //PortalFuncUtils.waitForElementToBeClickable(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab));
        Thread.sleep(15000);
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)));
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_businessName)).getText(), PortalParam.businessName, "Business name does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_email)).getText(), PortalParam.userName, "Username does not match");

        // this is a defect ala my knowledge goes. It should show dba instead of business name

        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_dba)).getText(), PortalParam.dba, "Dba does not match");
        assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_address)).getText().contains(PortalParam.businessAddress + " " + PortalParam.city), "Address does not match");
        assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_phone)).getText()), PortalParam.businessPhone, "Business phone does not match");
      /*  String a=driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_reqAmount)).getText();
        String b=PortalFuncUtils.removeSpecialChar("$ "+portalParam.amountNeed.split("-")[1]);
        assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_reqAmount)).getText()), PortalFuncUtils.removeSpecialChar("$ "+portalParam.amountNeed.split("-")[1] + ".00"), "Loan amount does not match");
//        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_businessTaxId)).getText(), PortalParam.businessTaxId, "BusinesstaxId does not match");
        assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_annualRevenue)).getText()), PortalParam.annualRevenue + ".00", "Annual revenue does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_estDate)).getText(), ("0" + (DateTimeFormatter.ofPattern("MMM").withLocale(Locale.ENGLISH)).parse(PortalParam.estMonth).get(ChronoField.MONTH_OF_YEAR)) + "/" + PortalParam.estDay + "/" + PortalParam.estYear, "Date does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_businessOwnership)).getText(), PortalParam.ownership, "Ownership does not match");
        assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_avgBankBalance)).getText()), PortalParam.aveBankBalance + ".00", "Average bank balance does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_existingLoan)).getText(), PortalParam.existingLoan, "Existing loan does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_industry)).getText(), PortalParam.industry, "Industry does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_purposeOfFunds)).getText(), PortalParam.purposeOfFunds, "Purpose of funds does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_entityType)).getText(), PortalParam.entityType, "Entity type does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_leadSource)).getText(), "Organic", "Lead source does not match");
       */ try {

        	// uploadStatementAndVerifyCashFlow();

           /* if (!isManualUpload) {
                verifyCashFlow();
            } else {
                uploadStatementAndVerifyCashFlow();
            }*/
         //   completeSearchVerification(isManualUpload);
            
            completeBusinessCreditRiskVerification();
            uploadStatementAndVerifyCashFlow();
            completeBankruptcyVerification();
            verifyVerificationDashboard();
            computeOffer();   

          //  uploadStatementAndVerifyCashFlow();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void verifyEditDealDetails(String appId,Boolean isManualUpload) throws Exception {
        PortalFuncUtils.waitForPageToLoad(driver);
        Thread.sleep(8000);
         assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)));
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_businessName)).getText(), PortalParam.businessName, "Business name does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_email)).getText(), PortalParam.userName, "Username does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_dba)).getText(), PortalParam.dba, "Dba does not match");
        assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_address)).getText().contains(PortalParam.businessAddress + " " + PortalParam.city), "Address does not match");
        assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_phone)).getText()), PortalParam.businessPhone, "Business phone does not match");
      try {
            completeBusinessCreditRiskVerification();
            uploadStatementAndVerifyCashFlow();
            completeBankruptcyVerification();
            verifyVerificationDashboard();
            computeOfferForDeal();   
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    private void completeBusinessCreditRiskVerification() throws Exception {
    	 PortalFuncUtils.waitForPageToLoad(driver);
    	 assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)),"Verification dashboard is not visible");
         assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)),"Verification dashboard is not clickable");
         PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab),"Verification Dashboard");
         //BusinessCreditRiskVerification();
         
         assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable.");
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
         PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_businessCreditReport),"Business Credit Report");
         PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_businessCreditReportButton),"Business Credit Report");
         Thread.sleep(2000);
         if(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_businessCreditReportCallExperian)).isDisplayed()){
        	 Thread.sleep(3000);
        	 driver.findElement(By.xpath("//span[text()='Bold Bean STANLEY SMITH BARNEY LLC']")).click();
        	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_businessCreditReportCallExperian),"Business Credit Report");
             
         }else {        	     
         }
         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Business Report Pulled')]")));
         wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Business Report Pulled')]")));
         logger.info("Business Report Pulled successfully");
         BusinessCreditRiskVerification();
         logger.info("Business Credit Risk Verification completed successfully");
         
        
    }

    private void BusinessCreditRiskVerification() throws Exception {
    	  try {
              PortalFuncUtils.waitForPageToLoad(driver);
              logger.info("Verifying dashboard status.");
              logger.info("=============================");
              wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
              PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard), "Verification Dashboard");
              List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
              for (int i = 1; i < 10; i++) {
                  if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("Business Credit Risk Verification"))
                  {
                      assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "Completed", "Status is not InProgress.");
                      assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "Passed", "Verification result is not Failed.");
                  }}}catch (Exception e) {
                	  throw new Exception(e.getMessage());
				}
		
	}
	private void uploadStatementAndVerifyCashFlow() throws Exception {
        try {
            PortalFuncUtils.waitForPageToLoad(driver);
           /* assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)),"Verification dashboard not clickable.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
          */ 
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_uploadBankStatementButton)),"Upload bank statement button not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_uploadBankStatementButton), "Upload Bank Statement");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox)),"Bank name can not be edited");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox), "Test Bank");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox), PortalParam.manualBankAccountNumber);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox), "Savings");
            if (driver.findElement(By.xpath(".//*[@id='items']/vaadin-combo-box-item[contains(.,'Savings')]")).isDisplayed()) {
                Thread.sleep(2000);
                driver.findElement(By.xpath(".//*[@id='items']/vaadin-combo-box-item[contains(.,'Savings')]")).click();
            }
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton)),"Select file button not seen.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton)),"Select file button not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton), "Select Files");
            fileupload("Bank Statement");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton)),"Submit file button not seen.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton)),"Submit file button not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton), "Submit File");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Bank Statement Uploaded')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Bank Statement Uploaded')]")));
            logger.info("Bank statement uploaded successfully");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
            /*assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog_bankLinkedUpdate)),"Bank linking update not seen in activity history");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab)),"Cashflow tab not visible.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab)),"Cashflow tab not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab), "Cashflow tab");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown)),"Edit cashflow not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown), "Cashflow dropdown");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount)),"Set as Funding account not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount), "Set As Funding Account");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Account preference funding done')]")),"Account preference funding done message not visible");
            assertTrue(PortalFuncUtils.waitforInvisibilityOf(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Account preference funding done')]")));
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown)),"Edit cashflow dropdown not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown), "Cashflow dropdown");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount)),"Set as Cashflow account not clickable");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount), "Set As Cashflow Account");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Account preference cashflow done')]")),"Account preference cashflow done message not seen.");
            assertTrue(PortalFuncUtils.waitforInvisibilityOf(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Account preference cashflow done')]")));*/
            navigateCashflow();
            List<WebElement> cashFlowAccounts = driver.findElements(By.xpath("//div[starts-with(@id,'cfPanelcfOne')]"));
            for(int i=1;i<=cashFlowAccounts.size();i++)
            {
                if(driver.findElements(By.xpath("//div[starts-with(@id,'cfPanelcfOne')]["+i+"]//h4[contains(.,'-"+PortalParam.manualBankAccountNumber+"')]")).size() == 1)
                {
                    assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//h4/ul//a[@role='button']")),"Edit cashflow dropdown not visible");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//h4/ul//a[@role='button']")),"Edit cashflow dropdown not clickable");
                    PortalFuncUtils.clickButton(driver,By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//h4/ul//a[@role='button']"),"Edit cashflow dropdown");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//*[contains(text(),'Set As Funding Account')]")),"Set As Funding Account not clickable");
                    PortalFuncUtils.clickButton(driver,By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//*[contains(text(),'Set As Funding Account')]"),"Set As Funding Account");
                    new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Account preference funding done')]")));
                    new WebDriverWait(driver,60).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Account preference funding done')]")));
                    assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//h4/ul//a[@role='button']")),"Edit cashflow dropdown not visible");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//h4/ul//a[@role='button']")),"Edit cashflow dropdown not clickable");
                    PortalFuncUtils.clickButton(driver,By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//h4/ul//a[@role='button']"),"Edit cashflow dropdown");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//*[contains(text(),'Set As Cashflow Account')]")),"Set As Cashflow Account not clickable");
                    PortalFuncUtils.clickButton(driver,By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//*[contains(text(),'Set As Cashflow Account')]"),"Set As Cashflow Account");
                    new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Account preference cashflow done')]")));
                    new WebDriverWait(driver,60).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Account preference cashflow done')]")));
                    break;
                }
            }
            navigateDashboard();
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog_factVerification)),"Activity history not seen for fact verification.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)),"Verification dashboard not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab), "Verification dashboard");
            List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
            for(int i=1;i<rows.size();i++)
            {
                if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[2]")).getText().equalsIgnoreCase("Cashflow Verification"))
                {
                    wait.until(ExpectedConditions.textToBe((By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[3]")),"Completed"));
                    wait.until(ExpectedConditions.textToBe((By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[4]")),"Passed"));
                    assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[3]")).getText().equalsIgnoreCase("Completed"),"Status is not Completed");
                    assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[4]")).getText().equalsIgnoreCase("Passed"),"Verification result is not Passed");
                    break;
                }
            }
            PortalFuncUtils.scrollOnTopOfThePage(driver);
            /*assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog_factVerification)),"Activity history not seen for fact verification.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)),"Verification dashboard not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab), "Verification dashboard");*/
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void addFundingRequest() throws Exception {
        Thread.sleep(5000);
        if(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_addFundingRequestMenuButton)).isDisplayed())
        {
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_addFundingRequestMenuButton)),"Add funding request not clickable.");
            //PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_closeUploadDocsButton),"Upload Docs close");
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_addFundingRequestMenuButton),"Add Funding Request Menu");
        }
        else
        {
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editMenuButton)),"Edit menu not clickable.");
            //PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_closeUploadDocsButton),"Upload Docs close");
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_editMenuButton),"Edit Request Menu");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_addFundingRequestMenuButton)),"Add funding request not clickable.");
            //PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_closeUploadDocsButton),"Upload Docs close");
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_addFundingRequestMenuButton),"Add Funding Request Menu");
        }
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_addFundingRequestButton)),"Add funding button not clickable.");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_addFundingRequestButton),"Add Funding Request");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Funding Request Added')]")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Funding Request Added')]")));
        wait.until(ExpectedConditions.textToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_headerStatus),"Approved "));
        assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_headerStatus)).getText().contains("Approved "));
        logger.info("##############################################################");
        logger.info("##########   Application moved to APPROVED status   ##########");
        logger.info("##############################################################");
    }

    
  

    

  

   

    

    private void completeBankVerification(Boolean isManualUpload) throws Exception {
        /*PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton));
        if(!PortalFuncUtils.isVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_uploadDocsButton),driver))
        {
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton),"Actions");
            wait.until(ExpectedConditions.elementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_uploadDocsButton)));
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_uploadDocsButton),"Upload Docs");
        }

        uploadIDdocs("Voided Check");*/
        verifyBankStatus("Bank Verification","Completed",isManualUpload);
    }

    private void verifyBankStatus(String check, String status,Boolean isManualUpload) throws Exception {
        logger.info("Verifying "+check+" statuses.");
        logger.info("=============================");
        List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
        for(int i=1;i<rows.size();i++)
        {
            if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[2]")).getText().equalsIgnoreCase(check))
            {
                assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[3]")).getText().equalsIgnoreCase("InProgress"),"Status is not InProgress");
                PortalFuncUtils.clickButton(driver,By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button"),"ID Verification Details");
                break;
            }
        }
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel)),"Bank verification verified label is not seen");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot)),"Bank verification screenshot is not seen");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox)),"Account number can not be entered");
        if(isManualUpload)
        {
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox), PortalParam.manualBankAccountNumber);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox), "122105155");
        }
        else {
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox), PortalParam.autobankAccountNumber);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox), "1111111111");
        }
        PortalFuncUtils.selectCheckbox(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox));
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox)),"Name of business checkbox not clickable");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifyButton),"Verify");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel)),"Bank verification verified label is not seen");
        //wait.until(ExpectedConditions.textToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel),"Bank verification is verified"));
        assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel)).getText().contains("Bank verification is verified"),"Label does not match");
        assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus)).getText().contains("Passed"),"Status is not passed");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_closeButton)),"Close button not clickable");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_closeButton),"Bank Verification windows Close");
        for(int i=1;i<rows.size();i++)
        {
            if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[2]")).getText().equalsIgnoreCase(check))
            {
                wait.until(ExpectedConditions.textToBe((By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[3]")),"Completed"));
                assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[3]")).getText().equalsIgnoreCase(status));
                break;
            }
        }
        //PortalFuncUtils.clickButton(driver,By.xpath("//*[@id='BankVerification12']//button[@aria-label=\"Close\"]"),"Bank Verification window Close");
        logger.info("Bank verification is complete");

    }

    private void computeOffer() throws Exception {

        PortalFuncUtils.scrollOnTopOfThePage(driver);
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not visible");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable");
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_computeOffer)),"Compute offer button not visible");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_computeOffer)),"Compute offer button not clickable");
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_computeOffer), "Compute Offer");
        Thread.sleep(5000);
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_computerOfferButton)),"Compute offer button not visible");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_computerOfferButton)),"Compute offer button not visible");
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_computerOfferButton), "Compute Offer button");
        Thread.sleep(2000);
        
       verifyBp2score();
    }

    private void computeOfferForDeal() throws Exception {

 	   PortalFuncUtils.scrollOnTopOfThePage(driver);
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not visible");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton)),"Actions button not clickable");
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Actions");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_computeOffer)),"Compute offer button not visible");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_computeOffer)),"Compute offer button not clickable");
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_computeOffer), "Compute Offer");
        Thread.sleep(5000);
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_computerOfferButton)),"Compute offer button not visible");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_computerOfferButton)),"Compute offer button not visible");
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_computerOfferButton), "Compute Offer button");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Offers Computed')]")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Offers Computed')]")));
        wait.until(ExpectedConditions.textToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_headerStatus), "Offer Generation "));
        PortalFuncUtils.scrollToElementandClick(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab));
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_dealGenerationLabel)),"Deal generated label is not seen");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_typeOfPaymentDropdown)),"Type of payment dropdown is not seen");
        //wait until toast message disappears. Offers compupted
        //PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_typeOfPaymentDropdown),"Daily");
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_typeOfPaymentDropdown), "Daily");
        if(driver.findElement(By.xpath("./*//*[@id='items']/vaadin-combo-box-item[contains(.,'Daily')]")).isDisplayed())
        {
            Thread.sleep(2000);
            driver.findElement(By.xpath("./*//*[@id='items']/vaadin-combo-box-item[contains(.,'Daily')]")).click();
        }
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_TAB);
        robot.keyRelease(KeyEvent.VK_TAB);
        new WebDriverWait(driver,60).until(ExpectedConditions.invisibilityOfElementWithText(By.xpath("//*[@id='dealGen']//th[contains(text(),'Number of Payments')]/following-sibling::td"),"0"));
        PortalFuncUtils.scrollToElementandClick(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_generateDealButton));
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_dealSelectedLabel)),"Deal selected label is not seen");
        editdeal();
     
  
 }
  
    public void editdeal() throws Exception {
  	  
  	  assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_editDealButton)),"Edit Action button not visible");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_editDealButton)),"Edit Action button not clickable");
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_editDealButton), "Edit button");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_dealGenerationLabel)),"Deal generated label is not seen");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_typeOfPaymentDropdown)),"Type of payment dropdown is not seen");
         PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_typeOfPaymentDropdown), "Daily");
        if(driver.findElement(By.xpath("./*//*[@id='items']/vaadin-combo-box-item[contains(.,'Daily')]")).isDisplayed())
        {
            Thread.sleep(2000);
            driver.findElement(By.xpath("./*//*[@id='items']/vaadin-combo-box-item[contains(.,'Daily')]")).click();
        }
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_TAB);
        new WebDriverWait(driver,60).until(ExpectedConditions.invisibilityOfElementWithText(By.xpath("//*[@id='dealGen']//th[contains(text(),'Number of Payments')]/following-sibling::td"),"0"));
        PortalFuncUtils.clearText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_editAmount), "Clearing text on Contact Name");          
        PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_editAmount), "6000");
        robot.keyPress(KeyEvent.VK_TAB);
        PortalFuncUtils.clearText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_editOriginationFee), "Clearing text on Contact Name");          
        PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_editOriginationFee), "100");
        robot.keyPress(KeyEvent.VK_TAB);
        PortalFuncUtils.scrollToElementandClick(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_generateDealButton));
        Thread.sleep(2000);
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_dealSelectedLabel)),"Deal selected label is not seen");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_selectedAmount)).getText(), "$6,000.00", "selected Amount is not displaying");
  	  assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_offerTab_selectedOriginationFee)).getText(), "$100.00", "selected OriginationFee is not displaying");
    }

    private void verifyBp2score() throws Exception {
    	 driver.navigate().refresh();
    	 Thread.sleep(10000);
    	 wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_scoringTab)));
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_scoringTab), "3rd Party Data");
         PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes));
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes), "BP2 Attributes");
         Thread.sleep(2000);
         assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source)),"Source tab not visible");
         assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source)),"Compute offer button not clickable");
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source), "Source");
         Thread.sleep(2000);
         assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate)),"Intermediate not visible");
         assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate)),"Compute offer button not clickable");
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate), "Intermediate");
         assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score)),"Bp2 Score is not match");
         assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score)).getText().split(":")[1].contains(PortalParam.Bp2Score),"Bp2 Score is not match");
         logger.info("Bp2 Score is "+PortalParam.Bp2Score);
	}
	private void fileupload(String doc) throws InterruptedException, AWTException {
        String userDirectory =  System.getProperty("user.dir");
        userDirectory=userDirectory.replaceAll("/", "\\\\/");

        String filepath = null;
        Thread.sleep(3000);
        if(doc.contains("Bank Statement"))
        {
            try {
                String command=PortalParam.upload_file_location +"upload.exe";
                File file = new File(PortalParam.upload_file_location+"CashflowVerificaitionPass.csv");
                Runtime.getRuntime().exec(command + " " + file.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        else {
            Thread.sleep(3000);
            try {
                String command=PortalParam.upload_file_location +"upload.exe";
                File file = new File(PortalParam.upload_file_location+"image.jpg");
                Runtime.getRuntime().exec(command + " " +file.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Thread.sleep(3000);
        /*setClipboardData(filepath);

        Thread.sleep(3000);

        Robot robot = new Robot();
        // Press CTRL+V
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);

        //Release CTRL+V
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);

        // Press Enter
        robot.keyPress(KeyEvent.VK_ENTER);

        //Release Enter Key
        robot.keyRelease(KeyEvent.VK_ENTER);*/

    }

    private void completeBankruptcyVerification() throws Exception {
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog_factVerification)),"Bankruptcy verification not seen in activity history log");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)),"Verification dashboard is not visible");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)),"Verification dashboard is not clickable");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab),"Verification Dashboard");
        bankruptcyVerification();
        PortalFuncUtils.scrollOnTopOfThePage(driver);
        //PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_headerStatus),"Pre Approved ");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_headerStatus)),"Status header is not visible");
        assertTrue(PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_headerStatus),"Pre Approved "),"Application status is not Pre Approved");
        logger.info("##################################################################");
        logger.info("##########   Application moved to PRE APPROVED status   ##########");
        logger.info("##################################################################");
        //assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_headerStatus)).getText().contains("Pre Approved "));
    }

    private void bankruptcyVerification() throws Exception {
        List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
        for(int i=1;i<rows.size();i++)
        {
            if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[2]")).getText().contains("Bankruptcy Verification")) {
                driver.findElement(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button")).click();
                Thread.sleep(2000);
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel)),"Bankruptcy verification status is not Initiated");
                assertTrue(PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton)));
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton)),"Initiated status is not seen");
                assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton)),"Initiate button is not clickable");
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton), "Initiate Now");
                assertTrue(PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox)),"Verification checkbox not seen");
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox)));
                assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox)),"Verification checkbox not checked");
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox));
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton), "Verify");
                assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel)).getText().contains("Bankruptcy verification is verified"),"Bankruptcy verification status does not match.");
                assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus)).getText().contains("Passed"),"Status is not passed");
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton),"Verification windows Close");
                logger.info("Bankruptcy verification is complete");
                break;
            }
        }
    }

    private void completeSearchVerification(Boolean isManualUpload) throws Exception {
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog_refreshButton)),"Refresh button is not visible");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog_refreshButton)),"Refresh button not clickable.");
        Thread.sleep(10000);
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog_refreshButton),"Activity Log Refresh");
        if(!isManualUpload) {
            assertTrue(driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'CashflowVerification verified')]")).size() > 0,"Cashflow updates not seen in activity history log");
        }
        else
        {
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog_factVerification)),"Fact verification not seen in activity history");
        }
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)),"Verification dashboard is not seen");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)),"Verification dashboard not clickable");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab),"Verification Dashboard");
        try {
            searchVerification();
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }

        assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel)).getText().contains("Search verification is verified"),"Search verification status is not verified.");
        assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus)).getText().contains("Passed"),"Status is not passed");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_closeButton)),"Search verification window can not be closed");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_closeButton),"Verification windows Close");
        PortalFuncUtils.scrollOnTopOfThePage(driver);
        PortalFuncUtils.waitForPageToLoad(driver);
        logger.info("Search verification is complete");
    }

    private void searchVerification() throws Exception {
        List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
        for(int i=1;i<rows.size();i++)
        {
            if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[2]")).getText().contains("Search Verification")) {
                driver.findElement(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button")).click();
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel)),"Not initiated label is not seen");
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton)));
                assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton)),"Initiate Now button not clickable");
                Thread.sleep(3000);
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton), "Initiate Now");
                PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox), "www.catesting.com");
                PortalFuncUtils.scrollToBottomOfThePage(driver);
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton), "Verify");
                break;
            }
        }
    }

    private void verifyVerificationDashboard() throws Exception {

        PortalFuncUtils.waitForPageToLoad(driver);
        logger.info("Verifying dashboard status.");
        logger.info("=============================");
        List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
        for (int i = 1; i < 10; i++) {
        	if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[2]")).getText().contains("Business Credit Risk Verification")) {
            assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "Completed", "Status is not completed.");
            assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "Passed", "Verification Status is not passed.");
            assertTrue(!driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[6]")).getText().isEmpty());
            break;
        	}
        }
        wait.until(ExpectedConditions.textToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_headerStatus), "Pre Approved "));
    }

    private void verifyCashFlow() throws Exception {
        try {
            navigateCashflow();
            List<WebElement> cashFlowAccounts = driver.findElements(By.xpath("//div[starts-with(@id,'cfPanelcfOne')]"));
            for(int i=1;i<=cashFlowAccounts.size();i++)
            {
                if(driver.findElements(By.xpath("//div[starts-with(@id,'cfPanelcfOne')]["+i+"]//h4[contains(.,'-"+PortalParam.autobankAccountNumber+"-checking')]")).size() == 1)
                {
                    assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//h4/ul//a[@role='button']")),"Edit cashflow dropdown not visible");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//h4/ul//a[@role='button']")),"Edit cashflow dropdown not clickable");
                    PortalFuncUtils.clickButton(driver,By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//h4/ul//a[@role='button']"),"Edit cashflow dropdown");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//*[contains(text(),'Set As Funding Account')]")),"Set As Funding Account not clickable");
                    PortalFuncUtils.clickButton(driver,By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//*[contains(text(),'Set As Funding Account')]"),"Set As Funding Account");
                    new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Account preference funding done')]")));
                    new WebDriverWait(driver,60).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Account preference funding done')]")));
                    assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//h4/ul//a[@role='button']")),"Edit cashflow dropdown not visible");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//h4/ul//a[@role='button']")),"Edit cashflow dropdown not clickable");
                    PortalFuncUtils.clickButton(driver,By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//h4/ul//a[@role='button']"),"Edit cashflow dropdown");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//*[contains(text(),'Set As Cashflow Account')]")),"Set As Cashflow Account not clickable");
                    PortalFuncUtils.clickButton(driver,By.xpath("//app-cashflow-info[@id='cashflowDetails']//div[@id='cfPanelcfOne-"+(i-1)+"']//*[contains(text(),'Set As Cashflow Account')]"),"Set As Cashflow Account");
                    new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Account preference cashflow done')]")));
                    new WebDriverWait(driver,60).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Account preference cashflow done')]")));
                    break;
                }
            }
            navigateDashboard();
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_activityLog_factVerification)),"Activity history not seen for fact verification.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)),"Verification dashboard not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab), "Verification dashboard");
            List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
            for(int i=1;i<rows.size();i++)
            {
                if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[2]")).getText().equalsIgnoreCase("Cashflow Verification"))
                {
                    wait.until(ExpectedConditions.textToBe((By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[3]")),"Completed"));
                    wait.until(ExpectedConditions.textToBe((By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[4]")),"Passed"));
                    assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[3]")).getText().equalsIgnoreCase("Completed"),"Status is not Completed");
                    assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr["+i+"]/td[4]")).getText().equalsIgnoreCase("Passed"),"Verification result is not Passed");
                    break;
                }
            }
            PortalFuncUtils.scrollOnTopOfThePage(driver);
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public void navigateCashflow() throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_cashflowTab),"Cashflow tab");
    }

    public VerificationDashboardTab navigateDashboard() throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab),"Verification Dashboard tab");
        return new VerificationDashboardTab(driver);

    }

    public void setClipboardData(String clipboardData) {
        StringSelection stringSelection = new StringSelection(clipboardData);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

    }

  

   
   

    public void verifyBp2Score(String appId, Boolean isManualUpload,int bp2Score) throws Exception {
        PortalFuncUtils.waitForPageToLoad(driver);
        if (isManualUpload) {
            Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(360, SECONDS).pollingEvery(30, SECONDS).ignoring(NoSuchElementException.class);
            Boolean function = wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    driver.navigate().refresh();
                    System.out.println("Verifying dimension");
                    System.out.println(driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'Fact BusinessTaxIdVerification verified for " + appId + " and result was Passed')]")).size());
                    return (driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'Fact BusinessTaxIdVerification verified for " + appId + " and result was Passed')]")).size() == 2);
                }
            });
        } else {
            Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(360, SECONDS).pollingEvery(30, SECONDS).ignoring(NoSuchElementException.class);
            Boolean function = wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    driver.navigate().refresh();
                    System.out.println("Verifying dimension");
                    System.out.println(driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'Fact CashflowVerification verified for " + appId + " and result was Passed')]")).size());
                    return (driver.findElements(By.xpath("//div[starts-with(@class,'activity-stream')]//*[contains(.,'Fact CashflowVerification verified for " + appId + " and result was Passed')]")).size() == 2);
                }
            });
        }
        Thread.sleep(5000);
        //PortalFuncUtils.waitForElementToBeClickable(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab));
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboardTab)));
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_businessName)).getText(), PortalParam.businessName, "Business name does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_email)).getText(), PortalParam.userName, "Username does not match");

        // this is a defect ala my knowledge goes. It should show dba instead of business name

        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_dba)).getText(), PortalParam.dba, "Dba does not match");
        assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_address)).getText().contains(PortalParam.businessAddress + " " + PortalParam.city), "Address does not match");
        assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_phone)).getText()), PortalParam.businessPhone, "Business phone does not match");
        assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_reqAmount)).getText()), PortalFuncUtils.removeSpecialChar(portalParam.amountNeed.split("-")[1] + ".00"), "Loan amount does not match");
//        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_businessTaxId)).getText(), PortalParam.businessTaxId, "BusinesstaxId does not match");
        assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_annualRevenue)).getText()), PortalParam.annualRevenue + ".00", "Annual revenue does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_estDate)).getText(), ("0" + (DateTimeFormatter.ofPattern("MMM").withLocale(Locale.ENGLISH)).parse(PortalParam.estMonth).get(ChronoField.MONTH_OF_YEAR)) + "/" + PortalParam.estDay + "/" + PortalParam.estYear, "Date does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_businessOwnership)).getText(), PortalParam.ownership, "Ownership does not match");
        assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_avgBankBalance)).getText()), PortalParam.aveBankBalance + ".00", "Average bank balance does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_existingLoan)).getText(), PortalParam.existingLoan, "Existing loan does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_industry)).getText(), PortalParam.industry, "Industry does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_purposeOfFunds)).getText(), PortalParam.purposeOfFunds, "Purpose of funds does not match");
        assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_entityType)).getText(), PortalParam.entityType, "Entity type does not match");
        try {
            if (!isManualUpload) {
                verifyCashFlow();
            } else {
                uploadStatementAndVerifyCashFlow();
            }
            completeSearchVerification(isManualUpload);
            completeBankruptcyVerification();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void verifyAnualRevenueVerification(String appId) throws Exception{
        try
        {
            PortalFuncUtils.waitForPageToLoad(driver);
            String a=UIObjParam.backoffice_appDetailspage_verificationDashboard;
            PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard);
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_email)).getText(), PortalParam.userName, "Username does not match");
            assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_annualRevenue)).getText()), PortalParam.annualRevenue, "Annual revenue does not match");
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
        verifyAnualRevenueReview(appId);
    }

    private void verifyAnualRevenueReview(String appId) throws Exception{
        try
        {
            PortalFuncUtils.waitForPageToLoad(driver);
            logger.info("Verifying dashboard status.");
            logger.info("=============================");
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard), "Verification Dashboard");
            List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
            for (int i = 1; i < rows.size(); i++)
            {
                if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("Annual Revenue Verification"))
                {
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "Initiated", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "None", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[5]")).getText(), "System", "by system");
                    driver.findElement(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button")).click();
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_CheckBox), "Click on checkbox ");
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerifyButton), "click on verify button ");
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationPopUpClose), "click on close popup button ");
                    Thread.sleep(5000);
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "Completed", "Annual revenue Manually not verified");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "Passed", "Status is not InProgress.");
                    assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[5]")).getText().equalsIgnoreCase(PortalParam.backofficeUsername), "Username does not match");
                    break;
                }
            }
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public void verifyTimeInBusinessVerification(String appId) throws Exception{
        try
        {
            PortalFuncUtils.waitForPageToLoad(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_email)).getText().equalsIgnoreCase(PortalParam.userName), "Username does not match");
            assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_annualRevenue)).getText()), PortalParam.annualRevenue, "Annual revenue does not match");
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
        verifyTimeInBusinessReview(appId);
    }

    private void verifyTimeInBusinessReview(String appId) throws Exception{
        try
        {
            PortalFuncUtils.waitForPageToLoad(driver);
            logger.info("Verifying dashboard status.");
            logger.info("=============================");
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard), "Verification Dashboard");
            List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
            for (int i = 1; i < 10; i++)
            {
                if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("Time In Business Verification"))
                {
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "Initiated", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "None", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[5]")).getText(), "System", "by system");
                    driver.findElement(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button")).click();
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox), "Click on checkbox ");
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifybutton), "click on verify button ");
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifyclosebutton), "click on close popup button ");
                    Thread.sleep(5000);
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "Completed", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "Passed", "Status is not InProgress.");
                    assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[5]")).getText().equalsIgnoreCase(PortalParam.backofficeUsername), "Username does not match");
                    break;
                }

            }
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public void verifyAddressVerification(String appId) throws Exception{
        try
        {
            PortalFuncUtils.waitForPageToLoad(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_email)).getText(), PortalParam.userName, "Username does not match");
            assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_annualRevenue)).getText()), PortalParam.annualRevenue, "Annual revenue does not match");
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
        verifyAddressVerificationReview(appId);
    }

    private void verifyAddressVerificationReview(String appId) throws Exception{
        try
        {
            PortalFuncUtils.waitForPageToLoad(driver);
            logger.info("Verifying dashboard status.");
            logger.info("=============================");
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard), "Verification Dashboard");
            List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
            for (int i = 1; i < 10; i++)
            {
                if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("Business Address Verification"))
                {
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "InProgress", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "Failed", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[5]")).getText(), "System", "by system");
                    driver.findElement(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button")).click();
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_addressVerification_verifypopupclose), "click on close popup button ");
                    Thread.sleep(5000);
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "InProgress", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "Failed", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[5]")).getText(), "System", "by system");
                    break;
                }
            }
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public void verifyDataMerchNM(String appId) throws Exception{
        try
        {
            PortalFuncUtils.waitForPageToLoad(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_email)).getText(), PortalParam.userName, "Username does not match");
            assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_annualRevenue)).getText()), PortalParam.annualRevenue, "Annual revenue does not match");
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
        verifyDataMerchNMReview(appId);
    }

    private void verifyDataMerchNMReview(String appId) throws Exception{
        try
        {
            PortalFuncUtils.waitForPageToLoad(driver);
            logger.info("Verifying dashboard status.");
            logger.info("=============================");
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard), "Verification Dashboard");
            List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
            for (int i = 1; i < 10; i++)
            {
                if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("DataMerch Verification"))
                {
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "Initiated", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "None", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[5]")).getText(), "System", "by system");
                    driver.findElement(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button")).click();
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox), "Click on checkbox ");
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyButton), "click on verify button ");
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyCloseButton), "click on close popup button ");
                    Thread.sleep(5000);
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "Completed", "Status is not Completed.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "Passed", "Status is not Passed.");
                    assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[5]")).getText().equalsIgnoreCase(PortalParam.backofficeUsername), "Username does not match");
                    break;
                }
            }
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public void verifyPersonalReport(String appId) throws Exception{
        try
        {
            PortalFuncUtils.waitForPageToLoad(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_email)).getText(), PortalParam.userName, "Username does not match");
            assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_annualRevenue)).getText()), PortalParam.annualRevenue, "Annual revenue does not match");
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
        verifyPersonalReportData(appId);
    }

    private void verifyPersonalReportData(String appId) throws Exception{
        try {
            PortalFuncUtils.waitForPageToLoad(driver);
            logger.info("Verifying dashboard status.");
            logger.info("=============================");
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard), "Verification Dashboard");
            List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
            for (int i = 1; i < 10; i++) {
                if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("Credit Score Verification"))
                {
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "Completed", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "Passed", "Verification result is not Failed.");
                    driver.findElement(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button")).click();
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab)),"pScore tab not clickable");
                    PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab),"PScore");
                    assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel)),"pScore value label is not seen");
                    assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel)).getText().contains("717"),"no pScore found");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton)),"Close button not clickable");
                    PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton),"Credit score close");
                    break;
                }

            }
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_scoringTab)));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_scoringTab), "3rd Party Data");
            PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport), "Experian Personal Report");
            String firstName=driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_firstName)).getText();
            if(firstName.equalsIgnoreCase(PortalParam.ownerFirstName)){
                logger.info("Owner First Name in experian report:" +firstName );
            }
            PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown));
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public void verifyPersonalFrozeReport(String appId) throws Exception{
        try
        {
            PortalFuncUtils.waitForPageToLoad(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_email)).getText(), PortalParam.userName, "Username does not match");
            assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_annualRevenue)).getText()), PortalParam.annualRevenue, "Annual revenue does not match");
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }


        verifyPersonalFrozeReportData(appId);
    }

    public void verifyPersonalNoRecordReport(String appId) throws Exception{
        try
        {
            PortalFuncUtils.waitForPageToLoad(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_email)).getText(), PortalParam.userName, "Username does not match");
            assertEquals(PortalFuncUtils.removeSpecialChar(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_businessinfo_annualRevenue)).getText()), PortalParam.annualRevenue, "Annual revenue does not match");
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }


        verifyPersonalNoRecordReportData(appId);
    }

    private void verifyPersonalNoRecordReportData(String appId) throws Exception {
        try {
            PortalFuncUtils.waitForPageToLoad(driver);
            logger.info("Verifying dashboard status.");
            logger.info("=============================");
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard), "Verification Dashboard");
            List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
            for (int i = 1; i < 10; i++) {
                if (driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("Credit Score Verification")) {
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "InProgress", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "Failed", "Verification result is not Failed.");
                    assertTrue(!driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[6]")).getText().isEmpty());
                    driver.findElement(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button")).click();
                    assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel)), "Not initiated label is not seen");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab)), "pScore tab not clickable");
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab), "PScore");
                    assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel)), "pScore value label is not seen");
                    assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel)).getText().contains("681"), "no pScore found");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton)), "Close button not clickable");
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton), "Credit score close");
                    break;
                }

            }
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_scoringTab)));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_scoringTab), "3rd Party Data");
            PortalFuncUtils.scrollToElement(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport), "Experian Personal Report");
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage)));
            String NoRecordMessage = driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage)).getText();
            logger.info("Owner NoRecord Message in experian report:" + NoRecordMessage);
            PortalFuncUtils.scrollToElement(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private void verifyPersonalFrozeReportData(String appId) throws Exception{
        try {
            PortalFuncUtils.waitForPageToLoad(driver);
            logger.info("Verifying dashboard status.");
            logger.info("=============================");
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard)));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationDashboard), "Verification Dashboard");
            List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
            for (int i = 1; i < 10; i++) {
                if(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("Credit Score Verification"))
                {
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText(), "InProgress", "Status is not InProgress.");
                    assertEquals(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[4]")).getText(), "Failed", "Verification result is not Failed.");
                    assertTrue(!driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[6]")).getText().isEmpty());
                    driver.findElement(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button")).click();
                    assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel)),"Not initiated label is not seen");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab)),"pScore tab not clickable");
                    PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab),"PScore");
                    assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel)),"pScore value label is not seen");
                    assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel)).getText().contains("681"),"no pScore found");
                    assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton)),"Close button not clickable");
                    PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton),"Credit score close");
                    break;
                }

            }
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_scoringTab)));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_scoringTab), "3rd Party Data");
            PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport), "Experian Personal Report");
            wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage)));
            String FrozeMessage=driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage)).getText();
            logger.info("Owner Froze Message in experian report:" +FrozeMessage );
            PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown));
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

}
