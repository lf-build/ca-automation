package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 29-11-2017.
 */
public class BackOfficeMainPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BackOfficeMainPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public BackOfficeMainPage(WebDriver driver) throws Exception {

        this.driver = driver;
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown)));
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_searchButton)));
        logger.info("=========== BackOfficeMainPage is loaded============");
    }
    public BackOfficeMainPage(){}

    public BackOfficeLoginPage logout(WebDriver driver) throws Exception {
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown)));
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown)));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown),"Profile");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_logoutLink)));
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_logoutLink)));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_header_logoutLink),"Log out");
        return new BackOfficeLoginPage(driver);

    }

    public BackOfficeDashboardPage getDashboard() throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_dashboardTab),"Dashboard");
        return new BackOfficeDashboardPage(driver);
    }

    public BackOfficeNewApplicationPage getNewApplication() throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_newAppTab),"New Application");
        return new BackOfficeNewApplicationPage(driver);
    }

    public BackOfficeUserManagementPage getUserManagement() throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_userManagementTab),"User Management");
        return new BackOfficeUserManagementPage(driver);
    }

    public BackOfficeSearchPage getSearch() throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_searchTab),"Search");
        return new BackOfficeSearchPage(driver);
    }

    public BackOfficeSearchApplicationPage getSearchApplication() throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_searchAppTab),"Search Application");
        return new BackOfficeSearchApplicationPage(driver);
    }

    public BackOfficeMyApplicationsPage getMyApplication() throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_myAppTab),"My Applications");
        return new BackOfficeMyApplicationsPage(driver);
    }

    public BackOfficeAllUsersPage getAllUsers() throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_allUsersTab),"All Users");
        return new BackOfficeAllUsersPage(driver);
    }

    public BackOfficeCreateNewUserPage getCreateNewUser() throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_createNewUserTab),"Create New User");
        return new BackOfficeCreateNewUserPage(driver);
    }
}
