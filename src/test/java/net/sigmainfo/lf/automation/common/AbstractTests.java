package net.sigmainfo.lf.automation.common;

import net.sigmainfo.lf.automation.api.function.ApiPropertiesReader;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.api.config.ApiConfig;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.portal.config.PortalConfig;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.function.PortalPropertiesReader;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import net.sigmainfo.lf.automation.portal.function.UIObjPropertiesReader;
import net.sigmainfo.lf.automation.portal.pages.DocusignPage;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.velocity.test.provider.BoolObj;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.testng.Assert;
import org.testng.annotations.*;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : AbstractTests.java
 * Description          : Drives automation suite and delegates testng annotations
 * Includes             : 1. Setup and quit method for browser opening and closing for portal cases
 *                        2. Initializes test data and test property files
 *                        3. Custom Reporting methods
 */

@ContextConfiguration(classes = {PortalConfig.class, ApiConfig.class})
@TestExecutionListeners(inheritListeners = false, listeners = {
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class})
@WebAppConfiguration
//@Test
public class AbstractTests extends AbstractTestNGSpringContextTests {

    private Logger logger = LoggerFactory.getLogger(AbstractTests.class);

    static Server automationServer = new Server(9096);

    static boolean setupDone = false;
    public static boolean ifFileExist=false;
    public static long auto_start = 0;
    public static String Execution_start_time = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss").format(new Date());
    public static long auto_finish = 0;
    public static String sResBackUp = "res/TestReport.txt";
    public enum browser_list {
        IE, FF, Chrome;
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static WebDriver driver;
    public static int profile_delay = 20;
    public static int ShortSleep = 20;
    public static File casefile,portal_scenfile,portal_feafile,api_scenfile,api_feafile,feafile;
    String months[] = {"Jan", "Feb", "Mar", "Apr","May", "Jun", "Jul", "Aug", "Sep","Oct", "Nov", "Dec"};

    @Autowired
    ApiParam apiParam;

    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    @Autowired
    public
    PortalParam portalParam;

    @Autowired
    public
    UIObjParam uiObjParam;

    @Autowired
    UIObjPropertiesReader uiPropertiesReader;

    @Autowired
    public
    PortalFuncUtils portalFuncUtils;

    @Autowired
    PortalPropertiesReader portalPropertiesReader;

    @BeforeSuite(alwaysRun = true)
    public void setUpOnce() throws Exception {
        logger.info("======================== Before Suite Invoked ==============================");
        setupJetty("CA-automation", automationServer);
        logger.info("Jetty Server Started..........");
    }



    @AfterSuite(alwaysRun = true)
    public void tearDown() throws Exception {
        logger.info("======================== After Suite Invoked. ==============================");
        automationServer.getServer().stop();
        logger.info("Jetty Server Stopped..........");
    }

    @PostConstruct
    private void postConstruct() throws SQLException,Exception {
        if (!setupDone) {
            logger.info("======================== Post Construct Invoked. ==============================");
            setupDBParams();
            setupUIObjects();
            setupDone = true;
        }
    }

    public void setupUIObjects() {
        logger.info("--------------- READING UI OBJECTS ----------------");
        uiObjParam.borrower_loginpage_loginLabel= uiPropertiesReader.getBorrower_loginpage_loginLabel();
        logger.info("uiObjParam.borrower_loginpage_loginLabel :" + uiObjParam.borrower_loginpage_loginLabel);
        uiObjParam.borrower_loginpage_emailTextBox= uiPropertiesReader.getBorrower_loginpage_emailTextBox();
        logger.info("uiObjParam.borrower_loginpage_emailTextBox :" + uiObjParam.borrower_loginpage_emailTextBox);
        uiObjParam.borrower_loginpage_passwordTextBox= uiPropertiesReader.getBorrower_loginpage_passwordTextBox();
        logger.info("uiObjParam.borrower_loginpage_passwordTextBox :" + uiObjParam.borrower_loginpage_passwordTextBox);
        uiObjParam.borrower_loginpage_submitButton= uiPropertiesReader.getBorrower_loginpage_submitButton();
        logger.info("uiObjParam.borrower_loginpage_submitButton :" + uiObjParam.borrower_loginpage_submitButton);
        uiObjParam.homepage_basicTab= uiPropertiesReader.getHomepage_basicTab();
        logger.info("uiObjParam.homepage_basicTab :" + uiObjParam.homepage_basicTab);
        uiObjParam.homepage_newApplicationLabel= uiPropertiesReader.getHomepage_newApplicationLabel();
        logger.info("uiObjParam.homepage_newApplicationLabel :" + uiObjParam.homepage_newApplicationLabel);
        uiObjParam.homepage_businessTab= uiPropertiesReader.getHomepage_businessTab();
        logger.info("uiObjParam.homepage_businessTab :" + uiObjParam.homepage_businessTab);
        uiObjParam.homepage_ownerTab= uiPropertiesReader.getHomepage_ownerTab();
        logger.info("uiObjParam.homepage_ownerTab :" + uiObjParam.homepage_ownerTab);
        uiObjParam.homepage_finishTab= uiPropertiesReader.getHomepage_finishTab();
        logger.info("uiObjParam.homepage_finishTab :" + uiObjParam.homepage_finishTab);
        uiObjParam.homepage_basicTab_howMuchDoYouNeedDropdown= uiPropertiesReader.getHomepage_basicTab_howMuchDoYouNeedDropdown();
        logger.info("uiObjParam.homepage_basicTab_howMuchDoYouNeedDropdown :" + uiObjParam.homepage_basicTab_howMuchDoYouNeedDropdown);
        uiObjParam.homepage_basicTab_howSoonYouNeedDropdown= uiPropertiesReader.getHomepage_basicTab_howSoonYouNeedDropdown();
        logger.info("uiObjParam.homepage_basicTab_howSoonYouNeedDropdown :" + uiObjParam.homepage_basicTab_howSoonYouNeedDropdown);
        uiObjParam.homepage_basicTab_firstNameTextBox= uiPropertiesReader.getHomepage_basicTab_firstNameTextBox();
        logger.info("uiObjParam.homepage_basicTab_firstNameTextBox :" + uiObjParam.homepage_basicTab_firstNameTextBox);
        uiObjParam.homepage_basicTab_lastNameTextBox= uiPropertiesReader.getHomepage_basicTab_lastNameTextBox();
        logger.info("uiObjParam.homepage_basicTab_lastNameTextBox :" + uiObjParam.homepage_basicTab_lastNameTextBox);
        uiObjParam.homepage_basicTab_purposeOfFundsDropdown= uiPropertiesReader.getHomepage_basicTab_purposeOfFundsDropdown();
        logger.info("uiObjParam.homepage_basicTab_purposeOfFundsDropdown :" + uiObjParam.homepage_basicTab_purposeOfFundsDropdown);
        uiObjParam.homepage_basicTab_primaryPhoneTextBox= uiPropertiesReader.getHomepage_basicTab_primaryPhoneTextBox();
        logger.info("uiObjParam.homepage_basicTab_primaryPhoneTextBox :" + uiObjParam.homepage_basicTab_primaryPhoneTextBox);
        uiObjParam.homepage_basicTab_emailAddressTextBox= uiPropertiesReader.getHomepage_basicTab_emailAddressTextBox();
        logger.info("uiObjParam.homepage_basicTab_emailAddressTextBox :" + uiObjParam.homepage_basicTab_emailAddressTextBox);
        uiObjParam.homepage_basicTab_passwordTextBox= uiPropertiesReader.getHomepage_basicTab_passwordTextBox();
        logger.info("uiObjParam.homepage_basicTab_passwordTextBox :" + uiObjParam.homepage_basicTab_passwordTextBox);
        uiObjParam.homepage_basicTab_confirmPasswordTextBox= uiPropertiesReader.getHomepage_basicTab_confirmPasswordTextBox();
        logger.info("uiObjParam.homepage_basicTab_confirmPasswordTextBox :" + uiObjParam.homepage_basicTab_confirmPasswordTextBox);
        uiObjParam.homepage_basicTab_cancelButton= uiPropertiesReader.getHomepage_basicTab_cancelButton();
        logger.info("uiObjParam.homepage_basicTab_cancelButton :" + uiObjParam.homepage_basicTab_cancelButton);
        uiObjParam.homepage_basicTab_nextButton= uiPropertiesReader.getHomepage_basicTab_nextButton();
        logger.info("uiObjParam.homepage_basicTab_nextButton :" + uiObjParam.homepage_basicTab_nextButton);
        uiObjParam.homepage_businessTab_legalCompanyNameTextBox= uiPropertiesReader.getHomepage_businessTab_legalCompanyNameTextBox();
        logger.info("uiObjParam.homepage_businessTab_legalCompanyNameTextBox :" + uiObjParam.homepage_businessTab_legalCompanyNameTextBox);
        uiObjParam.homepage_businessTab_businessAddressTextBox= uiPropertiesReader.getHomepage_businessTab_businessAddressTextBox();
        logger.info("uiObjParam.homepage_businessTab_businessAddressTextBox :" + uiObjParam.homepage_businessTab_businessAddressTextBox);
        uiObjParam.homepage_businessTab_cityTextBox= uiPropertiesReader.getHomepage_businessTab_cityTextBox();
        logger.info("uiObjParam.homepage_businessTab_cityTextBox :" + uiObjParam.homepage_businessTab_cityTextBox);
        uiObjParam.homepage_businessTab_stateDropdown= uiPropertiesReader.getHomepage_businessTab_stateDropdown();
        logger.info("uiObjParam.homepage_businessTab_stateDropdown :" + uiObjParam.homepage_businessTab_stateDropdown);
        uiObjParam.homepage_businessTab_postalCodeTextBox= uiPropertiesReader.getHomepage_businessTab_postalCodeTextBox();
        logger.info("uiObjParam.homepage_businessTab_postalCodeTextBox :" + uiObjParam.homepage_businessTab_postalCodeTextBox);
        uiObjParam.homepage_businessTab_rentOrOwnDropdown= uiPropertiesReader.getHomepage_businessTab_rentOrOwnDropdown();
        logger.info("uiObjParam.homepage_businessTab_rentOrOwnDropdown :" + uiObjParam.homepage_businessTab_rentOrOwnDropdown);
        uiObjParam.homepage_businessTab_industryDropdown= uiPropertiesReader.getHomepage_businessTab_industryDropdown();
        logger.info("uiObjParam.homepage_businessTab_industryDropdown :" + uiObjParam.homepage_businessTab_industryDropdown);
        uiObjParam.homepage_businessTab_businessTaxIdTextBox= uiPropertiesReader.getHomepage_businessTab_businessTaxIdTextBox();
        logger.info("uiObjParam.homepage_businessTab_businessTaxIdTextBox :" + uiObjParam.homepage_businessTab_businessTaxIdTextBox);
        uiObjParam.homepage_businessTab_entityTypeDropdown= uiPropertiesReader.getHomepage_businessTab_entityTypeDropdown();
        logger.info("uiObjParam.homepage_businessTab_entityTypeDropdown :" + uiObjParam.homepage_businessTab_entityTypeDropdown);
        uiObjParam.homepage_businessTab_estMonthDropdown= uiPropertiesReader.getHomepage_businessTab_estMonthDropdown();
        logger.info("uiObjParam.homepage_businessTab_estMonthDropdown :" + uiObjParam.homepage_businessTab_estMonthDropdown);
        uiObjParam.homepage_businessTab_estDayDropdown= uiPropertiesReader.getHomepage_businessTab_estDayDropdown();
        logger.info("uiObjParam.homepage_businessTab_estDayDropdown :" + uiObjParam.homepage_businessTab_estDayDropdown);
        uiObjParam.homepage_businessTab_estYearDropdown= uiPropertiesReader.getHomepage_businessTab_estYearDropdown();
        logger.info("uiObjParam.homepage_businessTab_estYearDropdown :" + uiObjParam.homepage_businessTab_estYearDropdown);
        uiObjParam.homepage_businessTab_cancelButton= uiPropertiesReader.getHomepage_businessTab_cancelButton();
        logger.info("uiObjParam.homepage_businessTab_cancelButton :" + uiObjParam.homepage_businessTab_cancelButton);
        uiObjParam.homepage_businessTab_nextButton= uiPropertiesReader.getHomepage_businessTab_nextButton();
        logger.info("uiObjParam.homepage_businessTab_nextButton :" + uiObjParam.homepage_businessTab_nextButton);
        uiObjParam.homepage_ownerTab_firstNameTextBox= uiPropertiesReader.getHomepage_ownerTab_firstNameTextBox();
        logger.info("uiObjParam.homepage_ownerTab_firstNameTextBox :" + uiObjParam.homepage_ownerTab_firstNameTextBox);
        uiObjParam.homepage_ownerTab_lastNameTextBox= uiPropertiesReader.getHomepage_ownerTab_lastNameTextBox();
        logger.info("uiObjParam.homepage_ownerTab_lastNameTextBox :" + uiObjParam.homepage_ownerTab_lastNameTextBox);
        uiObjParam.homepage_ownerTab_homeAddressTextBox= uiPropertiesReader.getHomepage_ownerTab_homeAddressTextBox();
        logger.info("uiObjParam.homepage_ownerTab_homeAddressTextBox :" + uiObjParam.homepage_ownerTab_homeAddressTextBox);
        uiObjParam.homepage_ownerTab_homeCityTextBox= uiPropertiesReader.getHomepage_ownerTab_homeCityTextBox();
        logger.info("uiObjParam.homepage_ownerTab_homeCityTextBox :" + uiObjParam.homepage_ownerTab_homeCityTextBox);
        uiObjParam.homepage_ownerTab_homeStateDropdown= uiPropertiesReader.getHomepage_ownerTab_homeStateDropdown();
        logger.info("uiObjParam.homepage_ownerTab_homeStateDropdown :" + uiObjParam.homepage_ownerTab_homeStateDropdown);
        uiObjParam.homepage_ownerTab_postalCodeTextBox= uiPropertiesReader.getHomepage_ownerTab_postalCodeTextBox();
        logger.info("uiObjParam.homepage_ownerTab_postalCodeTextBox :" + uiObjParam.homepage_ownerTab_postalCodeTextBox);
        uiObjParam.homepage_ownerTab_mobilePhoneTextBox= uiPropertiesReader.getHomepage_ownerTab_mobilePhoneTextBox();
        logger.info("uiObjParam.homepage_ownerTab_mobilePhoneTextBox :" + uiObjParam.homepage_ownerTab_mobilePhoneTextBox);
        uiObjParam.homepage_ownerTab_ownershipTextBox= uiPropertiesReader.getHomepage_ownerTab_ownershipTextBox();
        logger.info("uiObjParam.homepage_ownerTab_ownershipTextBox :" + uiObjParam.homepage_ownerTab_ownershipTextBox);
        uiObjParam.homepage_ownerTab_ssnTextBox= uiPropertiesReader.getHomepage_ownerTab_ssnTextBox();
        logger.info("uiObjParam.homepage_ownerTab_ssnTextBox :" + uiObjParam.homepage_ownerTab_ssnTextBox);
        uiObjParam.homepage_ownerTab_emailAddressTextBox= uiPropertiesReader.getHomepage_ownerTab_emailAddressTextBox();
        logger.info("uiObjParam.homepage_ownerTab_emailAddressTextBox :" + uiObjParam.homepage_ownerTab_emailAddressTextBox);
        uiObjParam.homepage_ownerTab_mobDropdown= uiPropertiesReader.getHomepage_ownerTab_mobDropdown();
        logger.info("uiObjParam.homepage_ownerTab_mobDropdown :" + uiObjParam.homepage_ownerTab_mobDropdown);
        uiObjParam.homepage_ownerTab_dobDropdown= uiPropertiesReader.getHomepage_ownerTab_dobDropdown();
        logger.info("uiObjParam.homepage_ownerTab_dobDropdown :" + uiObjParam.homepage_ownerTab_dobDropdown);
        uiObjParam.homepage_ownerTab_yobDropdown= uiPropertiesReader.getHomepage_ownerTab_yobDropdown();
        logger.info("uiObjParam.homepage_ownerTab_yobDropdown :" + uiObjParam.homepage_ownerTab_yobDropdown);
        uiObjParam.homepage_ownerTab_nextButton= uiPropertiesReader.getHomepage_ownerTab_nextButton();
        logger.info("uiObjParam.homepage_ownerTab_nextButton :" + uiObjParam.homepage_ownerTab_nextButton);
        uiObjParam.homepage_ownerTab_Country= uiPropertiesReader.getHomepage_ownerTab_Country();
        logger.info("uiObjParam.homepage_ownerTab_Country :" + uiObjParam.homepage_ownerTab_Country);

        
        uiObjParam.homepage_finishTab_annualRevenueTextBox= uiPropertiesReader.getHomepage_finishTab_annualRevenueTextBox();
        logger.info("uiObjParam.homepage_finishTab_annualRevenueTextBox :" + uiObjParam.homepage_finishTab_annualRevenueTextBox);
        uiObjParam.homepage_finishTab_avgBankBalanceTextBox= uiPropertiesReader.getHomepage_finishTab_avgBankBalanceTextBox();
        logger.info("uiObjParam.homepage_finishTab_avgBankBalanceTextBox :" + uiObjParam.homepage_finishTab_avgBankBalanceTextBox);
        uiObjParam.homepage_finishTab_existingLoanStatusDropdown= uiPropertiesReader.getHomepage_finishTab_existingLoanStatusDropdown();
        logger.info("uiObjParam.homepage_finishTab_existingLoanStatusDropdown :" + uiObjParam.homepage_finishTab_existingLoanStatusDropdown);
        uiObjParam.homepage_finishTab_agreementCheckBox= uiPropertiesReader.getHomepage_finishTab_agreementCheckBox();
        logger.info("uiObjParam.homepage_finishTab_agreementCheckBox :" + uiObjParam.homepage_finishTab_agreementCheckBox);
        uiObjParam.homepage_finishTab_cancelButton= uiPropertiesReader.getHomepage_finishTab_cancelButton();
        logger.info("uiObjParam.homepage_finishTab_cancelButton :" + uiObjParam.homepage_finishTab_cancelButton);
        uiObjParam.homepage_finishTab_submitButton= uiPropertiesReader.getHomepage_finishTab_submitButton();
        logger.info("uiObjParam.homepage_finishTab_submitButton :" + uiObjParam.homepage_finishTab_submitButton);
        uiObjParam.homepage_businessTab_Country= uiPropertiesReader.getHomepage_businessTab_Country();
        logger.info("uiObjParam.homepage_businessTab_Country :" + uiObjParam.homepage_businessTab_Country);

        uiObjParam.dashboard_overviewTab= uiPropertiesReader.getDashboard_overviewTab();
        logger.info("uiObjParam.dashboard_overviewTab :" + uiObjParam.dashboard_overviewTab);
        uiObjParam.dashboard_todoTab= uiPropertiesReader.getDashboard_todoTab();
        logger.info("uiObjParam.dashboard_todoTab :" + uiObjParam.dashboard_todoTab);
        uiObjParam.dashboard_profileTab= uiPropertiesReader.getDashboard_profileTab();
        logger.info("uiObjParam.dashboard_profileTab :" + uiObjParam.dashboard_profileTab);
        uiObjParam.dashboard_changePasswordTab= uiPropertiesReader.getDashboard_changePasswordTab();
        logger.info("uiObjParam.dashboard_changePasswordTab :" + uiObjParam.dashboard_changePasswordTab);
        uiObjParam.dashboard_borrowerNameLabel= uiPropertiesReader.getDashboard_borrowerNameLabel();
        logger.info("uiObjParam.dashboard_borrowerNameLabel :" + uiObjParam.dashboard_borrowerNameLabel);
        uiObjParam.dashboard_appIdLabel= uiPropertiesReader.getDashboard_appIdLabel();
        logger.info("uiObjParam.dashboard_appIdLabel :" + uiObjParam.dashboard_appIdLabel);
        uiObjParam.dashboard_phoneNumberLabel= uiPropertiesReader.getDashboard_phoneNumberLabel();
        logger.info("uiObjParam.dashboard_phoneNumberLabel :" + uiObjParam.dashboard_phoneNumberLabel);
        uiObjParam.dashboard_emailIdLabel= uiPropertiesReader.getDashboard_emailIdLabel();
        logger.info("uiObjParam.dashboard_emailIdLabel :" + uiObjParam.dashboard_emailIdLabel);
        uiObjParam.dashboard_loanAmountAppliedLabel= uiPropertiesReader.getDashboard_loanAmountAppliedLabel();
        logger.info("uiObjParam.dashboard_loanAmountAppliedLabel :" + uiObjParam.dashboard_loanAmountAppliedLabel);
        uiObjParam.dashboard_purposeOfFundsLabel= uiPropertiesReader.getDashboard_purposeOfFundsLabel();
        logger.info("uiObjParam.dashboard_purposeOfFundsLabel :" + uiObjParam.dashboard_purposeOfFundsLabel);
        uiObjParam.dashboard_bankVerificationRequestMsg= uiPropertiesReader.getDashboard_bankVerificationRequestMsg();
        logger.info("uiObjParam.dashboard_bankVerificationRequestMsg :" + uiObjParam.dashboard_bankVerificationRequestMsg);
        uiObjParam.dashboard_bankVerificationRequestMsg= uiPropertiesReader.getDashboard_bankVerificationRequestMsg();
        logger.info("uiObjParam.dashboard_linkBankAccountButton :" + uiObjParam.dashboard_linkBankAccountButton);
        uiObjParam.dashboard_profileDropdown= uiPropertiesReader.getDashboard_profileDropdown();
        logger.info("uiObjParam.dashboard_profileDropdown :" + uiObjParam.dashboard_profileDropdown);
        uiObjParam.dashboard_logoutLink= uiPropertiesReader.getDashboard_logoutLink();
        logger.info("uiObjParam.dashboard_logoutLink :" + uiObjParam.dashboard_logoutLink);
        uiObjParam.dashboard_linkBankAccountButton= uiPropertiesReader.getDashboard_linkBankAccountButton();
        logger.info("uiObjParam.dashboard_linkBankAccountButton :" + uiObjParam.dashboard_linkBankAccountButton);
        uiObjParam.backoffice_appDetailspage_verificationDashboardTab= uiPropertiesReader.getBackoffice_appDetailspage_verificationDashboardTab();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationDashboardTab :" + uiObjParam.backoffice_appDetailspage_verificationDashboardTab);
        uiObjParam.backoffice_appDetailspage_cashflowTab= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab :" + uiObjParam.backoffice_appDetailspage_cashflowTab);
        uiObjParam.borrower_todo_reapplyButton= uiPropertiesReader.getBorrower_todo_reapplyButton();
        logger.info("uiObjParam.borrower_todo_reapplyButton :" + uiObjParam.borrower_todo_reapplyButton);
        uiObjParam.borrower_todo_reapplyButton_yes= uiPropertiesReader.getBorrower_todo_reapplyButton_yes();
        logger.info("uiObjParam.borrower_todo_reapplyButton_yes :" + uiObjParam.borrower_todo_reapplyButton_yes);

        // ########### BACKOFFICE OBJECTS

        uiObjParam.backoffice_loginPage_usernameTextBox= uiPropertiesReader.getBackoffice_loginPage_usernameTextBox();
        logger.info("uiObjParam.backoffice_loginPage_usernameTextBox :" + uiObjParam.backoffice_loginPage_usernameTextBox);
        uiObjParam.backoffice_loginPage_passwordTextBox= uiPropertiesReader.getBackoffice_loginPage_passwordTextBox();
        logger.info("uiObjParam.backoffice_loginPage_passwordTextBox :" + uiObjParam.backoffice_loginPage_passwordTextBox);
        uiObjParam.backoffice_loginPage_agreementCheckbox= uiPropertiesReader.getBackoffice_loginPage_agreementCheckbox();
        logger.info("uiObjParam.backoffice_loginPage_agreementCheckbox :" + uiObjParam.backoffice_loginPage_agreementCheckbox);
        uiObjParam.backoffice_loginPage_loginButton= uiPropertiesReader.getBackoffice_loginPage_loginButton();
        logger.info("uiObjParam.backoffice_loginPage_loginButton :" + uiObjParam.backoffice_loginPage_loginButton);
        uiObjParam.backoffice_loginPage_forgetPasswordLink= uiPropertiesReader.getBackoffice_loginPage_forgetPasswordLink();
        logger.info("uiObjParam.backoffice_loginPage_forgetPasswordLink :" + uiObjParam.backoffice_loginPage_forgetPasswordLink);
        uiObjParam.backoffice_homepage_dashboardTab= uiPropertiesReader.getBackoffice_homepage_dashboardTab();
        logger.info("uiObjParam.backoffice_homepage_dashboardTab :" + uiObjParam.backoffice_homepage_dashboardTab);
        uiObjParam.backoffice_homepage_newAppTab= uiPropertiesReader.getBackoffice_homepage_newAppTab();
        logger.info("uiObjParam.backoffice_homepage_newAppTab :" + uiObjParam.backoffice_homepage_newAppTab);
        uiObjParam.backoffice_homepage_userManagementTab= uiPropertiesReader.getBackoffice_homepage_userManagementTab();
        logger.info("uiObjParam.backoffice_homepage_userManagementTab :" + uiObjParam.backoffice_homepage_userManagementTab);
        uiObjParam.backoffice_homepage_allUsersTab= uiPropertiesReader.getBackoffice_homepage_allUsersTab();
        logger.info("uiObjParam.backoffice_homepage_allUsersTab :" + uiObjParam.backoffice_homepage_allUsersTab);
        uiObjParam.backoffice_homepage_createNewUserTab= uiPropertiesReader.getBackoffice_homepage_createNewUserTab();
        logger.info("uiObjParam.backoffice_homepage_createNewUserTab :" + uiObjParam.backoffice_homepage_createNewUserTab);
        uiObjParam.backoffice_homepage_searchTab= uiPropertiesReader.getBackoffice_homepage_searchTab();
        logger.info("uiObjParam.backoffice_homepage_searchTab :" + uiObjParam.backoffice_homepage_searchTab);
        uiObjParam.backoffice_homepage_searchAppTab= uiPropertiesReader.getBackoffice_homepage_searchAppTab();
        logger.info("uiObjParam.backoffice_homepage_searchAppTab :" + uiObjParam.backoffice_homepage_searchAppTab);
        uiObjParam.backoffice_homepage_myAppTab= uiPropertiesReader.getBackoffice_homepage_myAppTab();
        logger.info("uiObjParam.backoffice_homepage_myAppTab :" + uiObjParam.backoffice_homepage_myAppTab);
        uiObjParam.backoffice_homepage_assignedAppsTab= uiPropertiesReader.getBackoffice_homepage_assignedAppsTab();
        logger.info("uiObjParam.backoffice_homepage_assignedAppsTab :" + uiObjParam.backoffice_homepage_assignedAppsTab);
        uiObjParam.backoffice_homepage_creditTab= uiPropertiesReader.getBackoffice_homepage_creditTab();
        logger.info("uiObjParam.backoffice_homepage_creditTab :" + uiObjParam.backoffice_homepage_creditTab);
        uiObjParam.backoffice_homepage_incompleteLeadsTab= uiPropertiesReader.getBackoffice_homepage_incompleteLeadsTab();
        logger.info("uiObjParam.backoffice_homepage_incompleteLeadsTab :" + uiObjParam.backoffice_homepage_incompleteLeadsTab);
        uiObjParam.backoffice_homepage_pendingBankLinkingTab= uiPropertiesReader.getBackoffice_homepage_pendingBankLinkingTab();
        logger.info("uiObjParam.backoffice_homepage_pendingBankLinkingTab :" + uiObjParam.backoffice_homepage_pendingBankLinkingTab);
        uiObjParam.backoffice_homepage_pendingVerificationTab= uiPropertiesReader.getBackoffice_homepage_pendingVerificationTab();
        logger.info("uiObjParam.backoffice_homepage_pendingVerificationTab :" + uiObjParam.backoffice_homepage_pendingVerificationTab);
        uiObjParam.backoffice_homepage_underwriterTab= uiPropertiesReader.getBackoffice_homepage_underwriterTab();
        logger.info("uiObjParam.backoffice_homepage_underwriterTab :" + uiObjParam.backoffice_homepage_underwriterTab);
        uiObjParam.backoffice_homepage_preApprovedTab= uiPropertiesReader.getBackoffice_homepage_preApprovedTab();
        logger.info("uiObjParam.backoffice_homepage_preApprovedTab :" + uiObjParam.backoffice_homepage_preApprovedTab);
        uiObjParam.backoffice_homepage_offerGenerationTab= uiPropertiesReader.getBackoffice_homepage_offerGenerationTab();
        logger.info("uiObjParam.backoffice_homepage_offerGenerationTab :" + uiObjParam.backoffice_homepage_offerGenerationTab);
        uiObjParam.backoffice_homepage_docsRequestedTab= uiPropertiesReader.getBackoffice_homepage_docsRequestedTab();
        logger.info("uiObjParam.backoffice_homepage_docsRequestedTab :" + uiObjParam.backoffice_homepage_docsRequestedTab);
        uiObjParam.backoffice_homepage_agreementDocsOutTab= uiPropertiesReader.getBackoffice_homepage_agreementDocsOutTab();
        logger.info("uiObjParam.backoffice_homepage_agreementDocsOutTab :" + uiObjParam.backoffice_homepage_agreementDocsOutTab);
        uiObjParam.backoffice_homepage_pendingSignTab= uiPropertiesReader.getBackoffice_homepage_pendingSignTab();
        logger.info("uiObjParam.backoffice_homepage_pendingSignTab :" + uiObjParam.backoffice_homepage_pendingSignTab);
        uiObjParam.backoffice_homepage_fundingReviewTab= uiPropertiesReader.getBackoffice_homepage_fundingReviewTab();
        logger.info("uiObjParam.backoffice_homepage_fundingReviewTab :" + uiObjParam.backoffice_homepage_fundingReviewTab);
        uiObjParam.backoffice_header_profileDropdown= uiPropertiesReader.getBackoffice_header_profileDropdown();
        logger.info("uiObjParam.backoffice_header_profileDropdown :" + uiObjParam.backoffice_header_profileDropdown);
        uiObjParam.backoffice_header_logoutLink= uiPropertiesReader.getBackoffice_header_logoutLink();
        logger.info("uiObjParam.backoffice_header_logoutLink :" + uiObjParam.backoffice_header_logoutLink);
        uiObjParam.backoffice_searchpage_applicationNumberTextBox= uiPropertiesReader.getBackoffice_searchpage_applicationNumberTextBox();
        logger.info("uiObjParam.backoffice_searchpage_applicationNumberTextBox :" + uiObjParam.backoffice_searchpage_applicationNumberTextBox);
        uiObjParam.backoffice_searchpage_businessPhoneTextBox= uiPropertiesReader.getBackoffice_searchpage_businessPhoneTextBox();
        logger.info("uiObjParam.backoffice_searchpage_businessPhoneTextBox :" + uiObjParam.backoffice_searchpage_businessPhoneTextBox);
        uiObjParam.backoffice_searchpage_firstNameTextBox= uiPropertiesReader.getBackoffice_searchpage_firstNameTextBox();
        logger.info("uiObjParam.backoffice_searchpage_firstNameTextBox :" + uiObjParam.backoffice_searchpage_firstNameTextBox);
        uiObjParam.backoffice_searchpage_lastNameTextBox= uiPropertiesReader.getBackoffice_searchpage_lastNameTextBox();
        logger.info("uiObjParam.backoffice_searchpage_lastNameTextBox :" + uiObjParam.backoffice_searchpage_lastNameTextBox);
        uiObjParam.backoffice_searchpage_emailTextBox= uiPropertiesReader.getBackoffice_searchpage_emailTextBox();
        logger.info("uiObjParam.backoffice_searchpage_emailTextBox :" + uiObjParam.backoffice_searchpage_emailTextBox);
        uiObjParam.backoffice_searchpage_businessNameTextBox= uiPropertiesReader.getBackoffice_searchpage_businessNameTextBox();
        logger.info("uiObjParam.backoffice_searchpage_businessNameTextBox :" + uiObjParam.backoffice_searchpage_businessNameTextBox);
        uiObjParam.backoffice_searchpage_statusDropdown= uiPropertiesReader.getBackoffice_searchpage_statusDropdown();
        logger.info("uiObjParam.backoffice_searchpage_statusDropdown :" + uiObjParam.backoffice_searchpage_statusDropdown);
        uiObjParam.backoffice_searchpage_appDateDropdown= uiPropertiesReader.getBackoffice_searchpage_appDateDropdown();
        logger.info("uiObjParam.backoffice_searchpage_appDateDropdown :" + uiObjParam.backoffice_searchpage_appDateDropdown);
        uiObjParam.backoffice_searchpage_expDateDropdown= uiPropertiesReader.getBackoffice_searchpage_expDateDropdown();
        logger.info("uiObjParam.backoffice_searchpage_expDateDropdown :" + uiObjParam.backoffice_searchpage_expDateDropdown);
        uiObjParam.backoffice_searchpage_searchButton= uiPropertiesReader.getBackoffice_searchpage_searchButton();
        logger.info("uiObjParam.backoffice_searchpage_searchButton :" + uiObjParam.backoffice_searchpage_searchButton);
        uiObjParam.backoffice_searchpage_foundAppIdLabel= uiPropertiesReader.getBackoffice_searchpage_foundAppIdLabel();
        logger.info("uiObjParam.backoffice_searchpage_foundAppIdLabel :" + uiObjParam.backoffice_searchpage_foundAppIdLabel);
        uiObjParam.backoffice_searchpage_foundAppDateLabl= uiPropertiesReader.getBackoffice_searchpage_foundAppDateLabl();
        logger.info("uiObjParam.backoffice_searchpage_foundAppDateLabl :" + uiObjParam.backoffice_searchpage_foundAppDateLabl);
        uiObjParam.backoffice_searchpage_foundBusinessNameLabel= uiPropertiesReader.getBackoffice_searchpage_foundBusinessNameLabel();
        logger.info("uiObjParam.backoffice_searchpage_foundBusinessNameLabel :" + uiObjParam.backoffice_searchpage_foundBusinessNameLabel);
        uiObjParam.backoffice_searchpage_foundBusinessPhoneLabel= uiPropertiesReader.getBackoffice_searchpage_foundBusinessPhoneLabel();
        logger.info("uiObjParam.backoffice_searchpage_foundBusinessPhoneLabel :" + uiObjParam.backoffice_searchpage_foundBusinessPhoneLabel);
        uiObjParam.backoffice_searchpage_foundBusinessLoanAmountLabel= uiPropertiesReader.getBackoffice_searchpage_foundBusinessLoanAmountLabel();
        logger.info("uiObjParam.backoffice_searchpage_foundBusinessLoanAmountLabel :" + uiObjParam.backoffice_searchpage_foundBusinessLoanAmountLabel);
        uiObjParam.backoffice_searchpage_foundBusinessExpiryDateLabel= uiPropertiesReader.getBackoffice_searchpage_foundBusinessExpiryDateLabel();
        logger.info("uiObjParam.backoffice_searchpage_foundBusinessExpiryDateLabel :" + uiObjParam.backoffice_searchpage_foundBusinessExpiryDateLabel);
        uiObjParam.backoffice_searchpage_foundBusinessStatusLabel= uiPropertiesReader.getBackoffice_searchpage_foundBusinessStatusLabel();
        logger.info("uiObjParam.backoffice_searchpage_foundBusinessStatusLabel :" + uiObjParam.backoffice_searchpage_foundBusinessStatusLabel);
        uiObjParam.backoffice_searchpage_viewDetailsButton= uiPropertiesReader.getBackoffice_searchpage_viewDetailsButton();
        logger.info("uiObjParam.backoffice_searchpage_viewDetailsButton :" + uiObjParam.backoffice_searchpage_viewDetailsButton);
        uiObjParam.backoffice_appDetailspage_headerStatus= uiPropertiesReader.getBackoffice_appDetailspage_headerStatus();
        logger.info("uiObjParam.backoffice_appDetailspage_headerStatus :" + uiObjParam.backoffice_appDetailspage_headerStatus);
        uiObjParam.backoffice_appDetailspage_activityLog_email= uiPropertiesReader.getBackoffice_appDetailspage_activityLog_email();
        logger.info("uiObjParam.backoffice_appDetailspage_activityLog_email :" + uiObjParam.backoffice_appDetailspage_activityLog_email);
        uiObjParam.backoffice_appDetailspage_activityLog_factVerification= uiPropertiesReader.getBackoffice_appDetailspage_activityLog_factVerification();
        logger.info("uiObjParam.backoffice_appDetailspage_activityLog_factVerification :" + uiObjParam.backoffice_appDetailspage_activityLog_factVerification);
        uiObjParam.selectBank_selectBankLabel= uiPropertiesReader.getSelectBank_selectBankLabel();
        logger.info("uiObjParam.selectBank_selectBankLabel :" + uiObjParam.selectBank_selectBankLabel);
        uiObjParam.selectBank_searchBankTextBox= uiPropertiesReader.getSelectBank_searchBankTextBox();
        logger.info("uiObjParam.selectBank_searchBankTextBox :" + uiObjParam.selectBank_searchBankTextBox);
        uiObjParam.selectBank_sunTrustBank= uiPropertiesReader.getSelectBank_sunTrustBank();
        logger.info("uiObjParam.selectBank_sunTrustBank :" + uiObjParam.selectBank_sunTrustBank);
        uiObjParam.selectBank_plaidUsernameTextBox= uiPropertiesReader.getSelectBank_plaidUsernameTextBox();
        logger.info("uiObjParam.selectBank_plaidUsernameTextBox :" + uiObjParam.selectBank_plaidUsernameTextBox);
        uiObjParam.selectBank_plaidPasswordTextBox= uiPropertiesReader.getSelectBank_plaidPasswordTextBox();
        logger.info("uiObjParam.selectBank_plaidPasswordTextBox :" + uiObjParam.selectBank_plaidPasswordTextBox);
        uiObjParam.selectBank_submitPlaidButton= uiPropertiesReader.getSelectBank_submitPlaidButton();
        logger.info("uiObjParam.selectBank_submitPlaidButton :" + uiObjParam.selectBank_submitPlaidButton);
        uiObjParam.selectBank_sunTrustResultantBank= uiPropertiesReader.getSelectBank_sunTrustResultantBank();
        logger.info("uiObjParam.selectBank_sunTrustResultantBank :" + uiObjParam.selectBank_sunTrustResultantBank);
        uiObjParam.selectBank_selectYourAccountLabel= uiPropertiesReader.getSelectBank_selectYourAccountLabel();
        logger.info("uiObjParam.selectBank_selectYourAccountLabel :" + uiObjParam.selectBank_selectYourAccountLabel);
        uiObjParam.selectBank_plaidSavingAccount= uiPropertiesReader.getSelectBank_plaidSavingAccount();
        logger.info("uiObjParam.selectBank_plaidSavingAccount :" + uiObjParam.selectBank_plaidSavingAccount);
        uiObjParam.selectBank_continueButton= uiPropertiesReader.getSelectBank_continueButton();
        logger.info("uiObjParam.selectBank_continueButton :" + uiObjParam.selectBank_continueButton);
        uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab_editCashflowDropdown();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown :" + uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown);
        uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflowLink= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab_editCashflowLink();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflowLink :" + uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflowLink);
        uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflow_submitButton= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab_editCashflow_submitButton();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflow_submitButton :" + uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflow_submitButton);
        uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflow_editCashflowSummaryTitle= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab_editCashflow_editCashflowSummaryTitle();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflow_editCashflowSummaryTitle :" + uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflow_editCashflowSummaryTitle);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_closeButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_searchVerification_closeButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_closeButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_closeButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus);
        uiObjParam.backoffice_appDetailspage_activityLog_cashFlowUpdate= uiPropertiesReader.getBackoffice_appDetailspage_activityLog_cashFlowUpdate();
        logger.info("uiObjParam.backoffice_appDetailspage_activityLog_cashFlowUpdate :" + uiObjParam.backoffice_appDetailspage_activityLog_cashFlowUpdate);
        uiObjParam.backoffice_appDetailspage_activityLog_refreshButton= uiPropertiesReader.getBackoffice_appDetailspage_activityLog_refreshButton();
        logger.info("uiObjParam.backoffice_appDetailspage_activityLog_refreshButton :" + uiObjParam.backoffice_appDetailspage_activityLog_refreshButton);
        uiObjParam.backoffice_homepage_actionsButton= uiPropertiesReader.getBackoffice_homepage_actionsButton();
        logger.info("uiObjParam.backoffice_homepage_actionsButton :" + uiObjParam.backoffice_homepage_actionsButton);
        uiObjParam.backoffice_homepage_actions_computeOffer= uiPropertiesReader.getBackoffice_homepage_actions_computeOffer();
        logger.info("uiObjParam.backoffice_homepage_actions_computeOffer :" + uiObjParam.backoffice_homepage_actions_computeOffer);
        uiObjParam.backoffice_homepage_actions_computerOfferButton= uiPropertiesReader.getBackoffice_homepage_actions_computerOfferButton();
        logger.info("uiObjParam.backoffice_homepage_actions_computerOfferButton :" + uiObjParam.backoffice_homepage_actions_computerOfferButton);
        uiObjParam.backoffice_searchpage_offerTab= uiPropertiesReader.getBackoffice_searchpage_offerTab();
        logger.info("uiObjParam.backoffice_searchpage_offerTab :" + uiObjParam.backoffice_searchpage_offerTab);
        uiObjParam.backoffice_searchpage_offerTab_dealGenerationLabel= uiPropertiesReader.getBackoffice_searchpage_offerTab_dealGenerationLabel();
        logger.info("uiObjParam.backoffice_searchpage_offerTab_dealGenerationLabel :" + uiObjParam.backoffice_searchpage_offerTab_dealGenerationLabel);
        uiObjParam.backoffice_searchpage_offerTab_typeOfPaymentDropdown= uiPropertiesReader.getBackoffice_searchpage_offerTab_typeOfPaymentDropdown();
        logger.info("uiObjParam.backoffice_searchpage_offerTab_typeOfPaymentDropdown :" + uiObjParam.backoffice_searchpage_offerTab_typeOfPaymentDropdown);
        uiObjParam.backoffice_searchpage_offerTab_generateDealButton= uiPropertiesReader.getBackoffice_searchpage_offerTab_generateDealButton();
        logger.info("uiObjParam.backoffice_searchpage_offerTab_generateDealButton :" + uiObjParam.backoffice_searchpage_offerTab_generateDealButton);
        uiObjParam.backoffice_searchpage_offerTab_dealSelectedLabel= uiPropertiesReader.getBackoffice_searchpage_offerTab_dealSelectedLabel();
        logger.info("uiObjParam.backoffice_searchpage_offerTab_dealSelectedLabel :" + uiObjParam.backoffice_searchpage_offerTab_dealSelectedLabel);
        uiObjParam.backoffice_homepage_actions_uploadDocsButton= uiPropertiesReader.getBackoffice_homepage_actions_uploadDocsButton();
        logger.info("uiObjParam.backoffice_homepage_actions_uploadDocsButton :" + uiObjParam.backoffice_homepage_actions_uploadDocsButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_idVerificationLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_idverification_idVerificationLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_idVerificationLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_idVerificationLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_idScreenshot= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_idverification_idScreenshot();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_idScreenshot :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_idScreenshot);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_licenceCheckbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_idverification_licenceCheckbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_licenceCheckbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_licenceCheckbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_nameCheckbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_idverification_nameCheckbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_nameCheckbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_nameCheckbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_dobCheckbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_idverification_dobCheckbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_dobCheckbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_dobCheckbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_verifyButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_idverification_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_verifyButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_verifyButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_verifiedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_idverification_verifiedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_verifiedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_verifiedLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_verifiedStatus= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_idverification_verifiedStatus();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_verifiedStatus :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_verifiedStatus);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_closeButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_idverification_closeButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_closeButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_idverification_closeButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifyButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bankverification_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifyButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifyButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_closeButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bankverification_closeButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_closeButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_closeButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus);
        uiObjParam.backoffice_homepage_actions_sendLoanAgreement= uiPropertiesReader.getBackoffice_homepage_actions_sendLoanAgreement();
        logger.info("uiObjParam.backoffice_homepage_actions_sendLoanAgreement :" + uiObjParam.backoffice_homepage_actions_sendLoanAgreement);
        uiObjParam.backoffice_homepage_actions_sendMCAAgreementButton= uiPropertiesReader.getBackoffice_homepage_actions_sendMCAAgreementButton();
        logger.info("uiObjParam.backoffice_homepage_actions_sendMCAAgreementButton :" + uiObjParam.backoffice_homepage_actions_sendMCAAgreementButton);
        uiObjParam.docusign_welcomeLabel= uiPropertiesReader.getDocusign_welcomeLabel();
        logger.info("uiObjParam.docusign_welcomeLabel :" + uiObjParam.docusign_welcomeLabel);
        uiObjParam.docusign_agreementCheckbox= uiPropertiesReader.getDocusign_agreementCheckbox();
        logger.info("uiObjParam.docusign_agreementCheckbox :" + uiObjParam.docusign_agreementCheckbox);
        uiObjParam.docusign_continueButton= uiPropertiesReader.getDocusign_continueButton();
        logger.info("uiObjParam.docusign_continueButton :" + uiObjParam.docusign_continueButton);
        uiObjParam.docusign_startNavigationButton= uiPropertiesReader.getDocusign_startNavigationButton();
        logger.info("uiObjParam.docusign_startNavigationButton :" + uiObjParam.docusign_startNavigationButton);
        uiObjParam.docusign_finishSignButton= uiPropertiesReader.getDocusign_finishSignButton();
        logger.info("uiObjParam.docusign_finishSignButton :" + uiObjParam.docusign_finishSignButton);
        uiObjParam.docusign_adopotSignatureLabel= uiPropertiesReader.getDocusign_adopotSignatureLabel();
        logger.info("uiObjParam.docusign_adopotSignatureLabel :" + uiObjParam.docusign_adopotSignatureLabel);
        uiObjParam.docusign_adoptAndSignButton= uiPropertiesReader.getDocusign_adoptAndSignButton();
        logger.info("uiObjParam.docusign_adoptAndSignButton :" + uiObjParam.docusign_adoptAndSignButton);
        uiObjParam.docusign_postSignThanksNoteLabel= uiPropertiesReader.getDocusign_postSignThanksNoteLabel();
        logger.info("uiObjParam.docusign_postSignThanksNoteLabel :" + uiObjParam.docusign_postSignThanksNoteLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_idVerificationLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_signedDocverification_idVerificationLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_idVerificationLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_idVerificationLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_idScreenshot= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_signedDocverification_idScreenshot();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_idScreenshot :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_idScreenshot);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_allSignatureCheckbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_signedDocverification_allSignatureCheckbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_allSignatureCheckbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_allSignatureCheckbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_verifyButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_signedDocverification_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_verifyButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_verifyButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedStatus= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedStatus();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedStatus :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedStatus);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_closeButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_signedDocverification_closeButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_closeButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_closeButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_welcomeVerificationLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_welcomeverification_welcomeVerificationLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_welcomeVerificationLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_welcomeVerificationLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_acceptTermsCheckbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_welcomeverification_acceptTermsCheckbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_acceptTermsCheckbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_acceptTermsCheckbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_verifyButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_welcomeverification_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_verifyButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_verifyButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedStatus= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedStatus();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedStatus :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedStatus);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_closeButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_welcomeverification_closeButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_closeButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_welcomeverification_closeButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_merchantVerificationLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_merchantverification_merchantVerificationLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_merchantVerificationLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_merchantVerificationLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_idScreenshot= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_merchantverification_idScreenshot();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_idScreenshot :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_idScreenshot);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_addressCheckbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_merchantverification_addressCheckbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_addressCheckbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_addressCheckbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_verifyButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_merchantverification_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_verifyButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_verifyButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_merchantverification_verifiedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedStatus= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_merchantverification_verifiedStatus();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedStatus :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedStatus);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_closeButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_merchantverification_closeButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_closeButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_merchantverification_closeButton);

        uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_propertyVerificationLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_propertyverification_propertyVerificationLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_propertyVerificationLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_propertyVerificationLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_idScreenshot= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_propertyverification_idScreenshot();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_idScreenshot :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_idScreenshot);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_pgorrentCheckbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_propertyverification_pgorrentCheckbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_pgorrentCheckbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_pgorrentCheckbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_goodstandingCheckbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_propertyverification_goodstandingCheckbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_goodstandingCheckbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_goodstandingCheckbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_submitMortgageCheckbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_propertyverification_submitMortgageCheckbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_submitMortgageCheckbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_submitMortgageCheckbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_renewLeaseCheckbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_propertyverification_renewLeaseCheckbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_renewLeaseCheckbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_renewLeaseCheckbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_landlordPhoneTextbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_propertyverification_landlordPhoneTextbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_landlordPhoneTextbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_landlordPhoneTextbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_landlordNameTextbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_propertyverification_landlordNameTextbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_landlordNameTextbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_landlordNameTextbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_verifyButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_propertyverification_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_verifyButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_verifyButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_propertyverification_verifiedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedStatus= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_propertyverification_verifiedStatus();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedStatus :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedStatus);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_closeButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_propertyverification_closeButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_closeButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_propertyverification_closeButton);
        uiObjParam.backoffice_homepage_closeUploadDocsButton= uiPropertiesReader.getBackoffice_homepage_closeUploadDocsButton();
        logger.info("uiObjParam.backoffice_homepage_closeUploadDocsButton :" + uiObjParam.backoffice_homepage_closeUploadDocsButton);
        uiObjParam.backoffice_homepage_actions_addFundingRequestButton= uiPropertiesReader.getBackoffice_homepage_actions_addFundingRequestButton();
        logger.info("uiObjParam.backoffice_homepage_actions_addFundingRequestButton :" + uiObjParam.backoffice_homepage_actions_addFundingRequestButton);
        uiObjParam.backoffice_homepage_actions_addFundingRequestMenuButton= uiPropertiesReader.getBackoffice_homepage_actions_addFundingRequestMenuButton();
        logger.info("uiObjParam.backoffice_homepage_actions_addFundingRequestMenuButton :" + uiObjParam.backoffice_homepage_actions_addFundingRequestMenuButton);
        uiObjParam.backoffice_homepage_actions_editMenuButton= uiPropertiesReader.getBackoffice_homepage_actions_editMenuButton();
        logger.info("uiObjParam.backoffice_homepage_actions_editMenuButton :" + uiObjParam.backoffice_homepage_actions_editMenuButton);
        uiObjParam.homepage_businessTab_dbaTextBox= uiPropertiesReader.getHomepage_businessTab_dbaTextBox();
        logger.info("uiObjParam.homepage_businessTab_dbaTextBox :" + uiObjParam.homepage_businessTab_dbaTextBox);
        uiObjParam.backoffice_appDetailspage_businessinfo_businessName= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_businessName();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_businessName :" + uiObjParam.backoffice_appDetailspage_businessinfo_businessName);
        uiObjParam.backoffice_appDetailspage_businessinfo_email= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_email();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_email :" + uiObjParam.backoffice_appDetailspage_businessinfo_email);
        uiObjParam.backoffice_appDetailspage_businessinfo_dba= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_dba();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_dba :" + uiObjParam.backoffice_appDetailspage_businessinfo_dba);
        uiObjParam.backoffice_appDetailspage_businessinfo_address= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_address();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_address :" + uiObjParam.backoffice_appDetailspage_businessinfo_address);
        uiObjParam.backoffice_appDetailspage_businessinfo_phone= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_phone();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_phone :" + uiObjParam.backoffice_appDetailspage_businessinfo_phone);
        uiObjParam.backoffice_appDetailspage_businessinfo_reqAmount= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_reqAmount();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_reqAmount :" + uiObjParam.backoffice_appDetailspage_businessinfo_reqAmount);
        uiObjParam.backoffice_appDetailspage_businessinfo_businessTaxId= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_businessTaxId();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_businessTaxId :" + uiObjParam.backoffice_appDetailspage_businessinfo_businessTaxId);
        uiObjParam.backoffice_appDetailspage_businessinfo_annualRevenue= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_annualRevenue();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_annualRevenue :" + uiObjParam.backoffice_appDetailspage_businessinfo_annualRevenue);
        uiObjParam.backoffice_appDetailspage_businessinfo_estDate= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_estDate();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_estDate :" + uiObjParam.backoffice_appDetailspage_businessinfo_estDate);
        uiObjParam.backoffice_appDetailspage_businessinfo_businessOwnership= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_businessOwnership();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_businessOwnership :" + uiObjParam.backoffice_appDetailspage_businessinfo_businessOwnership);
        uiObjParam.backoffice_appDetailspage_businessinfo_avgBankBalance= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_avgBankBalance();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_avgBankBalance :" + uiObjParam.backoffice_appDetailspage_businessinfo_avgBankBalance);
        uiObjParam.backoffice_appDetailspage_businessinfo_existingLoan= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_existingLoan();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_existingLoan :" + uiObjParam.backoffice_appDetailspage_businessinfo_existingLoan);
        uiObjParam.backoffice_appDetailspage_businessinfo_industry= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_industry();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_industry :" + uiObjParam.backoffice_appDetailspage_businessinfo_industry);
        uiObjParam.backoffice_appDetailspage_businessinfo_purposeOfFunds= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_purposeOfFunds();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_purposeOfFunds :" + uiObjParam.backoffice_appDetailspage_businessinfo_purposeOfFunds);
        uiObjParam.backoffice_appDetailspage_businessinfo_entityType= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_entityType();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_entityType :" + uiObjParam.backoffice_appDetailspage_businessinfo_entityType);
        uiObjParam.backoffice_appDetailspage_businessinfo_alertsTab= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_alertsTab();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_alertsTab :" + uiObjParam.backoffice_appDetailspage_businessinfo_alertsTab);
        uiObjParam.backoffice_appDetailspage_businessinfo_alerts_alertName= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_alerts_alertName();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_alerts_alertName :" + uiObjParam.backoffice_appDetailspage_businessinfo_alerts_alertName);
        uiObjParam.backoffice_appDetailspage_businessinfo_alerts_alertDescription= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_alerts_alertDescription();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_alerts_alertDescription :" + uiObjParam.backoffice_appDetailspage_businessinfo_alerts_alertDescription);
        uiObjParam.backoffice_appDetailspage_businessinfo_alerts_alertStatus= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_alerts_alertStatus();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_alerts_alertStatus :" + uiObjParam.backoffice_appDetailspage_businessinfo_alerts_alertStatus);
        uiObjParam.backoffice_appDetailspage_businessinfo_alerts_alertTag= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_alerts_alertTag();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_alerts_alertTag :" + uiObjParam.backoffice_appDetailspage_businessinfo_alerts_alertTag);
        uiObjParam.backoffice_appDetailspage_businessinfo_alerts_dismissButton= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_alerts_dismissButton();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_alerts_dismissButton :" + uiObjParam.backoffice_appDetailspage_businessinfo_alerts_dismissButton);
        uiObjParam.backoffice_appDetailspage_activityLog_declineUpdate= uiPropertiesReader.getBackoffice_appDetailspage_activityLog_declineUpdate();
        logger.info("uiObjParam.backoffice_appDetailspage_activityLog_declineUpdate :" + uiObjParam.backoffice_appDetailspage_activityLog_declineUpdate);
        uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab= uiPropertiesReader.getBackoffice_appDetailspage_scoringTab_ruleExecutionTab();
        logger.info("uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab :" + uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab);
        uiObjParam.backoffice_appDetailspage_scoringTab= uiPropertiesReader.getBackoffice_appDetailspage_scoringTab();
        logger.info("uiObjParam.backoffice_appDetailspage_scoringTab :" + uiObjParam.backoffice_appDetailspage_scoringTab);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel);
        uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab= uiPropertiesReader.getBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab();
        logger.info("uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab :" + uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab);
        uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab= uiPropertiesReader.getBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab();
        logger.info("uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab :" + uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab);
        uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab= uiPropertiesReader.getBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab();
        logger.info("uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab :" + uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton);
        uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab_pScoreLabel= uiPropertiesReader.getBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab_pScoreLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab_pScoreLabel :" + uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab_pScoreLabel);
        uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab_modelTab= uiPropertiesReader.getBackoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab_modelTab();
        logger.info("uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab_modelTab :" + uiObjParam.backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab_modelTab);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_notVerifiedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_notVerifiedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_notVerifiedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_notVerifiedLabel);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_passCheckBox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_passCheckBox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_passCheckBox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_passCheckBox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifyButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifyButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifyButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_closeButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_closeButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_closeButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_closeButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifiedLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifiedLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifiedLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifiedLabel);
        uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox :" + uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox);
        uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox :" + uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox);
        uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox :" + uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox);
        uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton :" + uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton);
        uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton :" + uiObjParam.backoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton);
        uiObjParam.backoffice_homepage_actions_uploadBankStatementButton= uiPropertiesReader.getBackoffice_homepage_actions_uploadBankStatementButton();
        logger.info("uiObjParam.backoffice_homepage_actions_uploadBankStatementButton :" + uiObjParam.backoffice_homepage_actions_uploadBankStatementButton);
        uiObjParam.backoffice_appDetailspage_activityLog_bankLinkedUpdate= uiPropertiesReader.getBackoffice_appDetailspage_activityLog_bankLinkedUpdate();
        logger.info("uiObjParam.backoffice_appDetailspage_activityLog_bankLinkedUpdate :" + uiObjParam.backoffice_appDetailspage_activityLog_bankLinkedUpdate);
        uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount :" + uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount);
        uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount :" + uiObjParam.backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount);
        uiObjParam.backoffice_searchpage_productIdTextBox= uiPropertiesReader.getBackoffice_searchpage_productIdTextBox();
        logger.info("uiObjParam.backoffice_searchpage_productIdTextBox :" + uiObjParam.backoffice_searchpage_productIdTextBox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_signedAgreementLabel= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_signedDocverification_signedAgreementLabel();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_signedAgreementLabel :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_signedDocverification_signedAgreementLabel);
        uiObjParam.backoffice_appDetailspage_cashflowTab_setAsCashflowAccountLink= uiPropertiesReader.getBackoffice_appDetailspage_cashflowTab_setAsCashflowAccountLink();
        logger.info("uiObjParam.backoffice_appDetailspage_cashflowTab_setAsCashflowAccountLink :" + uiObjParam.backoffice_appDetailspage_cashflowTab_setAsCashflowAccountLink);
        uiObjParam.backoffice_appDetailspage_businessinfo_leadSource= uiPropertiesReader.getBackoffice_appDetailspage_businessinfo_leadSource();
        logger.info("uiObjParam.backoffice_appDetailspage_businessinfo_leadSource :" + uiObjParam.backoffice_appDetailspage_businessinfo_leadSource);
        uiObjParam.backoffice_homepage_actions_uploadDocsCloseButton= uiPropertiesReader.getBackoffice_homepage_actions_uploadDocsCloseButton();
        logger.info("uiObjParam.backoffice_homepage_actions_uploadDocsCloseButton :" + uiObjParam.backoffice_homepage_actions_uploadDocsCloseButton);
        uiObjParam.backoffice_appDetailspage_verificationDashboard= uiPropertiesReader.getBackoffice_appDetailspage_verificationDashboard();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationDashboard :" + uiObjParam.backoffice_appDetailspage_verificationDashboard);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_CheckBox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_CheckBox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_CheckBox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_CheckBox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerifyButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerifyButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerifyButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerifyButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationStatus= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationStatus();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationStatus :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationStatus);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationPopUpClose= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationPopUpClose();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationPopUpClose :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_annualRevenueDetailsVerification_VerificationPopUpClose);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifystatus= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifystatus();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifystatus :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifystatus);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationdetails= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationdetails();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationdetails :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationdetails);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verificationcheckbox);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifybutton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifybutton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifybutton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifybutton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifyclosebutton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifyclosebutton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifyclosebutton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_timeInBusinessVerification_verifyclosebutton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_addressVerification_verifyDetails= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_addressVerification_verifyDetails();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_addressVerification_verifyDetails :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_addressVerification_verifyDetails);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_addressVerification_verifypopupclose= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_addressVerification_verifypopupclose();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_addressVerification_verifypopupclose :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_addressVerification_verifypopupclose);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_addressVerification_verifystatus= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_addressVerification_verifystatus();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_addressVerification_verifystatus :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_addressVerification_verifystatus);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyButton);
        uiObjParam.backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyCloseButton= uiPropertiesReader.getBackoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyCloseButton();
        logger.info("uiObjParam.backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyCloseButton :" + uiObjParam.backoffice_appDetailspage_verificationdashboard_dataMerchNoVerification_verifyCloseButton);
        uiObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport= uiPropertiesReader.getBackoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport();
        logger.info("uiObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport :" + uiObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport);
        uiObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_firstName= uiPropertiesReader.getBackoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_firstName();
        logger.info("uiObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_firstName :" + uiObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_firstName);
        uiObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage= uiPropertiesReader.getBackoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage();
        logger.info("uiObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage :" + uiObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_frozeMessage);
        uiObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage= uiPropertiesReader.getBackoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage();
        logger.info("uiObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage :" + uiObjParam.backoffice_appDetailspage_3rdPartyData_personalReportTab_experianPersonalReport_noRecordMessage);
        uiObjParam.backoffice_homepage_actions_businessCreditReport= uiPropertiesReader.getBackoffice_homepage_actions_businessCreditReport();
        logger.info("uiObjParam.backoffice_homepage_actions_businessCreditReport :" + uiObjParam.backoffice_homepage_actions_businessCreditReport);
        uiObjParam.backoffice_homepage_actions_businessCreditReportButton= uiPropertiesReader.getBackoffice_homepage_actions_businessCreditReportButton();
        logger.info("uiObjParam.backoffice_homepage_actions_businessCreditReportButton :" + uiObjParam.backoffice_homepage_actions_businessCreditReportButton);
        uiObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes= uiPropertiesReader.getBackoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes();
        logger.info("uiObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes :" + uiObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes);
        uiObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source= uiPropertiesReader.getBackoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source();
        logger.info("uiObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source :" + uiObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_source);
        uiObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate= uiPropertiesReader.getBackoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate();
        logger.info("uiObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate :" + uiObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_Intermediate);
        uiObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score= uiPropertiesReader.getBackoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score();
        logger.info("uiObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score :" + uiObjParam.backoffice_appDetailspage_3rdPartyData_ruleExecution_bp2Attributes_bp2score);
        uiObjParam.backoffice_homepage_actions_businessCreditReportCallExperian= uiPropertiesReader.getBackoffice_homepage_actions_businessCreditReportCallExperian();
        logger.info("uiObjParam.backoffice_homepage_actions_businessCreditReportCallExperian :" + uiObjParam.backoffice_homepage_actions_businessCreditReportCallExperian);
        uiObjParam.backoffice_homepage_actions_businessCreditReportCallRecord= uiPropertiesReader.getBackoffice_homepage_actions_businessCreditReportCallRecord();
        logger.info("uiObjParam.backoffice_homepage_actions_businessCreditReportCallRecord :" + uiObjParam.backoffice_homepage_actions_businessCreditReportCallRecord);
        uiObjParam.backoffice_appDetailspage_activityLog_statusChanged= uiPropertiesReader.getBackoffice_appDetailspage_activityLog_statusChanged();
        logger.info("uiObjParam.backoffice_appDetailspage_activityLog_statusChanged :" + uiObjParam.backoffice_appDetailspage_activityLog_statusChanged);
        uiObjParam.backoffice_homepage_actionsButton_bankruptcyButton= uiPropertiesReader.getBackoffice_homepage_actionsButton_bankruptcyButton();
        logger.info("uiObjParam.backoffice_homepage_actionsButton_bankruptcyButton :" + uiObjParam.backoffice_homepage_actionsButton_bankruptcyButton);
        uiObjParam.backoffice_homepage_actionsButton_bankruptcySearchButton= uiPropertiesReader.getBackoffice_homepage_actionsButton_bankruptcySearchButton();
        logger.info("uiObjParam.backoffice_homepage_actionsButton_bankruptcySearchButton :" + uiObjParam.backoffice_homepage_actionsButton_bankruptcySearchButton);
        uiObjParam.backoffice_homepage_actions_BankruptcyDetailsButton= uiPropertiesReader.getBackoffice_homepage_actions_BankruptcyDetailsButton();
        logger.info("uiObjParam.backoffice_homepage_actions_BankruptcyDetailsButton :" + uiObjParam.backoffice_homepage_actions_BankruptcyDetailsButton);
        uiObjParam.backoffice_appDetailspage_activityLog_statusChanged= uiPropertiesReader.getBackoffice_appDetailspage_activityLog_statusChanged();
        logger.info("uiObjParam.backoffice_appDetailspage_activityLog_statusChanged :" + uiObjParam.backoffice_appDetailspage_activityLog_statusChanged);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_country);
        uiObjParam.backoffice_homepage_actions_editButton= uiPropertiesReader.getBackoffice_homepage_actions_editButton();
        logger.info("uiObjParam.backoffice_homepage_actions_editButton :" + uiObjParam.backoffice_homepage_actions_editButton);
        uiObjParam.backoffice_homepage_actions_editButton_edit= uiPropertiesReader.getBackoffice_homepage_actions_editButton_edit();
        logger.info("uiObjParam.backoffice_homepage_actions_editButton_edit :" + uiObjParam.backoffice_homepage_actions_editButton_edit);
        uiObjParam.backoffice_homepage_actions_editButton_edit_contactName= uiPropertiesReader.getBackoffice_homepage_actions_editButton_edit_contactName();
        logger.info("uiObjParam.backoffice_homepage_actions_editButton_edit_contactName :" + uiObjParam.backoffice_homepage_actions_editButton_edit_contactName);
        uiObjParam.backoffice_homepage_actions_editButton_edit_contactPhone= uiPropertiesReader.getBackoffice_homepage_actions_editButton_edit_contactPhone();
        logger.info("uiObjParam.backoffice_homepage_actions_editButton_edit_contactPhone :" + uiObjParam.backoffice_homepage_actions_editButton_edit_contactPhone);
        uiObjParam.backoffice_homepage_actions_editButton_edit_saveChanges= uiPropertiesReader.getBackoffice_homepage_actions_editButton_edit_saveChanges();
        logger.info("uiObjParam.backoffice_homepage_actions_editButton_edit_saveChanges :" + uiObjParam.backoffice_homepage_actions_editButton_edit_saveChanges);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_businessTab= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_businessTab();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_businessTab :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_businessTab);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_legalBusinessName);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_businessTaxId);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_SICCode= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_businessTab_SICCode();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_SICCode :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_businessTab_SICCode);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_firstName);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_lastName);
        uiObjParam.backoffice_homepage_actions_editButton_edit_Ownerdetails= uiPropertiesReader.getBackoffice_homepage_actions_editButton_edit_Ownerdetails();
        logger.info("uiObjParam.backoffice_homepage_actions_editButton_edit_Ownerdetails :" + uiObjParam.backoffice_homepage_actions_editButton_edit_Ownerdetails);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_address);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_city);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_state);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_postalCode);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_mobileNumber);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_DOB);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_email);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_SSN);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_ownership);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_selectDay= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_selectDay();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_selectDay :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_selectDay);
        uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton= uiPropertiesReader.getBackoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton();
        logger.info("uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton :" + uiObjParam.backoffice_homepage_actions_endiButton_edit_ownerTab_addSecondOwner_closeButton);
        uiObjParam.backoffice_newAppTab_Basic_howMuchDoYouNeed= uiPropertiesReader.getBackoffice_newAppTab_Basic_howMuchDoYouNeed();
        logger.info("uiObjParam.backoffice_newAppTab_Basic_howMuchDoYouNeed :" + uiObjParam.backoffice_newAppTab_Basic_howMuchDoYouNeed);
        uiObjParam.backoffice_newAppTab_Basic_howSoonDoYouNeedIt= uiPropertiesReader.getBackoffice_newAppTab_Basic_howSoonDoYouNeedIt();
        logger.info("uiObjParam.backoffice_newAppTab_Basic_howSoonDoYouNeedIt :" + uiObjParam.backoffice_newAppTab_Basic_howSoonDoYouNeedIt);
        uiObjParam.backoffice_newAppTab_Basic_purposeOfLoan= uiPropertiesReader.getBackoffice_newAppTab_Basic_purposeOfLoan();
        logger.info("uiObjParam.backoffice_newAppTab_Basic_purposeOfLoan :" + uiObjParam.backoffice_newAppTab_Basic_purposeOfLoan);
        uiObjParam.backoffice_newAppTab_Basic_contactFirstName= uiPropertiesReader.getBackoffice_newAppTab_Basic_contactFirstName();
        logger.info("uiObjParam.backoffice_newAppTab_Basic_contactFirstName :" + uiObjParam.backoffice_newAppTab_Basic_contactFirstName);
        uiObjParam.backoffice_newAppTab_Basic_contactLastName= uiPropertiesReader.getBackoffice_newAppTab_Basic_contactLastName();
        logger.info("uiObjParam.backoffice_newAppTab_Basic_contactLastName :" + uiObjParam.backoffice_newAppTab_Basic_contactLastName);
        uiObjParam.backoffice_newAppTab_Basic_phoneNumber= uiPropertiesReader.getBackoffice_newAppTab_Basic_phoneNumber();
        logger.info("uiObjParam.backoffice_newAppTab_Basic_phoneNumber :" + uiObjParam.backoffice_newAppTab_Basic_phoneNumber);
        uiObjParam.backoffice_newAppTab_Basic_email= uiPropertiesReader.getBackoffice_newAppTab_Basic_email();
        logger.info("uiObjParam.backoffice_newAppTab_Basic_email :" + uiObjParam.backoffice_newAppTab_Basic_email);
        uiObjParam.backoffice_newAppTab_Basic_saveAndContinue= uiPropertiesReader.getBackoffice_newAppTab_Basic_saveAndContinue();
        logger.info("uiObjParam.backoffice_newAppTab_Basic_saveAndContinue :" + uiObjParam.backoffice_newAppTab_Basic_saveAndContinue);
        uiObjParam.backoffice_newAppTab_Business_legalBusinessName= uiPropertiesReader.getBackoffice_newAppTab_Business_legalBusinessName();
        logger.info("uiObjParam.backoffice_newAppTab_Business_legalBusinessName :" + uiObjParam.backoffice_newAppTab_Business_legalBusinessName);
        uiObjParam.backoffice_newAppTab_Business_dba= uiPropertiesReader.getBackoffice_newAppTab_Business_dba();
        logger.info("uiObjParam.backoffice_newAppTab_Business_dba :" + uiObjParam.backoffice_newAppTab_Business_dba);
        uiObjParam.backoffice_newAppTab_Business_businessAddress= uiPropertiesReader.getBackoffice_newAppTab_Business_businessAddress();
        logger.info("uiObjParam.backoffice_newAppTab_Business_businessAddress :" + uiObjParam.backoffice_newAppTab_Business_businessAddress);
        uiObjParam.backoffice_newAppTab_Business_city= uiPropertiesReader.getBackoffice_newAppTab_Business_city();
        logger.info("uiObjParam.backoffice_newAppTab_Business_city :" + uiObjParam.backoffice_newAppTab_Business_city);
        uiObjParam.backoffice_newAppTab_Business_postalCode= uiPropertiesReader.getBackoffice_newAppTab_Business_postalCode();
        logger.info("uiObjParam.backoffice_newAppTab_Business_postalCode :" + uiObjParam.backoffice_newAppTab_Business_postalCode);
        uiObjParam.backoffice_newAppTab_Business_rentorown= uiPropertiesReader.getBackoffice_newAppTab_Business_rentorown();
        logger.info("uiObjParam.backoffice_newAppTab_Business_rentorown :" + uiObjParam.backoffice_newAppTab_Business_rentorown);
        uiObjParam.backoffice_newAppTab_Business_industry= uiPropertiesReader.getBackoffice_newAppTab_Business_industry();
        logger.info("uiObjParam.backoffice_newAppTab_Business_industry :" + uiObjParam.backoffice_newAppTab_Business_industry);
        uiObjParam.backoffice_newAppTab_Business_businessTaxId= uiPropertiesReader.getBackoffice_newAppTab_Business_businessTaxId();
        logger.info("uiObjParam.backoffice_newAppTab_Business_businessTaxId :" + uiObjParam.backoffice_newAppTab_Business_businessTaxId);
        uiObjParam.backoffice_newAppTab_Business_sicCode= uiPropertiesReader.getBackoffice_newAppTab_Business_sicCode();
        logger.info("uiObjParam.backoffice_newAppTab_Business_sicCode :" + uiObjParam.backoffice_newAppTab_Business_sicCode);
        uiObjParam.backoffice_newAppTab_Business_entityType= uiPropertiesReader.getBackoffice_newAppTab_Business_entityType();
        logger.info("uiObjParam.backoffice_newAppTab_Business_entityType :" + uiObjParam.backoffice_newAppTab_Business_entityType);
        uiObjParam.backoffice_newAppTab_Business_dateEstablished= uiPropertiesReader.getBackoffice_newAppTab_Business_dateEstablished();
        logger.info("uiObjParam.backoffice_newAppTab_Business_dateEstablished :" + uiObjParam.backoffice_newAppTab_Business_dateEstablished);
        uiObjParam.backoffice_newAppTab_Business_dateEstablished_popup= uiPropertiesReader.getBackoffice_newAppTab_Business_dateEstablished_popup();
        logger.info("uiObjParam.backoffice_newAppTab_Business_dateEstablished_popup :" + uiObjParam.backoffice_newAppTab_Business_dateEstablished_popup);
        uiObjParam.backoffice_newAppTab_Business_dateEstablished_selectDate= uiPropertiesReader.getBackoffice_newAppTab_Business_dateEstablished_selectDate();
        logger.info("uiObjParam.backoffice_newAppTab_Business_dateEstablished_selectDate :" + uiObjParam.backoffice_newAppTab_Business_dateEstablished_selectDate);
        uiObjParam.backoffice_newAppTab_Business_country= uiPropertiesReader.getBackoffice_newAppTab_Business_country();
        logger.info("uiObjParam.backoffice_newAppTab_Business_country :" + uiObjParam.backoffice_newAppTab_Business_country);
        uiObjParam.backoffice_newAppTab_Business_saveAndContinue= uiPropertiesReader.getBackoffice_newAppTab_Business_saveAndContinue();
        logger.info("uiObjParam.backoffice_newAppTab_Business_saveAndContinue :" + uiObjParam.backoffice_newAppTab_Business_saveAndContinue);
        uiObjParam.backoffice_newAppTab_Business_state= uiPropertiesReader.getBackoffice_newAppTab_Business_state();
        logger.info("uiObjParam.backoffice_newAppTab_Business_state :" + uiObjParam.backoffice_newAppTab_Business_state);
        uiObjParam.backoffice_newAppTab_Owner_firstName= uiPropertiesReader.getBackoffice_newAppTab_Owner_firstName();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_firstName :" + uiObjParam.backoffice_newAppTab_Owner_firstName);
        uiObjParam.backoffice_newAppTab_Owner_lastName= uiPropertiesReader.getBackoffice_newAppTab_Owner_lastName();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_lastName :" + uiObjParam.backoffice_newAppTab_Owner_lastName);
        uiObjParam.backoffice_newAppTab_Owner_homeAddress= uiPropertiesReader.getBackoffice_newAppTab_Owner_homeAddress();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_homeAddress :" + uiObjParam.backoffice_newAppTab_Owner_homeAddress);
        uiObjParam.backoffice_newAppTab_Owner_city= uiPropertiesReader.getBackoffice_newAppTab_Owner_city();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_city :" + uiObjParam.backoffice_newAppTab_Owner_city);
        uiObjParam.backoffice_newAppTab_Owner_postalCode= uiPropertiesReader.getBackoffice_newAppTab_Owner_postalCode();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_postalCode :" + uiObjParam.backoffice_newAppTab_Owner_postalCode);
        uiObjParam.backoffice_newAppTab_Owner_country= uiPropertiesReader.getBackoffice_newAppTab_Owner_country();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_country :" + uiObjParam.backoffice_newAppTab_Owner_country);
        uiObjParam.backoffice_newAppTab_Owner_state= uiPropertiesReader.getBackoffice_newAppTab_Owner_state();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_state :" + uiObjParam.backoffice_newAppTab_Owner_state);
        uiObjParam.backoffice_newAppTab_Owner_mobilePhone= uiPropertiesReader.getBackoffice_newAppTab_Owner_mobilePhone();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_mobilePhone :" + uiObjParam.backoffice_newAppTab_Owner_mobilePhone);
        uiObjParam.backoffice_newAppTab_Owner_email= uiPropertiesReader.getBackoffice_newAppTab_Owner_email();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_email :" + uiObjParam.backoffice_newAppTab_Owner_email);
        uiObjParam.backoffice_newAppTab_Owner_OfOwnership= uiPropertiesReader.getBackoffice_newAppTab_Owner_OfOwnership();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_OfOwnership :" + uiObjParam.backoffice_newAppTab_Owner_OfOwnership);
        uiObjParam.backoffice_newAppTab_Owner_ssn= uiPropertiesReader.getBackoffice_newAppTab_Owner_ssn();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_ssn :" + uiObjParam.backoffice_newAppTab_Owner_ssn);
        uiObjParam.backoffice_newAppTab_Owner_dob= uiPropertiesReader.getBackoffice_newAppTab_Owner_dob();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_dob :" + uiObjParam.backoffice_newAppTab_Owner_dob);
        uiObjParam.backoffice_newAppTab_Owner_saveAndContinue= uiPropertiesReader.getBackoffice_newAppTab_Owner_saveAndContinue();
        logger.info("uiObjParam.backoffice_newAppTab_Owner_saveAndContinue :" + uiObjParam.backoffice_newAppTab_Owner_saveAndContinue);
        uiObjParam.backoffice_newAppTab_Finish_annualRevenue= uiPropertiesReader.getBackoffice_newAppTab_Finish_annualRevenue();
        logger.info("uiObjParam.backoffice_newAppTab_Finish_annualRevenue :" + uiObjParam.backoffice_newAppTab_Finish_annualRevenue);
        uiObjParam.backoffice_newAppTab_Finish_averageBankBalances= uiPropertiesReader.getBackoffice_newAppTab_Finish_averageBankBalances();
        logger.info("uiObjParam.backoffice_newAppTab_Finish_averageBankBalances :" + uiObjParam.backoffice_newAppTab_Finish_averageBankBalances);
        uiObjParam.backoffice_newAppTab_Finish_checkBox= uiPropertiesReader.getBackoffice_newAppTab_Finish_checkBox();
        logger.info("uiObjParam.backoffice_newAppTab_Finish_checkBox :" + uiObjParam.backoffice_newAppTab_Finish_checkBox);
        uiObjParam.backoffice_newAppTab_Finish_submit= uiPropertiesReader.getBackoffice_newAppTab_Finish_submit();
        logger.info("uiObjParam.backoffice_newAppTab_Finish_submit :" + uiObjParam.backoffice_newAppTab_Finish_submit);
        uiObjParam.backoffice_homepage_actions_editButton_sendPendingEmail= uiPropertiesReader.getBackoffice_homepage_actions_editButton_sendPendingEmail();
        logger.info("uiObjParam.backoffice_homepage_actions_editButton_sendPendingEmail :" + uiObjParam.backoffice_homepage_actions_editButton_sendPendingEmail);
        uiObjParam.backoffice_homepage_actions_editButton_sendPendingEmail_sendPendingEmailButton= uiPropertiesReader.getBackoffice_homepage_actions_editButton_sendPendingEmail_sendPendingEmailButton();
        logger.info("uiObjParam.backoffice_homepage_actions_editButton_sendPendingEmail_sendPendingEmailButton :" + uiObjParam.backoffice_homepage_actions_editButton_sendPendingEmail_sendPendingEmailButton);
        uiObjParam.backoffice_searchpage_offerTab_editDealButton= uiPropertiesReader.getBackoffice_searchpage_offerTab_editDealButton();
        logger.info("uiObjParam.backoffice_searchpage_offerTab_editDealButton :" + uiObjParam.backoffice_searchpage_offerTab_editDealButton);
        uiObjParam.backoffice_searchpage_offerTab_editAmount= uiPropertiesReader.getBackoffice_searchpage_offerTab_editAmount();
        logger.info("uiObjParam.backoffice_searchpage_offerTab_editAmount :" + uiObjParam.backoffice_searchpage_offerTab_editAmount);
        uiObjParam.backoffice_searchpage_offerTab_editOriginationFee= uiPropertiesReader.getBackoffice_searchpage_offerTab_editOriginationFee();
        logger.info("uiObjParam.backoffice_searchpage_offerTab_editOriginationFee :" + uiObjParam.backoffice_searchpage_offerTab_editOriginationFee);
        uiObjParam.backoffice_searchpage_offerTab_selectedAmount= uiPropertiesReader.getBackoffice_searchpage_offerTab_selectedAmount();
        logger.info("uiObjParam.backoffice_searchpage_offerTab_selectedAmount :" + uiObjParam.backoffice_searchpage_offerTab_selectedAmount);
        uiObjParam.backoffice_searchpage_offerTab_selectedOriginationFee= uiPropertiesReader.getBackoffice_searchpage_offerTab_selectedOriginationFee();
        logger.info("uiObjParam.backoffice_searchpage_offerTab_selectedOriginationFee :" + uiObjParam.backoffice_searchpage_offerTab_selectedOriginationFee);
        uiObjParam.backoffice_homepage_actions_switchOwner= uiPropertiesReader.getBackoffice_homepage_actions_switchOwner();
        logger.info("uiObjParam.backoffice_homepage_actions_switchOwner :" + uiObjParam.backoffice_homepage_actions_switchOwner);
        uiObjParam.backoffice_homepage_actions_switchOwner_message= uiPropertiesReader.getBackoffice_homepage_actions_switchOwner_message();
        logger.info("uiObjParam.backoffice_homepage_actions_switchOwner_message :" + uiObjParam.backoffice_homepage_actions_switchOwner_message);
        uiObjParam.backoffice_homepage_actions_switchOwner_closeButton= uiPropertiesReader.getBackoffice_homepage_actions_switchOwner_closeButton();
        logger.info("uiObjParam.backoffice_homepage_actions_switchOwner_closeButton :" + uiObjParam.backoffice_homepage_actions_switchOwner_closeButton);
        uiObjParam.backoffice_homepage_actions_switchOwner_newOwner= uiPropertiesReader.getBackoffice_homepage_actions_switchOwner_newOwner();
        logger.info("uiObjParam.backoffice_homepage_actions_switchOwner_newOwner :" + uiObjParam.backoffice_homepage_actions_switchOwner_newOwner);
        uiObjParam.backoffice_homepage_actions_switchOwner_AllOwners= uiPropertiesReader.getBackoffice_homepage_actions_switchOwner_AllOwners();
        logger.info("uiObjParam.backoffice_homepage_actions_switchOwner_AllOwners :" + uiObjParam.backoffice_homepage_actions_switchOwner_AllOwners);
        uiObjParam.backoffice_homepage_actions_switchOwner_switchOwnerButton= uiPropertiesReader.getBackoffice_homepage_actions_switchOwner_switchOwnerButton();
        logger.info("uiObjParam.backoffice_homepage_actions_switchOwner_switchOwnerButton :" + uiObjParam.backoffice_homepage_actions_switchOwner_switchOwnerButton);
        uiObjParam.backoffice_homepage_actions_switchOwner_switchActivity= uiPropertiesReader.getBackoffice_homepage_actions_switchOwner_switchActivity();
        logger.info("uiObjParam.backoffice_homepage_actions_switchOwner_switchActivity :" + uiObjParam.backoffice_homepage_actions_switchOwner_switchActivity);
        uiObjParam.backoffice_homepage_actionButton_reapplyAction= uiPropertiesReader.getBackoffice_homepage_actionButton_reapplyAction();
        logger.info("uiObjParam.backoffice_homepage_actionButton_reapplyAction :" + uiObjParam.backoffice_homepage_actionButton_reapplyAction);
        uiObjParam.backoffice_homepage_actionButton_reapplyButton= uiPropertiesReader.getBackoffice_homepage_actionButton_reapplyButton();
        logger.info("uiObjParam.backoffice_homepage_actionButton_reapplyButton :" + uiObjParam.backoffice_homepage_actionButton_reapplyButton);

        
    }

    public void setupDBParams() {

        logger.info("--------------- READING PORTAL PROPERTIES FILE ----------------");

        portalParam.borrowerUrl = portalPropertiesReader.getBorrowerUrl();
        logger.info("portalParam.borrowerUrl :" + portalParam.borrowerUrl);
        portalParam.backOfficeUrl = portalPropertiesReader.getBackOfficeUrl();
        logger.info("portalParam.backOfficeUrl :" + portalParam.backOfficeUrl);
        portalParam.backofficeUsername = portalPropertiesReader.getBackOfficeUsername();
        logger.info("portalParam.backofficeUsername :" + portalParam.backofficeUsername);
        portalParam.backofficePassword = portalPropertiesReader.getBackOfficePassword();
        logger.info("portalParam.backofficePassword :" + portalParam.backofficePassword);
        portalParam.custom_report_location = portalPropertiesReader.getCustom_report_location();
        logger.info("portalParam.custom_report_location :" + portalParam.custom_report_location);
        portalParam.browser = portalPropertiesReader.getBrowser();
        logger.info("portalParam.browser :" + portalParam.browser);
        portalParam.gmailId = portalPropertiesReader.getGmailId();
        logger.info("portalParam.gmailId :" + portalParam.gmailId);
        portalParam.gmailPassword = portalPropertiesReader.getGmailPassword();
        logger.info("portalParam.gmailPassword :" + portalParam.gmailPassword);
        portalParam.plaid_user = portalPropertiesReader.getPlaid_user();
        logger.info("portalParam.plaid_user :" + portalParam.plaid_user);
        portalParam.plaid_password = portalPropertiesReader.getPlaid_password();
        logger.info("portalParam.plaid_password :" + portalParam.plaid_password);
        portalParam.upload_file_location = portalPropertiesReader.getUpload_file_location();
        logger.info("portalParam.upload_file_location :" + portalParam.upload_file_location);
        portalParam.bankToConnect = portalPropertiesReader.getBankToConnect();
        logger.info("portalParam.bankToConnect :" + portalParam.bankToConnect);
        portalParam.iframeSource = portalPropertiesReader.getIframeSource();
        logger.info("portalParam.iframeSource :" + portalParam.iframeSource);
        portalParam.autobankAccountNumber = portalPropertiesReader.getAutobankAccountNumber();
        logger.info("portalParam.autobankAccountNumber :" + portalParam.autobankAccountNumber);
        portalParam.manualBankAccountNumber = portalPropertiesReader.getManualBankAccountNumber();
        logger.info("portalParam.manualBankAccountNumber :" + portalParam.manualBankAccountNumber);

    }

    private void setupJetty(final String contextRoot, Server server) throws Exception {
        final AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        applicationContext.register(ApiConfig.class,PortalConfig.class);
//        applicationContext.register(SmppConfig.class);
        final ServletHolder servletHolder = new ServletHolder(new DispatcherServlet(applicationContext));
        final ServletContextHandler context = new ServletContextHandler();
        context.setErrorHandler(null);
        context.setContextPath("/" + contextRoot);
        context.addServlet(servletHolder, "/*");

        server.setHandler(context);
        server.start();
    }

    @Test(groups = { "Result", "", "" }, description = "Results file creation")
    public static void AAResultsBackUp() throws Exception {
        System.out.println("*****Begining of ResultsBackUp *********************");

        try {
            TestResults.CreateResultTxt();
            //TestResults.ExportResultToTxt("Portal Url:=",GlobalVariables.portalUrl);
            Assert.assertTrue(true, "ResultsBackUp test case has been Passed.");
            System.out.println("*****Ending of ResultsBackUp *********************");

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Exception happened in executing test case ResultsBackUp");
            fail("ResultsBackUp test case has been failed.");
        }
    }

    protected void initializeData(String FuncMod, String Test_ID) throws Exception {

        try {

            logger.info("Reading test data from " + FuncMod + " worksheet.");
            logger.info("-----------------------------------------------");

            if (FuncMod.equalsIgnoreCase("Portal")) {
                portalParam.testId = portalFuncUtils.getTestData(FuncMod, Test_ID, "Test_ID");
                logger.info("portalParam.testId: " + portalParam.testId);
                portalParam.businessName = portalFuncUtils.getTestData(FuncMod, Test_ID, "businessName");
                logger.info("portalParam.businessName: " + portalParam.businessName);
                portalParam.amountNeed = portalFuncUtils.getTestData(FuncMod, Test_ID, "amountNeed");
                logger.info("portalParam.amountNeed: " + portalParam.amountNeed);
                portalParam.durationNeed = portalFuncUtils.getTestData(FuncMod, Test_ID, "durationNeed");
                logger.info("portalParam.durationNeed: " + portalParam.durationNeed);
                portalParam.firstName = portalFuncUtils.getTestData(FuncMod, Test_ID, "firstName");
                logger.info("portalParam.firstName: " + portalParam.firstName);
                portalParam.lastName = portalFuncUtils.getTestData(FuncMod, Test_ID, "lastName");
                logger.info("portalParam.lastName: " + portalParam.lastName);
                String inviteId = new SimpleDateFormat("ddMMHHmmss").format(Calendar.getInstance().getTime());
                portalParam.userName = "LF_test_Ss12"+inviteId+"@yopmail.com";
                portalParam.password = portalPropertiesReader.getEncryptedPassword();
                logger.info("portalParam.password: " + portalParam.password);
                portalParam.primaryPhone = portalFuncUtils.getTestData(FuncMod, Test_ID, "primaryPhone");
                logger.info("portalParam.primaryPhone: " + portalParam.primaryPhone);
                portalParam.purposeOfFunds = portalFuncUtils.getTestData(FuncMod, Test_ID, "purposeOfFunds");
                logger.info("portalParam.purposeOfFunds: " + portalParam.purposeOfFunds);
                portalParam.industry = portalFuncUtils.getTestData(FuncMod, Test_ID, "industry");
                logger.info("portalParam.industry: " + portalParam.industry);
                portalParam.businessAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "businessAddress");
                logger.info("portalParam.businessAddress: " + portalParam.businessAddress);
                portalParam.city = portalFuncUtils.getTestData(FuncMod, Test_ID, "city");
                logger.info("portalParam.city: " + portalParam.city);
                portalParam.businessPhone = portalFuncUtils.getTestData(FuncMod, Test_ID, "businessPhone");
                logger.info("portalParam.businessPhone: " + portalParam.businessPhone);
                portalParam.businessTaxId = portalFuncUtils.getTestData(FuncMod, Test_ID, "businessTaxId");
                logger.info("portalParam.businessTaxId: " + portalParam.businessTaxId);
                portalParam.newTaxId = portalFuncUtils.getTestData(FuncMod, Test_ID, "newTaxId");
                logger.info("portalParam.newTaxId: " + portalParam.newTaxId);
                portalParam.personalName = portalFuncUtils.getTestData(FuncMod, Test_ID, "personalName");
                logger.info("portalParam.personalName: " + portalParam.personalName);
                portalParam.ssn = portalFuncUtils.getTestData(FuncMod, Test_ID, "ssn");
                logger.info("portalParam.ssn: " + portalParam.ssn);
                portalParam.legalCompanyName = portalFuncUtils.getTestData(FuncMod, Test_ID, "legalCompanyName");
                logger.info("portalParam.legalCompanyName: " + portalParam.legalCompanyName);
                portalParam.businessAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "businessAddress");
                logger.info("portalParam.businessAddress: " + portalParam.businessAddress);
                portalParam.city = portalFuncUtils.getTestData(FuncMod, Test_ID, "city");
                logger.info("portalParam.city: " + portalParam.city);
                portalParam.state = portalFuncUtils.getTestData(FuncMod, Test_ID, "state");
                logger.info("portalParam.state: " + portalParam.state);
                portalParam.postalCode = portalFuncUtils.getTestData(FuncMod, Test_ID, "postalCode");
                logger.info("portalParam.postalCode: " + portalParam.postalCode);
                portalParam.ownership = portalFuncUtils.getTestData(FuncMod, Test_ID, "ownership");
                logger.info("portalParam.ownership: " + portalParam.ownership);
                portalParam.entityType = portalFuncUtils.getTestData(FuncMod, Test_ID, "entityType");
                logger.info("portalParam.entityType: " + portalParam.entityType);
                portalParam.estMonth = portalFuncUtils.getTestData(FuncMod, Test_ID, "estMonth");
                logger.info("portalParam.estMonth: " + portalParam.estMonth);
                portalParam.estDay = portalFuncUtils.getTestData(FuncMod, Test_ID, "estDay");
                logger.info("portalParam.estDay: " + portalParam.estDay);
                portalParam.estYear = portalFuncUtils.getTestData(FuncMod, Test_ID, "estYear");
                logger.info("portalParam.estYear: " + portalParam.estYear);
                portalParam.ownerFirstName = portalFuncUtils.getTestData(FuncMod, Test_ID, "ownerFirstName");
                logger.info("portalParam.ownerFirstName: " + portalParam.ownerFirstName);
                portalParam.ownerLastName = portalFuncUtils.getTestData(FuncMod, Test_ID, "ownerLastName");
                logger.info("portalParam.ownerLastName: " + portalParam.ownerLastName);
                portalParam.homeAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "homeAddress");
                logger.info("portalParam.homeAddress: " + portalParam.homeAddress);
                portalParam.homeCity = portalFuncUtils.getTestData(FuncMod, Test_ID, "homeCity");
                logger.info("portalParam.homeCity: " + portalParam.homeCity);
                portalParam.homeState = portalFuncUtils.getTestData(FuncMod, Test_ID, "homeState");
                logger.info("portalParam.homeState: " + portalParam.homeState);
                portalParam.homePostalCode = portalFuncUtils.getTestData(FuncMod, Test_ID, "homePostalCode");
                logger.info("portalParam.homePostalCode: " + portalParam.homePostalCode);
                portalParam.mobileNumber = portalFuncUtils.getTestData(FuncMod, Test_ID, "mobileNumber");
                logger.info("portalParam.mobileNumber: " + portalParam.mobileNumber);
                portalParam.percentOwnership = portalFuncUtils.getTestData(FuncMod, Test_ID, "percentOwnership");
                logger.info("portalParam.percentOwnership: " + portalParam.percentOwnership);
                portalParam.emailAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "emailAddress");
                logger.info("portalParam.emailAddress: " + portalParam.emailAddress);
                portalParam.dobDay = portalFuncUtils.getTestData(FuncMod, Test_ID, "dobDay");
                logger.info("portalParam.dobDay: " + portalParam.dobDay);
                portalParam.dobMonth = portalFuncUtils.getTestData(FuncMod, Test_ID, "dobMonth");
                logger.info("portalParam.dobMonth: " + portalParam.dobMonth);
                portalParam.dobYear = portalFuncUtils.getTestData(FuncMod, Test_ID, "dobYear");
                logger.info("portalParam.dobYear: " + portalParam.dobYear);
                portalParam.annualRevenue = portalFuncUtils.getTestData(FuncMod, Test_ID, "annualRevenue");
                logger.info("portalParam.annualRevenue: " + portalParam.annualRevenue);
                portalParam.aveBankBalance = portalFuncUtils.getTestData(FuncMod, Test_ID, "aveBankBalance");
                logger.info("portalParam.aveBankBalance: " + portalParam.aveBankBalance);
                portalParam.existingLoan = portalFuncUtils.getTestData(FuncMod, Test_ID, "existingLoan");
                logger.info("portalParam.existingLoan: " + portalParam.existingLoan);
                portalParam.dba = portalFuncUtils.getTestData(FuncMod, Test_ID, "dba");
                logger.info("portalParam.dba: " + portalParam.dba);
                portalParam.isDeclined = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "appDeclined"));
                logger.info("portalParam.isDeclined: " + portalParam.isDeclined);
                portalParam.Bp2Score = portalFuncUtils.getTestData(FuncMod, Test_ID, "Bp2Score");
                logger.info("portalParam.Bp2Score: " + portalParam.Bp2Score);
                portalParam.Country =portalFuncUtils.getTestData(FuncMod, Test_ID, "Country");
                logger.info("portalParam.Country: " + portalParam.Country);


            }
            logger.info("-----------------------------------------------");
        } catch (Exception e) {
            logger.info("    *****    Field not present in \"" + FuncMod + "\" worksheet.");
        }
    }

    public void writeToReport(String funcMod,String sTestID, String result) throws IOException {

        String base64String=null;
        JSONObject obj = new JSONObject();
        try {
            obj.put("name", sTestID);
            obj.put("status", result);
            if((funcMod.contains("Portal") && (result.equalsIgnoreCase("Failed"))))
            {
                //File file = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\images\\"+sTestID+".png");
                File file = new File("./"+portalParam.custom_report_location+sTestID+".png");
                System.out.println(file.exists());
                byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
                base64String = new String(encoded, StandardCharsets.US_ASCII);
                obj.put("screenshot", "data:image/png;base64,"+base64String);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            File file = new File(PortalParam.custom_report_location + "test.json");
            ifFileExist = file.createNewFile();
            if (ifFileExist) {
                FileWriter f = new FileWriter(file);
                f.write("cases : [" + obj.toString());
                f.flush();
                f.close();
            } else {
                FileWriter f = new FileWriter(file, true);
                f.write("," + obj.toString());
                f.flush();
                f.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @BeforeMethod(groups = {"verifyEndToEndFlow","Bp2ScoreTests","BusinessScenario1","BusinessScenario","OtherScenario"})
    @Parameters("browser")
    public void setUp() throws Exception {
        //String ibrowser = GlobalVariables.browser;

        browser_list browser = browser_list.valueOf(portalPropertiesReader.getBrowser());
        String nodeUrl;
        switch (browser) {

            case IE:

                // IEDriverServer available @
                // http://code.google.com/p/selenium/downloads/list
                System.setProperty("webdriver.ie.driver", "./webdriver/IEDriverServer.exe");
                DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
                capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                driver = new InternetExplorerDriver(capabilities);
                break;

            case Chrome:
                // chromedriver available @http://code.google.com/p/chromedriver/downloads/list
                System.setProperty("webdriver.chrome.driver", "./webdriver/chromedriver.exe");
                System.setProperty("java.awt.headless", "false");
                driver = new ChromeDriver();
                break;

            case FF:
                System.setProperty("webdriver.gecko.driver" ,"./webdriver/geckodriver.exe");
                driver = new FirefoxDriver();
        }

        Thread.sleep(profile_delay);
        driver.manage().timeouts().implicitlyWait(ShortSleep, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        logger.info("Loading url :" + portalPropertiesReader.getBorrowerUrl());
        driver.get(portalPropertiesReader.getBorrowerUrl());
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_newApplicationLabel)),portalPropertiesReader.getBorrowerUrl() +" could not be loaded.");
    }


    

    @AfterMethod(groups = {"verifyEndToEndFlow","Bp2ScoreTests","BusinessScenario1","BusinessScenario","OtherScenario"})
    public void quit()
    {
        driver.quit();
    }
    /**
     * Waits for the expected condition to complete
     *
     * @param e the expected condition to wait until it becomes true
     * @param timeout  how long to wait for the expected condition to turn true
     * @return true if the expected condition returned true and false if not
     */
    public boolean waitForCondition(ExpectedCondition<Boolean> e, int timeout) {
        WebDriverWait w = new WebDriverWait(driver, timeout);
        boolean returnValue = true;
        try {
            w.until(e);
        } catch (TimeoutException te) {
            logger.error("Failed to find the expected condition", te);
            returnValue = false;
        }
        return returnValue;
    }

    /**
     * Wait for the element to disappear from the screen
     *
     * @param finder the element to wait to disappear
     * @param timeout  how long to wait for the item to disappear (in seconds)
     * @return true if the element disappeared and false if it did not
     */
    protected boolean waitForElementRemoval(final By finder, int timeout) {
        ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return driver.findElements(finder).size() == 0;
            }
        };
        boolean returnValue = waitForCondition(e, timeout);
        return returnValue;
    }

    /**
     * Wait for the element to appear on the screen
     * @param finder  the element to wait to appear
     * @param timeout  how long to wait for the item to appear (in seconds)
     * @return true if the element appeared and false if it did not
     */
    protected boolean waitForElement(final By finder, int timeout) {
        //logger.info("Waiting for element to load");
        ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return driver.findElements(finder).size() > 0;
            }
        };
        boolean returnValue = waitForCondition(e, timeout);
        return returnValue;
    }

    /**
     * Sleeps for the desired amount of time
     *
     * @param time
     *            the amount of time to sleep in ms
     */
    protected void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ie) {
        }
    }

    public void waitForPageLoad(WebDriver driver) {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.manage().window().maximize();
        wait.until(pageLoadCondition);
    }

    @BeforeSuite(alwaysRun=true)
    public void  startCaseReport() throws Exception {
        deletePreviousScreenshots();
        logger.info("Initializing reporting framework");
        logger.info("---------------------------------");
        auto_start = System.currentTimeMillis();

        logger.info("portalParam.custom_report_location:"+portalParam.custom_report_location);

        FileUtils.copyFile(new File(portalParam.custom_report_location + "header.txt"),new File(portalParam.custom_report_location + "header_template.txt"));
        FileUtils.copyFile(new File(portalParam.custom_report_location + "trailor.txt"),new File(portalParam.custom_report_location + "trailor_template.txt"));

        logger.info("Calling Before Suite for generating reporting files.");

        casefile = new File(portalParam.custom_report_location+ "test.json");
        portal_scenfile = new File(portalParam.custom_report_location + "portal_scenario.json");
        portal_feafile = new File(portalParam.custom_report_location + "portal_feature.json");
        api_scenfile = new File(portalParam.custom_report_location + "api_scenario.json");
        api_feafile = new File(portalParam.custom_report_location + "api_feature.json");

        feafile = new File(portalParam.custom_report_location + "feature.json");

        casefile.createNewFile();
        FileWriter casef = new FileWriter(casefile);
        portal_scenfile.createNewFile();
        FileWriter portal_scenf = new FileWriter(portal_scenfile);
        portal_feafile.createNewFile();
        FileWriter portal_feaf = new FileWriter(portal_feafile);
        api_scenfile.createNewFile();
        FileWriter api_scenf = new FileWriter(api_scenfile);
        api_feafile.createNewFile();
        FileWriter api_feaf = new FileWriter(api_feafile);

        feafile.createNewFile();
        FileWriter featurefw = new FileWriter(feafile);
        portal_feaf.write("features: [");
        portal_scenf.write("scenarios: [");
        casef.write("cases : [");
        casef.flush();
        casef.close();
        portal_scenf.flush();
        portal_scenf.close();
        portal_feaf.flush();
        portal_feaf.close();
        featurefw.write("features: [");
        api_feaf.write("features: [");
        api_scenf.write("scenarios: [");
        api_scenf.flush();
        api_scenf.close();
        api_feaf.flush();
        api_feaf.close();
        featurefw.flush();
        featurefw.close();



    }

    private void deletePreviousScreenshots() {
        File folder = new File(PortalParam.upload_file_location);
        File fList[] = folder.listFiles();

        for (File f : fList) {
            if (f.getName().endsWith(".png")) {
                f.delete();
            }}
    }

    public void generateReport(String className, String description,String funcModule) throws IOException, JSONException {

        String classStatus = "";
        File scenFile = null;

        if(funcModule.equalsIgnoreCase("ProjectTest_Api"))
        {
            scenFile = api_scenfile;
        }
        else if(funcModule.equalsIgnoreCase("Portal"))
        {
            scenFile = portal_scenfile;
        }

        logger.info("scenFile:"+scenFile);
        logger.info("casefile:"+casefile);
        BufferedReader scenreader = new BufferedReader(new FileReader(scenFile));
        BufferedReader reader = new BufferedReader(new FileReader(casefile));
        String line = "", oldtext = "";
        while ((line = reader.readLine()) != null) {
            oldtext += line + "\r";
        }

        reader.close();
        String newtext = oldtext.replaceAll("\\[\\,", "[");
        newtext = newtext.replaceAll("\n", "");
        newtext = newtext + "]";
        FileWriter f = new FileWriter(casefile, true);
        f.write(newtext);
        f.flush();
        f.close();

        JSONObject jobj = new JSONObject();
        jobj.put("name", className);
        jobj.put("description", description);
        jobj.put("tags", "");
        if (newtext.contains("Fail")) {
            classStatus = "Fail";
        } else {
            classStatus = "Pass";
        }
        jobj.put("status", classStatus);
        jobj.put("automated", "true");

        line = "";
        oldtext = "";
        while ((line = scenreader.readLine()) != null) {
            oldtext += line + "\r";
        }
        FileWriter scenf = new FileWriter(scenFile, true);
        if (!oldtext.contains("scenarios: [")) {
            scenf.write("scenarios: [\n");
        }
        scenf.write(jobj.toString().replaceAll("\\}", "") + "," + newtext + "\n},");
        scenf.flush();
        scenf.close();
        casefile.delete();
    }

    @AfterSuite (alwaysRun = true)
    public void prepareFinalReport() throws IOException, JSONException,java.lang.Exception {
        auto_finish = System.currentTimeMillis();

        String testEnvtString =  setTestEnvtDetails(auto_start,auto_finish,Execution_start_time);
        testEnvtString = testEnvtString.toString().replaceAll("\"values\"","values").replaceAll("\\[\\{","\\[").replaceAll("\\}\\]","\\]").replaceAll("\":\"", ":");

        UpdateEnvtParams(testEnvtString, portalParam.custom_report_location+ "header_template.txt");
        System.out.println(testEnvtString);
        logger.info("Calling After Suite for preparing html report.");

        if((portal_feafile.isFile()) && (portal_scenfile.isFile()))
        {
            BufferedReader portal_feareader = new BufferedReader(new FileReader(portal_feafile));
            BufferedReader portal_scenreader = new BufferedReader(new FileReader(portal_scenfile));

            String line = "", oldtext = "";
            while((line = portal_scenreader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            portal_scenreader.close();
            //String newfeatext = oldtext.replaceAll("\\}\\,\\r",",\n");
            JSONObject fobj = new JSONObject();
            fobj.put("name","Portal");
            fobj.put("description","CA Portal Test Cases");

            FileWriter feaf = new FileWriter(portal_feafile,true);
            feaf.write(fobj.toString() + ",\n"+ oldtext);
            feaf.flush();
            feaf.close();

            oldtext = "";
            while((line = portal_feareader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            portal_feareader.close();
            oldtext = oldtext.replaceAll("\\\"\\}\\,\\r", "\\\",\n");
            oldtext = oldtext.replaceAll("\\^\\,", "");
            String finalFeatureText = oldtext + "]},";
            FileWriter finalfeaf = new FileWriter(portal_feafile);
            finalfeaf.write(finalFeatureText);
            finalfeaf.flush();
            finalfeaf.close();
        }

        /*if((api_feafile.isFile()) && (api_scenfile.isFile()))
        {
            BufferedReader api_feareader = new BufferedReader(new FileReader(api_feafile));
            BufferedReader api_scenreader = new BufferedReader(new FileReader(api_scenfile));

            String line = "", oldtext = "";
            while((line = api_scenreader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            api_scenreader.close();
            //String newfeatext = oldtext.replaceAll("\\}\\,\\r",",\n");
            JSONObject fobj = new JSONObject();
            fobj.put("name","ProjectTest_Api");
            fobj.put("description","ProjectTest API Test Cases");

            FileWriter feaf = new FileWriter(api_feafile,true);
            feaf.write(fobj.toString() + ",\n"+ oldtext);
            feaf.flush();
            feaf.close();

            oldtext = "";
            while((line = api_feareader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            api_feareader.close();
            oldtext = oldtext.replaceAll("\\\"\\}\\,\\r", "\\\",\n");
            oldtext = oldtext.replaceAll("\\^\\,", "");
            String finalFeatureText = oldtext + "]},";
            FileWriter finalfeaf = new FileWriter(api_feafile);
            finalfeaf.write(finalFeatureText);
            finalfeaf.flush();
            finalfeaf.close();
        }
*/
        //casefile.delete();

        BufferedReader portal_feareader = new BufferedReader(new FileReader(portal_feafile));
        BufferedReader api_feareader = new BufferedReader(new FileReader(api_feafile));

        String oldtext="",line="";
        while((line = api_feareader.readLine()) != null)
        {
            oldtext += line + "\r";
        }
        while((line = portal_feareader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        portal_feareader.close();
        api_feareader.close();
        String[] parts = oldtext.split("features: \\[", 2);
        oldtext = parts[0] + parts[1].replaceAll("features: \\[", "");

        FileWriter finalfeaf = new FileWriter(feafile,true);
        finalfeaf.write(oldtext);
        finalfeaf.flush();
        finalfeaf.close();

        File htmlReportFile = new File(portalParam.custom_report_location + "CA_Automation.html");
        File reportHeader = new File(portalParam.custom_report_location + "header_template.txt");
        File reportFeature = new File(portalParam.custom_report_location + "feature.json");
        File reportTrailer = new File(portalParam.custom_report_location + "trailor_template.txt");
        oldtext = "";line="";
        BufferedReader reportReader = new BufferedReader(new FileReader(reportHeader));
        while((line = reportReader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        BufferedReader featureReader = new BufferedReader(new FileReader(reportFeature));
        while((line = featureReader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        BufferedReader trailerReader = new BufferedReader(new FileReader(reportTrailer));
        while((line = trailerReader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        htmlReportFile.createNewFile();

        FileWriter htmlf = new FileWriter(htmlReportFile);
        htmlf.write(oldtext);
        htmlf.flush();
        htmlf.close();
        UpdateEnvtParams("ENVT_DETAILS", portalParam.custom_report_location + "header_template.txt");
    }

    public void UpdateEnvtParams(String testEnvtString,String fileName){
        try
        {
            String newtext=null;
            File file = new File(fileName);
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = "", oldtext = "";
            while((line = reader.readLine()) != null)
            {
                oldtext += line + "";
            }
            reader.close();
            // replace a word in a file
            //String newtext = oldtext.replaceAll("drink", "Love");

            //To replace a line in a file
            if(!testEnvtString.equalsIgnoreCase("ENVT_DETAILS")) {
                newtext = oldtext.replaceAll("ENVT_DETAILS", testEnvtString);
            }
            else {
                newtext = oldtext.replaceAll(testEnvtString, "ENVT_DETAILS");
            }
            FileWriter writer = new FileWriter(fileName);
            writer.write(newtext);writer.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }

    }

    public String setTestEnvtDetails(long startTime,long endTime,String execution_start_time) throws java.lang.Exception{
        JSONObject jo = new JSONObject();
        jo.put("ProjectName","CA-Automation");
        jo.put("Operating System",System.getProperty("os.name").toLowerCase());
        jo.put("Testing environment",PortalFuncUtils.removeSpecialChar(System.getProperty("envParam")).replaceAll("\\\\",""));
        jo.put("Date",execution_start_time);
        jo.put("Total Execution Time",toHHMMDD(endTime - startTime));

        JSONArray ja = new JSONArray();
        ja.put(jo);

        JSONObject mainObj = new JSONObject();
        mainObj.put("values", ja);
        return mainObj.toString();
    }

    public String toHHMMDD(long time){

        String hms = String.format("%02d"+ " hrs" +":%02d"+ " mins" + ":%02d"+ " sec", TimeUnit.MILLISECONDS.toHours(time),
                TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time)),
                TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
        return hms;
    }

    public boolean isPopedUp() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }   // catch
    }

    public String getOnlyStrings(String s) {
        Pattern pattern = Pattern.compile("[^a-z A-Z]");
        Matcher matcher = pattern.matcher(s);
        String str = matcher.replaceAll("");
        return str;
    }

    public DocusignPage readGmailAndDocusign(WebDriver driver, String mainTab) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver,30);
        driver.get("https://accounts.google.com/ServiceLogin?");
        loginToGmail(portalParam.gmailId,portalParam.gmailPassword);
        if(driver.getCurrentUrl().equalsIgnoreCase("https://myaccount.google.com/?pli=1"))
        {
            driver.get("https://mail.google.com/mail/u/0/#inbox");
        }
        /*driver.findElement(By.xpath("/*//*[@title='Google apps']")).click();
        driver.findElement(By.id("gb23")).click();*/
        List<WebElement> unreademail = driver.findElements(By.xpath("//*[@class='zF']"));
        //String docittMailer = getMailSender(System.getProperty("envParam"));
        String docittMailer = "Sigma via DocuSign";
        for(int i=0;i<unreademail.size();i++) {
            if (unreademail.get(i).isDisplayed() == true) {
                // Verify whether email received from Docitt
                if (unreademail.get(i).getText().equals(docittMailer)) {
                    //driver.get(baseUrl + "/mail/#inbox");
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id=':3a']/span[contains(text(),'"+docittMailer+"')]")));
                    driver.findElement(By.xpath(".//*[@id=':3a']/span[contains(text(),'"+docittMailer+"')]")).click();
                    String gmailTab = driver.getWindowHandle();
                    driver.findElement(By.xpath("//a[contains(@href,'https://demo.docusign.net')]")).click();
                    ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                    newTab.remove(gmailTab);
                    if(newTab.size() != 0) {
                        driver.switchTo().window(newTab.get(0));
                    }

                } else {
                    System.out.println("No mail form " + docittMailer);
                }
            }
        }
        return new DocusignPage(driver);
    }

    private void loginToGmail(String id, String password) {
        WebDriverWait wait = new WebDriverWait(driver,30);
        boolean newGmail = driver.findElements( By.xpath("//div[contains(text(),'More options')]") ).size() != 0;
        if(newGmail)
        {
            driver.findElement(By.xpath("//input[@id='identifierId']")).sendKeys(id);
            driver.findElement(By.xpath("//span[contains(text(),'Next')]")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Forgot password?')]"))).isDisplayed();
            driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
            driver.findElement(By.xpath("//span[contains(text(),'Next')]")).click();
        }
        else {
            driver.findElement(By.id("Email")).sendKeys(id);
            driver.findElement(By.id("next")).click();
            driver.findElement(By.id("Passwd")).sendKeys(password);
            driver.findElement(By.id("signIn")).click();

        }
    }

    public static String takeScreenShot(WebDriver driver,
                                        String screenShotName) {
        try {
            /*File file = new File("Screenshots" + fileSeperator + "Results");
            if (!file.exists()) {
                System.out.println("File created " + file);
                file.mkdir();
            }*/
            screenShotName = screenShotName + ".png";
            File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            File targetFile = new File(PortalParam.custom_report_location, screenShotName);
            FileUtils.copyFile(screenshotFile, targetFile);
            return screenShotName;
        } catch (Exception e) {
            System.out.println("An exception occured while taking screenshot " + e.getCause());
            return null;
        }
    }

}
