# README #

This file describes steps required for setting up automation suite for CA automation on your local machine.

### What is this repository for? ###

* CA Portal Automation

### How do I get set up? ###

1. Download and install jdk 1.8 and latest version of maven 
2. Set appropriate path and classpath as discribed @ https://www.mkyong.com/maven/how-to-install-maven-in-windows/
3. Checkout the code base on your machine
4. Run following commands from the location where pom.xml resides:
	mvn clean install -DskipTests=true -DDEMO_AUTOMATION_VERSION=DEMO_AUTOMATION_1.0.0
	mvn idea:clean idea:idea (If you use idea intellij as your IDE)
	mvn eclipse:clean eclipse:idea (If you use idea intellij as your IDE)
5. Some files will be created in your root directory
6. Click on the project file 


### Who do I talk to? ###

* Shaishav Shah
  shaishav.s@sigmainfo.net
  Automation Lead-QA